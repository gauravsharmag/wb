<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();
            $table->text('refrer')->nullable();    
            $table->text('host')->nullable();
            $table->string('type')->nullable();
            $table->longText('message')->nullable();
            $table->string('ip')->nullable();
            $table->string('url')->nullable();
            $table->string('method')->nullable();
            $table->text('user_agent')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
