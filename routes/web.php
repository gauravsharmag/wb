<?php
use App\Http\Controllers\LanguageController;

 

Route::get('/','Auth\LoginController@showLoginForm')->name('login');
Route::get('/login','Auth\LoginController@showLoginForm');

Auth::routes();
 
Route::post('/login','Auth\LoginController@login')->name('postLogin');    
Route::post('/forgot-password','Auth\LoginController@forgotPassword')->name('forgotPassword'); 
     
Route::get('/users/reset-password/{token}','Auth\LoginController@getResetPassword')->name('getResetPassword'); 
// Route::get('/users/reset-password/{token}','Auth\LoginController@getResetPassword')->name('password.request'); 
Route::post('/users/reset-password/{token}','Auth\LoginController@postResetPassword')->name('postResetPassword');



 
 
// Route::group(['middleware' => ['auth:web,admin,superadmin']],function(){ 
Route::group(['middleware' => ['auth:web,admin,superadmin']],function(){ 

	Route::get('/test','UsersController@test');
 
 
	Route::get('/dashboard','DashboardController@adminDashboad')->name('adminDashboad'); 
	Route::get('/get-dashboard-data','DashboardController@getClientDashboardData')->name('getClientDashboardData');
	Route::get('/get-visit-by-device','DashboardController@getVisitsByDevice')->name('getVisitsByDevice');  
	Route::get('/get-traffic-by-channel','DashboardController@getChannelTraffic')->name('getChannelTraffic');
	Route::get('/get-traffic-by-social','DashboardController@getSocialTraffic')->name('getSocialTraffic');
	Route::get('/get-top-page-views','DashboardController@getTopPageViews')->name('getTopPageViews');


 
	Route::get('/get-analytics-data','DashboardController@getAnalyticsData')->name('getAnalyticsData');  

	
	Route::get('/users/add','UsersController@getAddClient')->name('getAddClient');
	Route::post('/users/add','UsersController@postAddClient')->name('postAddClient');
	Route::get('/users','UsersController@getListUser')->name('getListUser');
	Route::get('/users-ajax','UsersController@getListUserAjax')->name('getListUserAjax'); 
	Route::get('/users/archive/{id}/{type}','UsersController@archiveUser')->name('archiveUser'); 
	Route::get('/users/delete/{id}','UsersController@deleteUser')->name('deleteUser');
	Route::get('/users/edit/{id}','UsersController@getUserEdit')->name('getUserEdit');
	Route::post('/users/edit','UsersController@postUserEdit')->name('postUserEdit'); 
	Route::get('/users/masquerade/{id}','UsersController@masqueradeClient')->name('masqueradeClient');

	Route::get('/audit-logs','AuditController@getAuditLog')->name('getAuditLog');
	Route::get('/audit-logs-ajax','AuditController@getAuditLogAjax')->name('getAuditLogAjax'); 
	Route::get('/audit-logs/view/{id}','AuditController@viewAuditLog')->name('viewAuditLog');
	
	Route::get('/users/admin-profile','UsersController@getEditProfileAdmin')->name('getEditProfileAdmin');
	Route::post('/users/admin-profile','UsersController@postEditProfileAdmin')->name('postEditProfileAdmin');

	Route::get('/support','SupportController@getSupports')->name('getSupports');
	Route::get('/support-ajax','SupportController@getSupportsAjax')->name('getSupportsAjax');
	Route::get('/support/view/{id}','SupportController@viewSupport')->name('viewSupport');

	Route::get('/reports','ReportController@getReport')->name('getReport');
	Route::get('/reports-ajax','ReportController@getReportAjax')->name('getReportAjax');  
	Route::get('/reports/prepare','ReportController@prepareReport')->name('prepareReport'); 


});  

Route::group(['middleware' => ['auth','superadmin']],function(){ 
	Route::get('/users/admins','UsersController@getManageAdmins')->name('getManageAdmins');
	Route::get('/users/admins-ajax','UsersController@getManageAdminsAjax')->name('getManageAdminsAjax');
	Route::get('/users/add-admin','UsersController@getAddAdmin')->name('getAddAdmin'); 
	Route::post('/users/add-admin','UsersController@postAddAdmin')->name('postAddAdmin'); 
	Route::get('/users/edit-admin/{id}','UsersController@getAdminEdit')->name('getAdminEdit');
	Route::post('/users/edit-admin','UsersController@postAdminEdit')->name('postAdminEdit');

	Route::get('/settings','SettingController@getSetting')->name('getSetting');
	Route::post('/settings','SettingController@postSetting')->name('postSetting');
});
 


Route::group(['middleware' => ['auth','client']],function(){   
	Route::get('/client-dashboard','DashboardController@clientDashboad')->name('clientDashboad'); 	
	Route::get('/support/add','SupportController@getSupportAdd')->name('getSupportAdd'); 	
	Route::post('/support/add','SupportController@postSupportAdd')->name('postSupportAdd');
	Route::get('/users/masquerade-back','UsersController@masqueradeClientBack')->name('masqueradeClientBack');

	/*Route::get('/get-client-dashboard-data','DashboardController@getClientDashboardData')->name('getClientDashboardData');
	Route::get('/get-client-visit-by-device','DashboardController@getVisitsByDevice')->name('getVisitsByDevice');  
	Route::get('/get-client-traffic-by-channel','DashboardController@getChannelTraffic')->name('getChannelTraffic');
	Route::get('/get-client-traffic-by-social','DashboardController@getSocialTraffic')->name('getSocialTraffic');
	Route::get('/get-client-top-page-views','DashboardController@getTopPageViews')->name('getTopPageViews');*/

	Route::get('/performance','PerformanceController@getPerformance')->name('getPerformance');
	Route::get('/performance/get-data','PerformanceController@getPerformanceData')->name('getPerformanceData');
	Route::get('/performance/get-device-data','PerformanceController@getDeviceData')->name('getDeviceData');
	Route::get('/performance/get-channel-data','PerformanceController@getChannelData')->name('getChannelData'); 

	Route::get('/reviews','ReviewController@getReviews')->name('getReviews');   

	Route::get('/keywords','KeywordController@getKeywords')->name('getKeywords');     
	Route::get('/keywords/get','KeywordController@getKeywordsAjax')->name('getKeywordsAjax');

	Route::get('/export/keywords','ExportController@exportKeywords')->name('exportKeywords');      

}); 
 

Route::group(['middleware'=>['auth']],function(){   
	Route::get('/logout','Auth\LoginController@logout')->name('logout'); 
	Route::get('/users/profile','UsersController@getEditProfile')->name('getEditProfile');
	Route::post('/users/profile','UsersController@postEditProfile')->name('postEditProfile'); 
	Route::get('/users/change-password','UsersController@getChangePassword')->name('getChangePassword');
	Route::post('/users/change-password','UsersController@postChangePassword')->name('postChangePassword');   

	Route::get('/export','ExportController@exportClientDashboard')->name('exportClientDashboard');   
	Route::get('/export-performance','ExportController@exportPerformance')->name('exportPerformance');

	Route::get('/get-client-dashboard-data','DashboardController@getClientDashboardData')->name('getClientDashboardData');
	Route::get('/get-client-visit-by-device','DashboardController@getVisitsByDevice')->name('getVisitsByDevice');  
	Route::get('/get-client-traffic-by-channel','DashboardController@getChannelTraffic')->name('getChannelTraffic');
	Route::get('/get-client-traffic-by-social','DashboardController@getSocialTraffic')->name('getSocialTraffic');
	Route::get('/get-client-top-page-views','DashboardController@getTopPageViews')->name('getTopPageViews');  
});  

 
 



