<?php 
namespace App\Helpers;
use Config;
use Illuminate\Support\Str;

use Spatie\Analytics\Analytics;
use Spatie\Analytics\AnalyticsClientFactory;
use Illuminate\Support\Facades\Auth;
use DB;

class AnalyticHelper 
{
	public static function getView($viewId) {
	    config(['analytics.view_id' => $viewId]); 
	    $config = config('analytics');
	    $client = AnalyticsClientFactory::createForConfig($config);
	    return new analytics($client, $viewId);
	}

	public static function getGaAccounts() { 
		return DB::table('ga_accounts')
		        ->select( DB::raw("CONCAT(ga_account_id,'-',ga_property_name) AS name"),'id')
		        ->pluck('name','id'); 
	} 
	public static function getUserViews($user) {
		return DB::table('user_views')
				->select( DB::raw("CONCAT(ga_accounts.ga_view_id,'-',ga_accounts.ga_property_name) AS name"),'ga_accounts.id')
				->leftJoin('ga_accounts','ga_accounts.id','=','user_views.ga_account_id')
				->where('user_views.user_id',$user)
				->pluck('ga_accounts.name','ga_accounts.id'); 
	}
	public static function getAllGaViews() {
		return DB::table('user_views')
				->select( DB::raw("CONCAT(ga_accounts.ga_view_id,'-',ga_accounts.ga_property_name) AS name"),'ga_accounts.id')
				->leftJoin('ga_accounts','ga_accounts.id','=','user_views.ga_account_id')
				->pluck('ga_accounts.name','ga_accounts.id'); 
	}
	public static function getGaViewById($id) {
		return DB::table('ga_accounts')->whereId($id)->value('ga_view_id');
	} 

	public static function setGaQueryDates($start,$end) {
		$return=[];
		$startDate = date('Y-m-01');
		$endDate = date('Y-m-d');
		if($start && $end){
		    $startDate = date('Y-m-d',strtotime($start));
		    $endDate = date('Y-m-d',strtotime($end));
		}
		$return['start'] = $startDate;
		$return['end'] = $endDate;
		return $return;
	}

 

	public static function getClientDashboardData($analytics,$view,$start,$end) { 
		$analyticsViewId = 'ga:'.$view;
		$metrics1 = 'ga:sessions, ga:users, ga:newUsers, ga:avgSessionDuration, ga:bounceRate , ga:pageviews, ga:visits,ga:avgTimeOnPage'; 
		$dimensions = 'ga:date,ga:year,ga:month,ga:day';
		// ga:deviceCategory
		$data = $analytics->data_ga->get($analyticsViewId, $start, $end, $metrics1,array('dimensions'=>$dimensions) );
		return $data;
	}
	public static function getClientVisitsByDevice($analytics,$view,$start,$end) {
		$analyticsViewId = 'ga:'.$view;
		$metrics1 = 'ga:visits'; 
		$dimensions = 'ga:date,ga:deviceCategory';
		$data = $analytics->data_ga->get($analyticsViewId, $start, $end, $metrics1,array('dimensions'=>$dimensions) );
		return $data;
	}
	public static function getTrafficByChannel($analytics,$view,$start,$end) {
		$analyticsViewId = 'ga:'.$view;
		$metrics1 = 'ga:visits';  
		$dimensions = 'ga:date,ga:channelGrouping';
		$data = $analytics->data_ga->get($analyticsViewId, $start, $end, $metrics1,array('dimensions'=>$dimensions) );
		return $data; 
	}
	public static function getSocialTraffic($analytics,$view,$start,$end) {
		$analyticsViewId = 'ga:'.$view;
		$metrics1 = 'ga:visits'; 
		$dimensions = 'ga:date,ga:year,ga:month,ga:day,ga:source';
		$data = $analytics->data_ga->get($analyticsViewId, $start, $end, $metrics1,array('dimensions'=>$dimensions) );
		return $data;
	}
	public static function getPageViews($analytics,$view,$start,$end) {
		$analyticsViewId = 'ga:'.$view;
		// ga:uniquePageviews
		$metrics1 = 'ga:pageviews'; 
		$dimensions = 'ga:pagePath,ga:pageTitle';  
		$data = $analytics->data_ga->get($analyticsViewId, $start, $end, $metrics1,array(
						'dimensions'=>$dimensions,
						'sort'=> '-ga:pageviews',
						'max-results' =>5
			) 
		);
		return $data; 	
	}

}