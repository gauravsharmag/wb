<?php


namespace App\Helpers;
use Request;
use Illuminate\Support\Facades\Auth;
use DB;

class Log
{
    public static function write($user,$type,$message)
    {
    	$log=[];
        $log['user']=$user;
    	$log['type']=$type;
        $log['message']=$message;
    	$log['url']=Request::fullUrl();
    	$log['method']=Request::method();
    	$log['ip']=Request::ip();
    	$log['user_agent']=Request::header('user-agent'); 
        $log['host']=Request::getHttpHost();
        $log['refrer']=Request::server('HTTP_REFERER'); 
        $log['created_at']=date('Y-m-d H:i:s');
        $log['updated_at']=date('Y-m-d H:i:s');
        DB::table('logs')->insert($log);
    }

    public static function getAllLogs() {
        return DB::table('logs')->select('id','user','type','created_at')->orderBy('id','desc')->get();
    }
    public static function getLogById($id) {
        return DB::table('logs')->whereId($id)->first();
    } 
}