<?php

namespace App\Http\Middleware;

use Closure;

class AdminClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(\Auth::check()) {
            if(\Auth::user()->role_id !=1 || \Auth::user()->role_id !=3 || \Auth::user()->role_id !=2 ) {
                return redirect()->back()->with('error',__('Sorry,You are not authorized to access that location.'));  
            } 
        }  
        return $next($request);
    }
}
