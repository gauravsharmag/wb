<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AnalyticHelper;
use Yajra\Datatables\Datatables;
use DB;

class KeywordController extends Controller
{
	public function getKeywords() { 
		return view('keywords.index'); 
	}
	public function getKeywordsAjax() {
		$website = Auth::user()->website_url; 
		if($website){
			include(app_path() . '/Functions/Keywords.php');  
			$keywordItems = $result['tasks'][0]['result'][0]['items'];
			$userKeywords = DB::table('user_keywords')->whereUserId(Auth::user()->id)->pluck('keyword','keyword')->toArray();
			if($userKeywords && $keywordItems){
				foreach ($keywordItems as $word) {
					if($word['keyword_data']['keyword']){
						if(in_array($word['keyword_data']['keyword'], $userKeywords)){
							DB::table('user_keywords')
								->whereUserId(Auth::user()->id)
								->whereKeyword($word['keyword_data']['keyword'])
								->update([
									'keyword_rank'=>$word['ranked_serp_element']['serp_item']['rank_absolute'],
									'url'=>$word['ranked_serp_element']['serp_item']['url'],
									'updated_at'=>date('Y-m-d H:i:s')
								]);
						}	
					}
				}
			}
		}
 
		$keywords = DB::table('user_keywords')
						->select('keyword','keyword_rank','url')
						->whereUserId(Auth::user()->id)
						->orderByRaw('ISNULL(keyword_rank), keyword_rank ASC')
						// ->orderBy('keyword_rank','ASC')
						->get();
		return Datatables::of($keywords)
			->editColumn('keyword_rank',function($k){
				if($k->keyword_rank){
					if($k->keyword_rank <=5){
						return '<span class="badge badge-success">'.$k->keyword_rank.'</span>';
					}else if($k->keyword_rank >5 &&  $k->keyword_rank <=10 ){
						return '<span class="badge badge-warning">'.$k->keyword_rank.'</span>';
					} else if($k->keyword_rank >10 &&  $k->keyword_rank <=20 ){
						return '<span class="badge badge-danger">'.$k->keyword_rank.'</span>';
					} else if($k->keyword_rank >20){
						return '<span class="badge badge-dark">'.$k->keyword_rank.'</span>';
					}
				}else {
					return '<span class="badge badge-secondary">Not Ranked</span>';
				}
			})
			->editColumn('url',function($k){
				if($k->url){
					return $k->url;
				}else {
					return "---";
				}
			})
		    ->escapeColumns([]) 
	        ->make(true);
	}  

	
 
}