<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\Helpers\Log; 
use App\Mail\SupportMail; 
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    

	public function getSupports() { 
		return view('supports.index');
	}
	public function getSupportsAjax() {
		$sup = DB::table('supports')->select('id','name','email','created_at')->orderBy('id','DESC')->get();
    	return Datatables::of($sup) 
	        ->addColumn('action', function ($c) {    
	            return '<a href="/support/view/'.$c->id.'" class="btn btn-xs btn-success" title="'.__('View').'"><i class="bx bx-bullseye"></i> </a>';
	        })
	        ->editColumn('created_at',function($c){
	            return date('d-M-Y H:i:s',strtotime($c->created_at));
	        })
	        ->escapeColumns([]) 
	        ->make(true);
	}
	public function viewSupport($id) {
		$support = DB::table('supports')->whereId($id)->first();
		return view('supports.view',compact('support'));
	}

	public function getSupportAdd() {
		return view('supports.add');
	}
	public function postSupportAdd(Request $request) {  
		$request->validate([ 'question' => 'required' ]);
		/*$support = DB::table('supports')->insertGetId([
			'name'=>$request->name,
			'email'=>$request->email,
			'question'=>$request->question,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s')
		]);*/
		$this->notifyAdmin($request->name,$request->email,$request->question); 
		return redirect()->back()->with('success',__('Support request submited successfully'));
	}
	public function notifyAdmin($clientName,$clientEmail,$msg) {
		$email = DB::table('settings')->whereOptionName('email')->value('option_value');  
		$title = DB::table('settings')->whereOptionName('website_title')->value('option_value');  
		if($email){
			if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
			    try {
			      Mail::to($email)->send(new SupportMail($clientName,$clientEmail,$msg,$title));  
			      Log::write('---','SupportEmail - '.$email,'Support Email sent to '.$email.' at '.date('Y-m-d H:i:s').' By '.$clientEmail); 
			    } catch(Exception $e) {   
			      return redirect()->back()->with('error',__('Sorry,mail could not be sent,please try again!'));
			    } 
			}
		}
	}
 

}
