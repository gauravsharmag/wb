<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Illuminate\Http\Request;
use App\Helpers\AnalyticHelper;
use App\Services\AnalyticService;
use Illuminate\Support\Facades\Auth;
use App\Services\ReportService;


class ExportController extends Controller
{
    

	public function exportKeywords(Request $request) {
		ob_start();
		$reportService = new ReportService();
		$keywordsData = $reportService->getKeywordsData(Auth::user()->id);
		if($request->type=='csv'){ 
			$file = fopen('php://output','w'); 
			fputcsv($file,array('Keyword','Rank','Url'));
			$this->addBlankRow($file);
			foreach ($keywordsData as $key) {
				$add=[$key->keyword,$key->keyword_rank,$key->url];		
				fputcsv($file,$add);
			}
			fclose($file); 
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename=keywords.csv');  
			header('Pragma: no-cache');
		}else { 
			$pdf = PDF::loadView('pdf.export_keywords',compact('keywordsData')); 
			return $pdf->download('keywords.pdf'); 
		} 
	}

	public function exportClientDashboard(Request $request) {
		$response['success']=false;
		ob_start();
		include(app_path() . '/Functions/googleAnalytics.php');
		$viewID = AnalyticHelper::getGaViewById($request->view_id); 
		
			$dataRow=[];
			if($viewID){
			    $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end);
			    try {
			        $dashboardData = AnalyticHelper::getClientDashboardData($analytics,$viewID,$dates['start'],$dates['end']); 
			        $socialData = AnalyticHelper::getSocialTraffic($analytics,$viewID,$dates['start'],$dates['end']); 
			        $channelData =  AnalyticHelper::getTrafficByChannel($analytics,$viewID,$dates['start'],$dates['end']);  
			        $deviceData = AnalyticHelper::getClientVisitsByDevice($analytics,$viewID,$dates['start'],$dates['end']);

			        $service = new AnalyticService();
			        $dashboardExport = $service->getDashboardExportData($dashboardData);
			        $socialExport= $service->getSocialExportData($socialData); 
			        $channelExport = $service->getChannelExportData($channelData); 
			        $deviceExport = $service->getDeviceExportData($deviceData);

			        $hasKeywords = false;
			        if(Auth::user()->role_id == 2){
			        	$userID = Auth::user()->id;
        		        $keywords = DB::table('user_keywords')
                							->select('keyword','keyword_rank','url')
                							->whereUserId($userID)
                							->orderByRaw('ISNULL(keyword_rank), keyword_rank ASC')
                							->get();
                		$hasKeywords = true;
			        }
    		        
	        		$topPageViews = AnalyticHelper::getPageViews($analytics,$viewID,$dates['start'],$dates['end']); 

			        if($request->type=='csv'){
	        	        $file = fopen('php://output','w'); 
	        	        fputcsv($file,array('Visits',''));
	        	        fputcsv($file,array('Date','Visits'));

	        	        foreach ($dashboardExport['visits']['date'] as $key => $line) {
	        	        	$add=[$line,$dashboardExport['visits']['views'][$key] ];		
	        	        	fputcsv($file,$add);
	        	        }
	        	        $this->addBlankRow($file);
	        	        fputcsv($file,array('PageViews',''));
	        	        fputcsv($file,array('Date','Views'));
	        	        foreach ($dashboardExport['pageViews']['date'] as $key => $line) {
	        	        	$add=[$line,$dashboardExport['pageViews']['views'][$key] ];		
	        	        	fputcsv($file,$add);
	        	        } 
	        	       	$this->addBlankRow($file);
	        	        fputcsv($file,array('AverageTime','')); 
	        	        fputcsv($file,array('Date','Time'));
	        	        foreach ($dashboardExport['avgTime']['date'] as $key => $line) {
	        	        	$add=[$line,$dashboardExport['avgTime']['views'][$key] ];		
	        	        	fputcsv($file,$add); 
	        	        }
	        	        $this->addBlankRow($file);
	        	        fputcsv($file,array('BounceRate',''));
	        	        fputcsv($file,array('Date','Bouncerate'));
	        	        foreach ($dashboardExport['bounceRate']['date'] as $key => $line) {
	        	        	$add=[$line,$dashboardExport['bounceRate']['views'][$key] ];		
	        	        	fputcsv($file,$add);
	        	        }  
	        	       	$this->addBlankRow($file);
	        	        fputcsv($file,array('Social/Referral',''));
	        	        fputcsv($file,array('Date','Visits'));
	        	        foreach ($socialExport['data'] as $key => $line) {
	        	        	$add=[$key,$line ];		
	        	        	fputcsv($file,$add);
	        	        } 
	        	       	$this->addBlankRow($file);
	        	        fputcsv($file,array('ChannelData',''));
	        	        fputcsv($file,array('Channel','Data'));
	        	        foreach ($channelExport['data'] as $key => $line) {
	        	        	$add=[$key,$line ];		
	        	        	fputcsv($file,$add);
	        	        }  
	        	        $this->addBlankRow($file);
	        	        fputcsv($file,array('DeviceData',''));
	        	        fputcsv($file,array('Device','Data'));
	        			foreach ($deviceExport['data'] as $key => $line) {
	        				$add=[$key,$line ];		
	        				fputcsv($file,$add);
	        			}
	        			
	        			if($keywords){ 
	        				$this->addBlankRow($file);
	        				fputcsv($file,array('Keywords',''));
	        				fputcsv($file,array('Keyword','Rank','Url'));
	        				foreach ($keywords as $key ) {
	        					$add=[$key->keyword,$key->keyword_rank?$key->keyword_rank:'--',$key->url ];		
	        					fputcsv($file,$add);	
	        				}
	        			}
	        	       
	        	        if($topPageViews){
	        	           $this->addBlankRow($file);
	        	           fputcsv($file,array('TopPageViews',''));
	        	           fputcsv($file,array('PageTitle','PageUrl','Views')); 
	        	           foreach ($topPageViews['rows'] as $page ) {
	        	           	$add=[$page[1],$page[0],$page[2]];
	        	           	fputcsv($file,$add);	
	        	           }
	        	        }
	        	        fclose($file); 
	        	        header('Content-type: application/csv');
	        	        header('Content-Disposition: attachment; filename=data.csv'); 
	        	        header('Pragma: no-cache');
			        }else { 
			        	// dd($keywords);
			        	return view('pdf.export_dashboard_charts',compact('request','viewID','topPageViews','keywords','hasKeywords'));    
  
  
			        	/*$pdfData['dashboardData']=$dashboardExport;
			        	$pdfData['socialData']=$socialExport;
			        	$pdfData['channelData']=$channelExport;
			        	$pdfData['deviceData']=$deviceExport;
			        	$pdfData['keywords']=$keywords; 
			        	$pdfData['topPageViews']=$topPageViews;  
			        	$pdf = PDF::loadView('pdf.export_dashboard',compact('pdfData'));
			        	return $pdf->download('data.pdf');*/ 
			        }

			        
			    } catch (Exception $e) {
			        $response['error'] = $e->getMessage();   
			    } 
			}else {
			    $response['error'] = $this->noDataError;
			} 			
		
	}

	public function exportPerformance(Request $request) { 
		ob_start();
		include(app_path() . '/Functions/googleAnalytics.php');	
		$viewID = AnalyticHelper::getGaViewById($request->view_id); 
			$dataRow=[];
			if($viewID){
			    $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end);
			    try {
			        $dashboardData = AnalyticHelper::getClientDashboardData($analytics,$viewID,$dates['start'],$dates['end']); 
			        $deviceData = AnalyticHelper::getClientVisitsByDevice($analytics,$viewID,$dates['start'],$dates['end']);
			        $channelData = AnalyticHelper::getTrafficByChannel($analytics,$viewID,$dates['start'],$dates['end']);

			        $service = new AnalyticService();
				    $dashboardExport = $service->exportPerformanceData($dashboardData);    
			        $deviceExport = $service->exportPerformanceDevice($deviceData);
			        $channelExport = $service->exportPerformanceChannel($channelData); 

			        if($request->type=='csv'){
			        	$file = fopen('php://output','w');
			        	fputcsv($file,array('Visits',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($dashboardExport['visits']['date'] as $key => $line) {
			        		$add=[$line,$dashboardExport['visits']['views'][$key] ];		
			        		fputcsv($file,$add);
			        	}
			        	$this->addBlankRow($file);
			        	
			        	fputcsv($file,array('PageViews',''));
			        	fputcsv($file,array('Date','Views'));
			        	foreach ($dashboardExport['pageViews']['date'] as $key => $line) {
			        		$add=[$line,$dashboardExport['pageViews']['views'][$key] ];		
			        		fputcsv($file,$add);
			        	} 
			        	$this->addBlankRow($file);
			        	fputcsv($file,array('AverageTime','')); 
			        	fputcsv($file,array('Date','Time'));
			        	foreach ($dashboardExport['avgTime']['date'] as $key => $line) {
			        		$add=[$line,$dashboardExport['avgTime']['views'][$key] ];		
			        		fputcsv($file,$add); 
			        	}
			        	$this->addBlankRow($file);
			        	fputcsv($file,array('BounceRate',''));
			        	fputcsv($file,array('Date','Bouncerate'));
			        	foreach ($dashboardExport['bounceRate']['date'] as $key => $line) {
			        		$add=[$line,$dashboardExport['bounceRate']['views'][$key] ];		
			        		fputcsv($file,$add);
			        	}


			        	$this->addBlankRow($file);
			        	fputcsv($file,array('VisitsByDesktop',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($deviceExport['desktop'] as $key => $line) {
			        		$add=[$key,$line];		
			        		fputcsv($file,$add);
			        	}

			        	$this->addBlankRow($file);
			        	fputcsv($file,array('VisitsByMobile',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($deviceExport['mobile'] as $key => $line) {
			        		$add=[$key,$line];		
			        		fputcsv($file,$add);
			        	}

			        	$this->addBlankRow($file);
			        	fputcsv($file,array('VisitsByTablet',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($deviceExport['tab'] as $key => $line) { 
			        		$add=[$key,$line];		
			        		fputcsv($file,$add);
			        	}
			        	

			        	$this->addBlankRow($file);
			        	fputcsv($file,array('DirectVisits',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($channelExport['direct'] as $key => $line) { 
			        		$add=[$key,$line];		
			        		fputcsv($file,$add);
			        	}

			        	$this->addBlankRow($file);
			        	fputcsv($file,array('OrganicVisits',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($channelExport['organic'] as $key => $line) { 
			        		$add=[$key,$line];		
			        		fputcsv($file,$add);
			        	}

			        	$this->addBlankRow($file);
			        	fputcsv($file,array('ReferralVisits',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($channelExport['referral'] as $key => $line) { 
			        		$add=[$key,$line];		
			        		fputcsv($file,$add);
			        	}

			        	$this->addBlankRow($file);
			        	fputcsv($file,array('SocialVisits',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($channelExport['social'] as $key => $line) { 
			        		$add=[$key,$line];		
			        		fputcsv($file,$add);
			        	}

			        	$this->addBlankRow($file);
			        	fputcsv($file,array('PaidVisits',''));
			        	fputcsv($file,array('Date','Visits'));
			        	foreach ($channelExport['paid'] as $key => $line) { 
			        		$add=[$key,$line];		
			        		fputcsv($file,$add);
			        	} 
			        	fclose($file);  
			        	header('Content-type: application/csv');
			        	header('Content-Disposition: attachment; filename=data.csv'); 
			        	header('Pragma: no-cache');
			        }else {
    		        	return view('pdf.export_performance_charts',compact('request','viewID'));   
			        	/*$pdfData['dashboardData']=$dashboardExport;
			        	$pdfData['channelData']=$channelExport;
			        	$pdfData['deviceData']=$deviceExport;
			        	$pdf = PDF::loadView('pdf.export_performance',compact('pdfData'));
			        	return $pdf->download('data.pdf');  */
			        } 

			    } catch (Exception $e) {
			        $response['error'] = $e->getMessage();   
			    } 
			}else {
			    $response['error'] = $this->noDataError;
			}
	}  

	public function addBlankRow(&$file) {
		fputcsv($file,array('',''));
	    fputcsv($file,array('',''));
	}

}
