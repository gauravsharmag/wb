<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Log;


class AuditController extends Controller
{
    public function getAuditLog() {
   		return view('audit_logs.index');
    }
    public function getAuditLogAjax() {
    	$logs = Log::getAllLogs();
    	return Datatables::of($logs) 
	        ->addColumn('action', function ($c) {    
	            return '<a href="/audit-logs/view/'.$c->id.'" class="btn btn-xs btn-success" title="'.__('View').'"><i class="bx bx-bullseye"></i> </a>';
	        })
	        ->editColumn('created_at',function($c){
	            return date('d-M-Y H:i:s',strtotime($c->created_at));
	        })
	        ->escapeColumns([]) 
	        ->make(true);
    }
    public function viewAuditLog($id) {
    	$log = Log::getLogById($id);
    	return view('audit_logs.view',compact('log')); 
    }

}
