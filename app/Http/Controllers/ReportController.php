<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use PDF;
use App\Services\ReportService;
use App\Services\ExportService; 
use App\Services\AnalyticService;
use Yajra\Datatables\Datatables;


class ReportController extends Controller
{


	public $reportService;
	public $exportService; 

	public function __construct(ReportService $reportService,ExportService $exportService) {
		$this->reportService = $reportService;
		$this->exportService = $exportService;
	} 

  public function getGaReportData($start,$end,$user) {
    $exportData = [];
    include(app_path() . '/Functions/googleAnalytics.php'); 
    $reportData = $this->reportService->getGaReportData($analytics,$start,$end,$user);
    $service = new AnalyticService();
    $exportData['dashboardExport'] = $service->getDashboardExportData($reportData['dashboardData']);
    $exportData['socialExport']= $service->getSocialExportData($reportData['socialData']); 
    $exportData['channelExport'] = $service->getChannelExportData($reportData['channelData']); 
    $exportData['deviceExport'] = $service->getDeviceExportData($reportData['deviceData']);
    $exportData['topPageViews'] = $reportData['topPageViews'];
    return $exportData;
  }
  public function getKeywordReportData($user) {
    return $this->reportService->getKeywordsData($user);
  }
  public function getFullReportData($start,$end,$user) {
    $rsp=[];
    $rsp['exportGaData'] = $this->getGaReportData($start,$end,$user);
    $rsp['exportKeywordData'] = $this->getKeywordReportData($user);
    return $rsp;
  }


  public function prepareReport(Request $request) {
      ob_start();
      $report = $this->checkReportType($request->type);
      if($report['success']){
        if($report['reportType']=='ga'){ 
          $exportData = $this->getGaReportData($request->start,$request->end,$report['userID']);
          if($request->report_type=='csv'){
            $this->exportService->exportGaReportCsv($request->report_type,$exportData);   
          }else {  
            $pdfData['dashboardData']=$exportData['dashboardExport'];
            $pdfData['socialData']=$exportData['socialExport'];
            $pdfData['channelData']=$exportData['channelExport'];
            $pdfData['deviceData']=$exportData['deviceExport'];
            $pdfData['topPageViews']=$exportData['topPageViews']; 
            $pdf = PDF::loadView('pdf.export_dashboard',compact('pdfData'));
            return $pdf->download('data.pdf'); 
          }
        }else if($report['reportType']=='kw') { 
          $keywordsData = $this->getKeywordReportData($report['userID']);
          if($request->report_type=='csv'){
            $this->exportService->exportKeywordsCsv($keywordsData);   
          } else {  
            $pdf = PDF::loadView('pdf.export_keywords',compact('keywordsData')); 
            return $pdf->download('keywords.pdf'); 
          }
        }else {
          $fullData = $this->getFullReportData($request->start,$request->end,$report['userID']);
          // dd($fullData);  
          if($request->report_type=='csv'){ 
            $this->exportService->exportFullReportCsv($fullData);    
          } else {  
            $pdf = PDF::loadView('pdf.export_full',compact('fullData'));  
            return $pdf->download('data.pdf'); 
          }
        }
      }      
  }
 

   
 	public function getReport() {
 		return view('reports.index'); 
 	} 
 
 	public function getReportAjax(Request $request) {
 		$users = DB::table('users')
 			->select('id','name','email','phone','created_at')
      	->where('role_id',2)
      	->whereArchived(0) 
      	->get();  
 		return Datatables::of($users)  
         ->editColumn('email',function($c){
            return '<span id="skw_'.$c->id.'"><input type="checkbox" onclick="prepareReport(this);" class="checkbox-input" name="kw_'.$c->id.'" id="kw_'.$c->id.'"></span>';
         }) 
         ->editColumn('phone',function($c){
            return '<span id="sga_'.$c->id.'"><input type="checkbox" onclick="prepareReport(this);" class="checkbox-input" name="ga_'.$c->id.'" id="ga_'.$c->id.'"></span>';
         })
         ->editColumn('created_at',function($c){
            return '<span id="sfull_'.$c->id.'"><input type="checkbox" onclick="prepareReport(this);" class="checkbox-input" name="full_'.$c->id.'" id="full_'.$c->id.'"></span>';  
         })
         ->escapeColumns([]) 
         ->make(true);  
 	}

  private function checkReportType($str) {
    $resp['success']=false;;
    if($str){
      $chunks = explode('_',$str); 
      if($chunks){
        $resp['reportType'] = $chunks[0];
        $resp['userID'] = $chunks[1];
        $resp['success'] = true;
      } 
    }
    return $resp;
  } 




}
