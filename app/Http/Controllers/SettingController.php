<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class SettingController extends Controller
{
	
	public function getSetting() {
		$options=DB::table('settings')->select('option_name','option_value')->get();
		$optArr=[];
		foreach($options as $o){
		    $optArr[$o->option_name]=$o->option_value;
		}
		return view('settings.index',compact('optArr'));    	 
	} 
	public function postSetting(Request $request) {
		$request->validate([ 
		    'website_title' => 'required|max:60',
		    'website_desc' => 'required|max:160',
		    'logo' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
		    'phone'=>'required|digits:10', 
		    'email'=>'required|email|unique:users|max:100',
		    'footer_text'=>'required'
		]); 
		
		$error='';
		foreach ($request->all() as $key => $value) {
		    if($value && $key !='_token' && $key !='logo') {
		        $check=DB::table('settings')->where('option_name',$key)->exists();
		        if($check){
		            $update=DB::table('settings')->where('option_name',$key)->update(['option_value'=>$value,'updated_at'=>date('Y-m-d H:i:s')]);
		            if(!$update){
		                $error='ERROR';
		            }
		        } else {
		            $insert=DB::table('settings')->insertGetId(['option_value'=>$value,'option_name'=>$key,'created_at'=>date('Y-m-d H:i:s')]);
		            if(!$insert){
		                $error='ERROR';
		            }           
		        }
		    }
		} 

		if($request->has('logo')){
			$logo='logo';
		    $logoName=uniqid().'.'.$request->logo->getClientOriginalExtension(); 
		    $path='/images/logo/';
		    $request->$logo->move(public_path('images/logo'),$logoName);
		    $checkLogo=DB::table('settings')->whereOptionName('logo')->first();
		    if($checkLogo){
		        $updateLogo=DB::table('settings')->whereOptionName('logo')->update([
		            'option_value'=>$path.$logoName,
		            'updated_at'=>date('Y-m-d H:i:s')
		        ]); 
		        $chunks=explode('/',$checkLogo->option_value);
		        if(isset($chunks[3]) && $chunks[3]){
		            @unlink(public_path('images/logo/').$chunks[3]); 
		        }
		        if(!$updateLogo){
		            $error='ERROR';
		        }
		    }else{
		        $insertLogo=DB::table('settings')->insertGetId([
		            'option_value'=>$path.$logoName,
		            'option_name'=>'logo',  
		            'created_at'=>date('Y-m-d H:i:s'),
		            'updated_at'=>date('Y-m-d H:i:s')
		        ]);
		        if(!$insertLogo){ 
		            $error='ERROR';
		        }
		    }
		}

		if($error==''){ 
		    return redirect()->back()->with('success',__('Settings saved successfully'));
		}else{
		    return redirect()->back()->with('error',__('Sorry,settings cannot be saved,please try again!')); 
		} 
	}


}
