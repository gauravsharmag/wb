<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;
use Mail;
use App\Mail\SendPasswordResetLink;
use App\Helpers\Log; 


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers; 

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // Login
    public function showLoginForm(){
        $pageConfigs = ['bodyCustomClass' => 'bg-full-screen-image'];
        if(Auth::check()){ 
            if(Auth::user()->role_id == 2 ){
                return redirect()->route('clientDashboad');
            }else {
                return redirect()->route('adminDashboad'); 
            }
        }
        return view('/auth/login', [
            'pageConfigs' => $pageConfigs
        ]);
    }

     /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    { 
        Log::write(Auth::user()->name,'Logout',Auth::user()->email.' Logged out at '.date('Y-m-d H:i:s')); 
        // $this->guard()->logout();
        Auth::logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/login');
    }
    public function login(Request $request) {  
        $request->validate([ 
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]); 
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password],$request->remember ) ) {
            if(!Auth::user()->archived){ 
                Log::write(Auth::user()->name,'Login',Auth::user()->email.' Logged in at '.date('Y-m-d H:i:s'));
                if(Auth::user()->role_id==1 || Auth::user()->role_id==3){ 
                    return redirect()->intended('/dashboard');
                }else { 
                    return redirect()->intended('client-dashboard');
                }
            }else {
                Auth::logout();
                return redirect()->back()->with('error',__('Sorry,your account has been archived,please ask admin to reactivate your account.'));    
            }
        }
        return redirect()->back()->with('error',__('Invalid details,please try again!'));
    }

    public function forgotPassword(Request $r) {   
        if($r->email) { 
            $user=User::whereEmail($r->email)->first(); 
            if($user) {
                $token=uniqid();
                DB::table('password_resets')->insert([
                    'user_id'=>$user->id,
                    'email'=>$user->email,
                    'token'=>$token, 
                    'created_at'=>date('Y-m-d H:i:s')
                ]);
                $url=url('/').'/users/reset-password/'.$token; 
                $site_title = env('SITE_TITLE');
                $this->sendPasswordResetLink($r->email,'Password Reset Link','Please click on the link below to reset your password',$url,$site_title);
                return redirect('/login')->with('success',__('Please check your inbox for reset link')); 
            } else { 
                return redirect()->back()->with('error',__('Sorry,email does not exist'));
            }
        }
        return redirect()->back()->with('error',__('Sorry,email is required'));  
    } 
    public function getResetPassword($token){
        if($token) {
            $status=DB::table('password_resets')->whereToken($token)->first();
            if($status) {
                if(!$status->status) { 
                    return view('pages.auth-reset-password',compact('token'));  
                } else {
                    return redirect('/login')->with('error',__('Invalid token'));   
                }
            } else {
                return redirect('/login')->with('error',__('Invalid token'));   
            }
        } 
    }    
    public function postResetPassword(Request $r,$token){
        $this->validate($r, [ 
            'password'=> 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/', 
            'confirm_password' => 'required|same:password' 
        ],
        [
            'password.regex' =>'Password must have at least 1 lowercase AND 1 uppercase AND 1 number'
        ]
        );
        $status=DB::table('password_resets')->whereToken($token)->first();  
        if($status) {
            if(!$status->status) {
                $user=User::find($status->user_id);
                $user->password=Hash::make($r->get('password')); 
                if($user->save()) { 
                    DB::table('password_resets')->whereToken($token)->update(['status'=>1]); 
                    return redirect('/login')->with('success',__('Password updated successfully,please login'));
                } else {
                    return redirect()->back()->with('error',__('Sorry,password could not be updated,please try again!'));
                } 
            } else {
                return redirect()->back()->with('error',__('Invalid token'));   
            }
        }else{ 
            return redirect()->back()->with('error',__('Invalid token'));   
        } 
    }
    public function sendPasswordResetLink($email,$subject,$msg,$link,$site_title) {
        if($email) {
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                try {
                    Mail::to($email)->send(new SendPasswordResetLink($subject,$msg,$link,$site_title));      
                } catch(Exception $e) { 
                    return redirect()->back()->with('error',__('Sorry,mail could not be sent,please try again!'));
                }
            }
        }
    }

}
