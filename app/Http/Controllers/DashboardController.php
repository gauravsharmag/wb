<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use Analytics;
use Spatie\Analytics\Period; 
use Config;

use Spatie\Analytics\Analytics;
use Spatie\Analytics\AnalyticsClientFactory;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AnalyticHelper;
use DB;


class DashboardController extends Controller
{
    //ecommerce
    public $noDataError = 'No Data Found';

    public function dashboardEcommerce(){
        return view('pages.dashboard-ecommerce');
    }

    public function getAnalyticsData() {
        include(app_path() . '/Functions/googleAnalytics.php');
        foreach ($profile as $p) {
            $check = DB::table('ga_accounts')->where('ga_account_id',$p[3])->first();
            if($check){
                DB::table('ga_accounts')->where('ga_account_id',$p[3])->update([
                    'ga_account_id'=>$p[3],
                    'ga_property_name'=>$p[1],
                    'ga_view_id'=>$p[0],
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);    
            }else {
                DB::table('ga_accounts')->insert([
                    'ga_account_id'=>$p[3],
                    'ga_property_name'=>$p[1],
                    'ga_view_id'=>$p[0],
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);    
            }
        }
    } 

    public function adminDashboad() {
        $views = AnalyticHelper::getAllGaViews();
        return view('users.admin_dashboard',compact('views')); 
    }

    public function returnFirstElement($arr) {
        $return=[];
        $c=0;
        foreach ($arr as $key => $value) {
            if($c==0){
                $return['id'] = $key;
                $return['value']=$value;
            }
            $c++;
        }
        return $return;
    }
    public function clientDashboad() {
        $views = AnalyticHelper::getUserViews(Auth::user()->id);
        $viewArr=$views->toArray();
        $firstView = $this->returnFirstElement($viewArr);
        return view('users.client_dashboard',compact('views','viewArr'));     
    }
    public function getClientDashboardData(Request $request) { 
        $response['success']=false;
        include(app_path() . '/Functions/googleAnalytics.php');
        $viewID = AnalyticHelper::getGaViewById($request->view_id); 
        $dataRow=[];
        if($viewID){
            $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end);
            try {
                $data = AnalyticHelper::getClientDashboardData($analytics,$viewID,$dates['start'],$dates['end']);     
                if($data){ 
                    $counter=0;
                    $totalArr=[];
                    $totalVisits = $totalPageViews = $totalBounceRate = $totalAvgTime = 0;
                    foreach ($data['rows'] as $row) {
                        $dataRow[$counter]['date'] = $row[2].'-'.$row[3].'-'.$row[1];
                        $dataRow[$counter]['visits'] = $row[10];
                        $dataRow[$counter]['pageViews'] = $row[9]; 
                        $dataRow[$counter]['bounceRate'] = round($row[8]); 
                        $dataRow[$counter]['avgTime'] = round($row[11]);   

                        $totalVisits = $totalVisits + $row[10];
                        $totalPageViews = $totalPageViews + $row[9];
                        $totalBounceRate = $totalBounceRate + $row[8];
                        $totalAvgTime = $totalAvgTime + $row[11];
                        $counter++;
                    }
                    $totalArr['totalVisits'] = $totalVisits;
                    $totalArr['totalPageViews'] = $totalPageViews;
                    $totalArr['totalBounceRate'] = $totalBounceRate;
                    $totalArr['totalAvgTime'] = $totalAvgTime;
                    $totalArr['avgTime'] = round($totalAvgTime/count($dataRow)) .' seconds'; 
                    $totalArr['avgBounceRate'] = round($totalBounceRate / count($dataRow)).'%';

                    if($dataRow){
                        $response['success']=true;
                        $response['data'] = $dataRow;
                        $response['duration'] = 'from '.date('M d, Y',strtotime($dates['start'])) .' to '.date('M d, Y',strtotime($dates['end']));
                        $response['totals'] = $totalArr;
                    }
                } 
            } catch (Exception $e) {
                $response['error'] = $e->getMessage();   
            } 
        }else {
            $response['error'] = $this->noDataError;
        } 
        return response()->json(['response'=>$response], 200);
    }  
    public function getSocialTraffic(Request $request) {
        $response['success']=false;
        include(app_path() . '/Functions/googleAnalytics.php');
        $viewID = AnalyticHelper::getGaViewById($request->view_id);
        $dataRow=$countRow=$nDataRow=$totalArr=[];
        $sum = 0;
        if($viewID){
            $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end);
            try {
                $data = AnalyticHelper::getSocialTraffic($analytics,$viewID,$dates['start'],$dates['end']); 
                if($data){
                    $counter=0;
                    foreach ($data['rows'] as $row) {
                        if($row[4] !='(direct)'){
                            $countRow[$counter]['date'] = $row[2].'-'.$row[3].'-'.$row[1];
                            $countRow[$counter]['visits'] = $row[5];
                            $counter++;
                        }
                    }
                    if($countRow){
                        $response['success']=true;
                        foreach ($countRow as $value) {
                            $dataRow[$value['date']][] = $value['visits'];
                        }
                    }
                    if($dataRow){
                        foreach ($dataRow as $key => $value) {
                            $nDataRow[$key] = array_sum($value);
                            $sum = $sum + array_sum($value);
                        }
                        $response['success']=true;
                        $response['data'] = $nDataRow;
                        $response['totals'] = $sum;
                    }
                } 
            } catch(Exception $e){
                $response['error'] = $e->getMessage();   
            }   
        }else {
            $response['error'] = $this->noDataError;
        }
        return response()->json(['response'=>$response], 200);
    } 
    public function getTopPageViews(Request $request) {
        $response['success']=false;
        include(app_path() . '/Functions/googleAnalytics.php');
        $viewID = AnalyticHelper::getGaViewById($request->view_id);
        $dataRow=$countRow=[];
        if($viewID){
            $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end);
            try {
                $data = AnalyticHelper::getPageViews($analytics,$viewID,$dates['start'],$dates['end']); 
                if($data){
                    $response['success']=true;
                    $response['data'] = $data;
                }
            } catch(Exception $e) {
                $response['error'] = $e->getMessage();   
            }   
        }else {
            $response['error'] = $this->noDataError;
        } 
        return response()->json(['response'=>$response], 200);
    } 
    public function getVisitsByDevice(Request $request){
        $response['success']=false;
        include(app_path() . '/Functions/googleAnalytics.php');
        $viewID = AnalyticHelper::getGaViewById($request->view_id);
        $dataRow=$countRow=[];
        if($viewID){
            $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end); 
            try {
                $data = AnalyticHelper::getClientVisitsByDevice($analytics,$viewID,$dates['start'],$dates['end']);
                if($data){
                    $counter=0;
                    foreach ($data['rows'] as $row) {
                        $countRow[$row[1]][] = $row[2];
                    } 
                    if($countRow){
                        $response['success']=true;
                        $dataRow['desktop'] = isset($countRow['desktop'])?array_sum($countRow['desktop']):0 ;
                        $dataRow['mobile'] = isset($countRow['mobile'])?array_sum($countRow['mobile']):0;
                        $dataRow['tablet'] = isset($countRow['tablet'])?array_sum($countRow['tablet']):0;
                        $response['data'] = $dataRow;
                        $response['totals'] = $dataRow['desktop'] + $dataRow['mobile'] + $dataRow['tablet'];
                    }
                }
            } catch(Exception $e) {
               $response['error'] = $e->getMessage();  
            }   
        }else {
            $response['error'] = $this->noDataError;
        } 
        return response()->json(['response'=>$response], 200);
    }
    public function getChannelTraffic(Request $request) {
        $response['success']=false; 
        include(app_path() . '/Functions/googleAnalytics.php');
        $viewID = AnalyticHelper::getGaViewById($request->view_id);
        $dataRow=$countRow=[];
        if($viewID){
            $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end); 
            try {
                $totalSum =0;
                $data = AnalyticHelper::getTrafficByChannel($analytics,$viewID,$dates['start'],$dates['end']);  
                if($data){
                    $counter=0;
                    foreach ($data['rows'] as $row) {
                        $countRow[$row[1]][] = $row[2];
                    }
                    if($countRow){
                        $response['success']=true;
                        foreach ($countRow as $key => $value) {
                            $dataRow[$key] = array_sum($value);
                            $totalSum = $totalSum + array_sum($value);
                        }
                        $response['data'] = $dataRow;
                        $response['totals'] = $totalSum;
                    }
                }   
            } catch (Exception $e) {
               $response['error'] = $e->getMessage();   
            }   
        } else {
            $response['error'] = $this->noDataError;
        }
        return response()->json(['response'=>$response], 200);
    }    
   
    // analystic
    public function dashboardAnalytics(){

    	/*$data = Analytics::fetchMostVisitedPages(Period::days(7));
    	$analyticsData = Analytics::fetchVisitorsAndPageViews(Period::months(1));
    	$refrerls = Analytics::fetchTopReferrers(Period::days(7), 10);
    	$topBrowsers = Analytics::fetchTopBrowsers(Period::days(7));
    	$users = Analytics::fetchUserTypes(Period::days(7));
    	$service = Analytics::getAnalyticsService();*/


        // $accountsObject = $this->analytics->management_accounts->listManagementAccounts();


        /*$analytics = $this->getView(61839672);  
    	$customQuery = $analytics->performQuery(
    		Period::days(7),
    		'ga:sessions',
    		[
    			'metrics' => 'ga:users, ga:sessions, ga:bounceRate, ga:pageviews, ga:newUsers',
    			'dimensions' => 'ga:yearMonth'
    		]
    	);
    	dump($customQuery);
    	dd($customQuery->totalsForAllResults);*/



        include(app_path() . '/Functions/googleAnalytics.php');

    	// dump($profile);
        foreach ($profile as $p) {
            // $analytics = $this->getView($p[0]);  
            $analytics = AnalyticHelper::getView($p[0]);
            $customQuery = $analytics->performQuery(
                Period::days(7),
                'ga:sessions',
                [
                    'metrics' => 'ga:users, ga:sessions, ga:bounceRate, ga:pageviews, ga:newUsers',
                    'dimensions' => 'ga:yearMonth'
                ]
            );    
            /*dump($p[2]);  
            dump($customQuery->totalsForAllResults);*/
        }

        // dd('stop...');
        return view('pages.dashboard-analytics');
    }
}
