<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AnalyticHelper;
use DB;

class PerformanceController extends Controller
{
   
   	public $noDataError = 'No Data Found';

    public function returnFirstElement($arr) {
        $return=[];
        $c=0;
        foreach ($arr as $key => $value) {
            if($c==0){
                $return['id'] = $key;
                $return['value']=$value;
            }
            $c++;
        }
        return $return;
    }

    public function getPerformance() {
    	$views = AnalyticHelper::getUserViews(Auth::user()->id);
        $viewArr=$views->toArray();
        $firstView = $this->returnFirstElement($viewArr);
   		return view('performances.index',compact('views','viewArr'));  
    }
    public function getPerformanceData(Request $request) {
        // dd($request);
    	$response['success']=false;
    	include(app_path() . '/Functions/googleAnalytics.php');
    	$viewID = AnalyticHelper::getGaViewById($request->view_id); 
    	$dataRow=[]; 
    	if($viewID){
    	    $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end);
    	    try {
                
    	        $data = AnalyticHelper::getClientDashboardData($analytics,$viewID,$dates['start'],$dates['end']);     
    	        if($data){ 
    	            $counter=0;
                    $totalArr=[];
                    $totalVisits = $totalPageViews = $totalBounceRate = $totalAvgTime = 0;
    	            foreach ($data['rows'] as $row) {
    	                $dataRow[$counter]['date'] = $row[2].'-'.$row[3].'-'.$row[1];
    	                $dataRow[$counter]['visits'] = $row[10];
    	                $dataRow[$counter]['pageViews'] = $row[9]; 
    	                $dataRow[$counter]['bounceRate'] = round($row[8]); 
    	                $dataRow[$counter]['avgTime'] = round($row[11]);   
                        $totalVisits = $totalVisits + $row[10];
                        $totalPageViews = $totalPageViews + $row[9];
                        $totalBounceRate = $totalBounceRate + $row[8];
                        $totalAvgTime = $totalAvgTime + $row[11];
    	                $counter++;
    	            }
                    $totalArr['totalVisits'] = $totalVisits;
                    $totalArr['totalPageViews'] = $totalPageViews;
                    $totalArr['totalBounceRate'] = $totalBounceRate;
                    $totalArr['totalAvgTime'] = $totalAvgTime;
                    $totalArr['avgTime'] = round($totalAvgTime/count($dataRow)) .' seconds'; 
                    $totalArr['avgBounceRate'] = round($totalBounceRate / count($dataRow)).'%'; 

    	            if($dataRow){
    	                $response['success']=true;
    	                $response['data'] = $dataRow;
    	                $response['duration'] = 'from '.date('M d,Y',strtotime($dates['start'])) .' to '.date('M d,Y',strtotime($dates['end']));
                        $response['totals'] = $totalArr;
    	            }
                    
    	        } 
    	    } catch (Exception $e) {
    	        $response['error'] = $e->getMessage();   
    	    } 
    	}else {
    	    $response['error'] = $this->noDataError;
    	} 
    	return response()->json(['response'=>$response], 200);
    }
    public function getDeviceData(Request $request) {
        $response['success']=false;
        include(app_path() . '/Functions/googleAnalytics.php');
        $viewID = AnalyticHelper::getGaViewById($request->view_id);
        $countRow=[];
        if($viewID){ 
            $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end); 
            try {
                $data = AnalyticHelper::getClientVisitsByDevice($analytics,$viewID,$dates['start'],$dates['end']);
                if($data){
                    $counter=0;
                    foreach ($data['rows'] as $row) {
                        $countRow[ date('m-d-Y',strtotime($row[0])) ][$row[1]] = $row[2];
                    }
                    if($countRow){
                        $totalDesktop = $totalMob = $totalTab = 0;
                        foreach ($countRow as $key => $value) {
                            if(!isset($value['tablet'])) {
                                $countRow[$key]['tablet']=0; 
                            }
                            if(!isset($value['mobile'])) {
                                $countRow[$key]['mobile']=0; 
                            }
                            if(!isset($value['desktop'])) {
                                $countRow[$key]['desktop']=0;  
                            }
                            $totalDesktop = $totalDesktop + $countRow[$key]['desktop']; 
                            $totalMob = $totalMob + $countRow[$key]['mobile']; 
                            $totalTab = $totalTab + $countRow[$key]['tablet']; 
                        }
                        $response['success']=true;
                        $response['data'] = $countRow;
                        $response['totals'] = ['desktop'=>$totalDesktop,'mobile'=>$totalMob,'tablet'=>$totalTab];
                    }
                }
            } catch(Exception $e) {
               $response['error'] = $e->getMessage();  
            }    
        }else {
            $response['error'] = $this->noDataError;
        } 
        return response()->json(['response'=>$response], 200);
    }
    public function getChannelData(Request $request) {
        $response['success']=false;
        include(app_path() . '/Functions/googleAnalytics.php');
        $viewID = AnalyticHelper::getGaViewById($request->view_id);
        $countRow=[];
        if($viewID){  
            $dates = AnalyticHelper::setGaQueryDates($request->start,$request->end); 
            try {
                $data = AnalyticHelper::getTrafficByChannel($analytics,$viewID,$dates['start'],$dates['end']);
                if($data){ 
                    $counter=0;
                    foreach ($data['rows'] as $row) {
                        $countRow[date('m-d-Y',strtotime($row[0]))][$row[1]] = $row[2];
                    }
                    $totalDirect = $totalOrganic = $totalReferral = $totalSocial = $totalPaid =0;
                    if($countRow){
                        foreach ($countRow as $key => $value) {
                            if(!isset($value['Direct'])) {
                                $countRow[$key]['Direct']=0; 
                            }
                            if(!isset($value['Organic Search'])) {
                                $countRow[$key]['Organic Search']=0; 
                            }
                            if(!isset($value['Referral'])) {
                                $countRow[$key]['Referral']=0; 
                            }
                            if(!isset($value['Social'])) {
                                $countRow[$key]['Social']=0; 
                            }
                            if(!isset($value['Paid Search'])) {
                                $countRow[$key]['Paid Search']=0; 
                            }

                            $totalDirect = $totalDirect + $countRow[$key]['Direct'];
                            $totalOrganic = $totalOrganic + $countRow[$key]['Organic Search'];
                            $totalReferral = $totalReferral + $countRow[$key]['Referral'];
                            $totalSocial = $totalSocial + $countRow[$key]['Social'];
                            $totalPaid= $totalPaid + $countRow[$key]['Paid Search'];
                        } 
                        $response['success']=true;
                        $response['data'] = $countRow;
                        $response['totals'] = ['direct'=>$totalDirect,'organic'=>$totalOrganic,'referral'=>$totalReferral,'social'=>$totalSocial,'paid'=>$totalPaid];
                    }
                } 
            } catch(Exception $e) {
               $response['error'] = $e->getMessage();  
            }    
        }else {
            $response['error'] = $this->noDataError;
        } 
        return response()->json(['response'=>$response], 200);
    }   


}
