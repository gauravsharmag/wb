<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Helpers\AnalyticHelper;
use App\Mail\NotifyClient;
use DB;
use Mail;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Log; 



class UsersController extends Controller
{

  public function test() {
    return view('users.test');   
  }

  //user List 
  public function listUser(){
    return view('pages.page-users-list');
  }
  //user view 
  public function viewUser(){
    return view('pages.page-users-view');
  }
  //user edit 
  public function editUser(){
    return view('pages.page-users-edit');
  }



  public function getUserEdit($id) {
    $states = DB::table('states')->pluck('name','id'); 
    $user = DB::table('users')->whereId($id)->first();
    $gaAccounts = AnalyticHelper::getGaAccounts();  
    $views = DB::table('user_views')->where('user_id',$id)->pluck('ga_account_id','ga_account_id')->toArray();
    $viewIds = array_values($views);
    $ids = implode(',',$viewIds);
    $keywords = DB::table('user_keywords')->whereUserId($id)->pluck('keyword','id')->toArray();
    return view('users.edit',compact('user','gaAccounts','viewIds','states','keywords'));  
  }
  public function postUserEdit(Request $request) { 
    // dd($request);
    $request->validate([   
        'ga_account'=>'required',
        'client_name'=>'required|max:90', 
        'contact_name'=>'required|max:90', 
        'phone'=>'required|digits:10', 
        'address'=>'required|max:255',
        'website_url'=>'nullable|url|max:255', 
        'email'=>'required|email|unique:users,id,'.$request->id,
        'state'=>'required', 
        'city'=>'required|max:30',
        'zip'=>'required|digits:5',
        'website_logo' => 'mimes:jpeg,png,jpg,gif|max:2048',
        'cid'=>'required'
    ]);
    $imageName=null;
    if($request->has('website_logo')){
        $image=$request->file('website_logo');
        $imageName=uniqid().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('/images/users'), $imageName); 
    }
    $updateArr=[
        'name'=>$request->client_name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'contact_name'=>$request->contact_name,
        'address'=>$request->address,
        'website_url'=>$request->website_url,
        'website_logo'=>$imageName,
        'cid'=>$request->cid,
        'updated_at'=>date('Y-m-d H:i:s')
    ];
    if($imageName){
      $updateArr['website_logo']=$imageName;
    } 
    $update = DB::table('users')->whereId($request->id)->update($updateArr);
    if($update){
      DB::table('user_views')->where('user_id',$request->id)->delete();
      foreach($request->ga_account as $ga){ 
          DB::table('user_views')->insert([
            'user_id'=>$request->id, 
            'ga_account_id'=>$ga, 
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')   
          ]);  
      }
      if($request->has('keywords')){
          $keyChunks = preg_split('/\R/', $request->keywords);
          if($keyChunks){
            DB::table('user_keywords')->where('user_id',$request->id)->delete(); 
            foreach ($keyChunks as $chunk) {
              $chunk = strlen($chunk) > 200 ? substr($chunk,0,200) : $chunk;
              DB::table('user_keywords')->insert([
                'user_id'=>$request->id,
                'keyword'=>$chunk,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')   
              ]);
            }
          }
      }
      Log::write(Auth::user()->name,'EditClient - '.$request->email,'Client edited with email '.$request->email.' by user '.Auth::user()->email.' at '.date('Y-m-d H:i:s') );  
      return redirect('/users')->with('success',__('Client updated successfully'));
    }
    return redirect()->back()->with('error',__('Sorry,client could not updated,please try again!')); 
  } 
  public function getListUser() {
    return view('users.list');
  }
  public function getListUserAjax(Request $request) {
    $typeArchive=[0];
    if($request->type==1){
      $typeArchive=[0,1];
    }
    $users = DB::table('users')
          ->where('role_id',2)
          ->whereIn('archived',$typeArchive)
          ->get();  
    return Datatables::of($users)  
            ->addColumn('action', function ($c) {    
                $str='<a href="/users/edit/'.$c->id.'" class="btn btn-xs btn-success" title="'.__('Edit').'"><i class="bx bxs-edit-alt"></i> </a>';     
                if(!$c->archived){
                  $str .='&nbsp;<a id="archiveUser" title="'.__('Archive').'"  onclick="archiveUser('.$c->id.');" href="javascript:;" class="btn btn-xs btn-warning"><i class="bx bxs-down-arrow-circle"></i> </a>';   
                }else {
                  $str .='&nbsp;<a id="activateUser" title="'.__('Activate').'"  onclick="unArchiveUser('.$c->id.');" href="javascript:;" class="btn btn-xs btn-info"><i class="bx bxs-checkbox-checked"></i> </a>';    
                }
                $str .='&nbsp;<a id="deleteUser" title="'.__('Delete').'"  onclick="deleteUser('.$c->id.');" href="javascript:;" class="btn btn-xs btn-danger"><i class="bx bx-trash-alt"></i> </a>'; 

                $str .='&nbsp;<a id="masqueradeClient" title="'.__('Masquerade').'"  onclick="masqueradeClient('.$c->id.');" href="javascript:;" class="btn btn-xs btn-primary"><i class="bx bx-user"></i> </a>'; 

                return $str;     
            })
            ->editColumn('created_at',function($c){
                return date('d-M-Y H:i:s',strtotime($c->created_at));
            })
            ->editColumn('role_id',function($c){
                if($c->role_id==1){
                  return "<span class='badge badge-danger'>ADMIN</span>";
                }elseif ($c->role_id==2) {
                  return "<span class='badge badge-success'>CLIENT</span>";
                }elseif ($c->role_id==3) {
                  return "<span class='badge badge-info'>SUPER ADMIN</span>";
                }
            })
            ->editColumn('archived',function($c){
                if($c->archived){
                  return "<span class='badge badge-danger'>YES</span>";
                }else {
                   return "<span class='badge badge-success'>NO</span>";
                }
            })
            ->escapeColumns([]) 
            ->make(true);  
  }
  public function getAddClient() {
    $states = DB::table('states')->pluck('name','id'); 
    $gaAccounts = AnalyticHelper::getGaAccounts(); 
    return view('users.add',compact('gaAccounts','states')); 
  }
  public function postAddClient(Request $request) { 
    $request->validate([   
        'ga_account'=>'required',
        'client_name'=>'required|max:90', 
        'contact_name'=>'required|max:90', 
        'phone'=>'required|digits:10', 
        'address'=>'required|max:255',
        'website_url'=>'nullable|url|max:255', 
        'email'=>'required|email|unique:users|max:100',
        'state'=>'required',
        'city'=>'required|max:30',
        'zip'=>'required|digits:5',
        'website_logo' => 'mimes:jpeg,png,jpg,gif|max:2048',
        'cid'=>'required'
    ]);
    $imageName=null;
    if($request->has('website_logo')){
        $image=$request->file('website_logo');
        $imageName=uniqid().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('/images/users'), $imageName); 
    }
    $client = DB::table('users')->insertGetId([
        'name'=>$request->client_name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'contact_name'=>$request->contact_name,
        'address'=>$request->address,
        'website_url'=>$request->website_url,
        'website_logo'=>$imageName,
        // 'ga_account_id'=>$request->ga_account,
        'state_id'=>$request->state,
        'city'=>$request->city,
        'zip'=>$request->zip, 
        'cid'=>$request->cid, 
        'created_at'=>date('Y-m-d H:i:s'),
        'updated_at'=>date('Y-m-d H:i:s')
    ]);

    if($client){
      foreach($request->ga_account as $ga){ 
        DB::table('user_views')->insert([
            'user_id'=>$client,
            'ga_account_id'=>$ga, 
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')   
        ]);  
      }

      if($request->has('keywords')){
          $keyChunks = preg_split('/\R/', $request->keywords);
          if($keyChunks){
            foreach ($keyChunks as $chunk) {
              $chunk = strlen($chunk) > 200 ? substr($chunk,0,200) : $chunk;
              DB::table('user_keywords')->insert([
                'user_id'=>$client,
                'keyword'=>$chunk,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')   
              ]);
            }
          }
      }
      Log::write(Auth::user()->name,'AddClient - '.$request->email,'Client added with email '.$request->email.' by user '.Auth::user()->email.' at '.date('Y-m-d H:i:s') );  
      $this->notifyClient($client); 
      return redirect('/users')->with('success',__('Client added successfully'));
    }
    return redirect()->back()->with('error',__('Sorry,client could not added,please try again!'));
  }
  public function notifyClient($client) {
    if($client){
      $info = DB::table('users')->whereId($client)->first();
      if($info){
          $token=uniqid();
          DB::table('password_resets')->insert([
              'user_id'=>$info->id, 
              'email'=>$info->email,
              'token'=>$token, 
              'created_at'=>date('Y-m-d H:i:s')
          ]);
          $url=url('/').'/users/reset-password/'.$token; 
          $site_title = env('SITE_TITLE'); 
          if(filter_var($info->email, FILTER_VALIDATE_EMAIL)) {
              try {
                $msg = 'Your account has been successfully created on '.env('SITE_TITLE').'. Please click on the button below to set your account password.';
                Mail::to($info->email)->send(new NotifyClient($info->name,'New Account Registration',$url,$msg)); 
                Log::write('---','RegistrationEmail - '.$info->email,'Registration Email sent to '.$info->email.' at '.date('Y-m-d H:i:s'));      
              } catch(Exception $e) { 
                return redirect()->back()->with('error',__('Sorry,mail could not be sent,please try again!'));
              } 
          }
      }
    }
  }


  public function getEditProfile() { 
    $states = DB::table('states')->pluck('name','id'); 
    $profile = DB::table('users')->whereId(Auth::user()->id)->first();
    $keywords = DB::table('user_keywords')->whereUserId(Auth::user()->id)->pluck('keyword','id')->toArray();
    return view('users.profile',compact('profile','states','keywords'));  
  }
  public function postEditProfile(Request $request) {
    // dd($request);
    $request->validate([   
        'client_name'=>'required|max:90', 
        'phone'=>'required|digits:10', 
        'address'=>'required|max:255', 
        'email'=>'required|unique:users,id,'.Auth::user()->id,
        'website_logo' => 'mimes:jpeg,png,jpg,gif|max:2048',
        'website_url'=>'nullable|url|max:255', 
        'state'=>'required',
        'city'=>'required|max:30',
        'zip'=>'required|digits:5', 
    ]);

    $imageName=null;
    if($request->has('website_logo')){
        $image=$request->file('website_logo');
        $imageName=uniqid().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('/images/users'), $imageName); 
    }
    $updateArr=[
        'name'=>$request->client_name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'address'=>$request->address,
        'state_id'=>$request->state,
        'city'=>$request->city,
        'zip'=>$request->zip,
        'website_url'=>$request->website_url,
        'updated_at'=>date('Y-m-d H:i:s')
    ];
    if($imageName){ 
      $updateArr['website_logo']=$imageName;
    }
    $update = DB::table('users')->whereId($request->id)->update($updateArr);
    if($update){
      if($request->has('keywords')){
          $keyChunks = preg_split('/\R/', $request->keywords);
          if($keyChunks){
            DB::table('user_keywords')->whereUserId(Auth::user()->id)->delete();
            foreach ($keyChunks as $chunk) {
              $chunk = strlen($chunk) > 200 ? substr($chunk,0,200) : $chunk;
              DB::table('user_keywords')->insert([
                'user_id'=>Auth::user()->id,
                'keyword'=>$chunk, 
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')   
              ]);
            }
          }
      }
      return redirect()->back()->with('success',__('Profile updated successfully'));
    }
    return redirect()->back()->with('error',__('Sorry,profile could not updated,please try again!'));  
  }
  public function getChangePassword() {
    return view('users.change_password');
  }
  public function postChangePassword(Request $r){
      $this->validate($r, [
            'password'=> 'required', 
            'confirm_password' => 'required|same:password'
      ],[ 
            'password.required'=>__('Please enter password'),
            'confirm_password.required'=>__('Please enter confirm password'),
           ] 
      );  
      $user=User::find(Auth::user()->id); 
      $user->password=Hash::make($r->password);  
      if($user->save()) {
        return redirect()->back()->with('success',__('Password updated successfully'));
      } else {
        return redirect()->back()->with('error',__('Sorry,password could not be updated,please try again!'));
      } 
  }
  public function archiveUser($user,$type=0) {
    if($user){
      $archive = DB::table('users')->whereId($user)->update([
          'archived'=>$type,
          'updated_at'=>date('Y-m-d H:i:s')
      ]);  
      if($archive){
        $logType = 'ArchiveUser';
        $msgType = 'Archived';
        $email = DB::table('users')->whereId($user)->value('email'); 
        if($type==0){ 
          $logType = 'ActivateUser';
          $msgType = 'Activated';
        } 
        Log::write(Auth::user()->name,$logType.' - '.$email,$email.' '.$msgType.'  at '.date('Y-m-d H:i:s').' by user '.Auth::user()->email);
        return redirect()->back()->with('success',__('Request completed successfully')); 
      }
      return redirect()->back()->with('error',__('Sorry,request could not completed,please try again!'));
    } 
    return redirect()->back()->with('error',__('Invalid request!')); 
  }
  public function deleteUser($user) {
    if($user){
      $userInfo = DB::table('users')->whereId($user)->first();
      if($userInfo){
        if(DB::table('users')->whereId($user)->delete()){
          Log::write(Auth::user()->name,'DeleteUser - '.$userInfo->email,'User deleted with email '.$userInfo->email.' by user '.Auth::user()->email.' at '.date('Y-m-d H:i:s') );  
          return redirect()->back()->with('success',__('User deleted successfully'));
        }
      }
      return redirect()->back()->with('error',__('Sorry,user could not deleted,please try again!'));
    }
  }


  public function masqueradeClient($client) {
    if($client) {
      session(['back_id' => Auth::user()->id ]); 
      $newUser=User::where('id',$client)->first();
      if(Auth::loginUsingId($newUser->id)) {
        if(Auth::user()->role_id==2) {
          return redirect('/client-dashboard')->with('success',__('Account switched successfully'));
        } 
      } else {
        session()->pull('back_id');
        return redirect()->back()->with('error',__('Sorry, Error in switchning account, please try again!'));
      }
    }
  } 
  public function masqueradeClientBack() {
    if(session()->has('back_id')) {
      if(Auth::loginUsingId(session('back_id'))) {
        session()->pull('back_id');
        if(Auth::user()->role_id==1 || Auth::user()->role_id==3) { 
          return redirect('/dashboard')->with('success',__('Account switched successfully'));     
        }
      } else {  
        session()->pull('back_id');
        return redirect()->back()->with('error',__('Sorry, Error in switchning account, please try again!'));     
      }  
    }else {
        return redirect()->back()->with('error',__('Sorry,no id found to go back,please try again!'));     
    }
  }

  public function getManageAdmins() {
    return view('users.manage_admins'); 
  }
  public function getManageAdminsAjax(Request $request) {
    $typeArchive=[0];
    if($request->type==1){
      $typeArchive=[0,1];
    }
    $users = DB::table('users')
          ->where('role_id','!=',2)
          ->where('id','!=',Auth::user()->id)
          ->whereIn('archived',$typeArchive)
          ->get();  
    return Datatables::of($users) 
            ->addColumn('action', function ($c) {    
                $str='';
                $str='<a href="/users/edit-admin/'.$c->id.'" class="btn btn-xs btn-success" title="'.__('Edit').'"><i class="bx bxs-edit-alt"></i> </a>';     
                if(!$c->archived){ 
                  $str .='&nbsp;<a id="archiveUser" title="'.__('Archive').'"  onclick="archiveUser('.$c->id.');" href="javascript:;" class="btn btn-xs btn-warning"><i class="bx bx-trash-alt"></i> </a>';   
                }else {
                  $str .='&nbsp;<a id="activateUser" title="'.__('Activate').'"  onclick="unArchiveUser('.$c->id.');" href="javascript:;" class="btn btn-xs btn-info"><i class="bx bxs-checkbox-checked"></i> </a>';    
                }
                $str .='&nbsp;<a id="deleteUser" title="'.__('Delete').'"  onclick="deleteUser('.$c->id.');" href="javascript:;" class="btn btn-xs btn-danger"><i class="bx bx-trash-alt"></i> </a>'; 
                return $str;     
            }) 
            ->editColumn('archived',function($c){ 
                if($c->archived){
                  return "Archived";
                }else {
                   return "Activate";
                }
            })
            ->editColumn('role_id',function($c){ 
                if($c->role_id==1){
                  return "Admin";
                }else {
                  return "Super Admin";
                }
            })
            ->escapeColumns([]) 
            ->make(true); 
  }

  public function getAddAdmin() {
    $states = DB::table('states')->pluck('name','id'); 
    return view('users.add_admin',compact('states')); 
  }
  public function postAddAdmin(Request $request) {

    $request->validate([    
        'client_name'=>'required|max:90', 
        'phone'=>'required|digits:10', 
        'email'=>'required|email|unique:users|max:100',
        'website_logo' => 'mimes:jpeg,png,jpg,gif|max:2048'
    ]);
    $imageName=null;
    if($request->has('pic')){
      $image=$request->file('pic');
      $imageName=uniqid().'.'.$image->getClientOriginalExtension();
      $image->move(public_path('/images/users'), $imageName); 
    }
    $addArr=[
        'role_id'=>$request->admin_type,
        'name'=>$request->client_name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'address'=>$request->address,
        'state_id'=>$request->state,
        'city'=>$request->city,
        'zip'=>$request->zip,
        'created_at'=>date('Y-m-d H:i:s'),
        'updated_at'=>date('Y-m-d H:i:s')
    ];
    if($imageName){ 
      $addArr['website_logo']=$imageName;  
    } 
    
    $admin = DB::table('users')->insertGetId($addArr);
    if($admin){
      Log::write(Auth::user()->name,'AddAdmin - '.$request->email,'Admin added with email '.$request->email.' by user '.Auth::user()->email.' at '.date('Y-m-d H:i:s') );  
      $this->notifyClient($admin); 
      return redirect('/users/admins')->with('success',__('Admin added successfully'));
    }
    return redirect()->back()->with('error',__('Sorry,admin could not added,please try again!'));
  } 

  public function getAdminEdit($id) {
    $user = DB::table('users')->whereId($id)->first();
    $states = DB::table('states')->pluck('name','id'); 
    return view('users.edit_admin',compact('user','states')); 
  }
  public function postAdminEdit(Request $request) {
    $request->validate([   
        'client_name'=>'required|max:90', 
        'phone'=>'required|digits:10', 
        'email'=>'required|max:100|unique:users,id,'.$request->id,
        'website_logo' => 'mimes:jpeg,png,jpg,gif|max:2048'
    ]);
    $updateArr=[
        'role_id'=>$request->admin_type,
        'name'=>$request->client_name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'updated_at'=>date('Y-m-d H:i:s')
    ];
    $update = DB::table('users')->whereId($request->id)->update($updateArr);
    if($update){
      Log::write(Auth::user()->name,'EditAdmin - '.$request->email,'Admin edited with email '.$request->email.' by user '.Auth::user()->email.' at '.date('Y-m-d H:i:s') );  
      return redirect('/users/admins')->with('success',__('Admin updated successfully'));
    }
    return redirect()->back()->with('error',__('Sorry,admin could not updated,please try again!'));  
  }

  public function getEditProfileAdmin() {
    $states = DB::table('states')->pluck('name','id'); 
    $profile = DB::table('users')->whereId(Auth::user()->id)->first();
    return view('users.admin_profile',compact('profile','states')); 
  }

  public function postEditProfileAdmin(Request $request) {
    // dd($request);
    $request->validate([   
        'name'=>'required|max:90', 
        'phone'=>'required|digits:10', 
        'email'=>'required|max:100|unique:users,id,'.$request->id,
        'website_logo' => 'mimes:jpeg,png,jpg,gif|max:2048',
    ]); 
    $updateArr=[
        'name'=>$request->name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'address'=>$request->address,
        'state_id'=>$request->state,
        'city'=>$request->city,
        'zip'=>$request->zip,
        'updated_at'=>date('Y-m-d H:i:s')
    ];
    $imageName=null;
    if($request->has('pic')){
      $image=$request->file('pic');
      $imageName=uniqid().'.'.$image->getClientOriginalExtension();
      $image->move(public_path('/images/users'), $imageName); 
    }
    if($imageName){ 
      $updateArr['website_logo']=$imageName;
    } 
    $update = DB::table('users')->whereId($request->id)->update($updateArr);
    if($update){  
      return redirect()->back()->with('success',__('Profile updated successfully'));
    } 
    return redirect()->back()->with('error',__('Sorry,profile could not updated,please try again!'));  


  }
 

 
}
