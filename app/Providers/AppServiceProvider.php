<?php

namespace App\Providers;

use DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $options=DB::table('settings')->whereIn('option_name',['logo','website_title'])->pluck('option_value'); 
        $siteLogo=$siteTitle='';
        if($options[1]){
           $siteLogo=$options[1];    
        }
        if($options[0]){
           $siteTitle=$options[0];
        } 
        // dd($siteLogo);
        View::share('siteLogo',$siteLogo); 
        View::share('siteTitle',$siteTitle);  
    }
}
