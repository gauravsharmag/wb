<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupportMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $msg;
    public $siteTitle;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$msg,$siteTitle) {
        $this->name = $name;
        $this->email = $email;
        $this->msg = $msg;
        $this->siteTitle = $siteTitle;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject(__('Support Request'))
                ->markdown('emails.support')  
                ->with('name',$this->name)
                ->with('email',$this->email)
                ->with('msg',$this->msg)
                ->with('site_title',$this->siteTitle);
    }
}
