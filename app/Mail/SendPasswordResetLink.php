<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendPasswordResetLink extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $subject; 
    public $msg; 
    public $link; 
    public $site_title; 


    public function __construct($subject,$msg,$link,$site_title) {
        $this->subject=$subject;
        $this->msg=$msg;
        $this->link=$link;
        $this->site_title=$site_title;
    }

    /** 
     * Build the message.
     *
     * @return $this
     */
    public function build() { 
        return $this->subject(__($this->subject))
                ->markdown('emails.forgot_password') 
                ->with('msg',$this->msg)
                ->with('link',$this->link)
                ->with('site_title',$this->site_title); 
    } 
}
