<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyClient extends Mailable
{
    use Queueable, SerializesModels;


    public $name;
    public $subject;
    public $link;
    public $msg;



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$subject,$link,$msg){
        $this->name = $name;
        $this->subject = $subject;
        $this->link = $link;
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject(__($this->subject))
                ->markdown('emails.add_client') 
                ->with('name',$this->name)
                ->with('msg',$this->msg)
                ->with('link',$this->link)
                ->with('site_title',env('SITE_TITLE'));
    }
}
