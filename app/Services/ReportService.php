<?php 
namespace App\Services;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AnalyticHelper;
use DB;

class ReportService 
{
 
	public function getGaReportData($analytics,$start,$end,$userID) {  
		$rsp=[];
		$viewID = $this->getViewIdByUserId($userID);
		if($viewID){
			$dates = AnalyticHelper::setGaQueryDates($start,$end);
			$rsp['dashboardData'] = AnalyticHelper::getClientDashboardData($analytics,$viewID,$dates['start'],$dates['end']);
			$rsp['socialData'] =  AnalyticHelper::getSocialTraffic($analytics,$viewID,$dates['start'],$dates['end']); 
			$rsp['channelData'] = AnalyticHelper::getTrafficByChannel($analytics,$viewID,$dates['start'],$dates['end']);  
			$rsp['deviceData'] =  AnalyticHelper::getClientVisitsByDevice($analytics,$viewID,$dates['start'],$dates['end']);
			$rsp['topPageViews'] = AnalyticHelper::getPageViews($analytics,$viewID,$dates['start'],$dates['end']); 
			return $rsp; 
		}
	}

	public function getKeywordsData($user){  
        return DB::table('user_keywords')
			->select('keyword','keyword_rank','url')
			->whereUserId($user)
			->orderByRaw('ISNULL(keyword_rank), keyword_rank ASC')
			// ->orderBy('keyword_rank','asc') 
			->get();
	}


	public function getViewIdByUserId($userId) {
		$view =DB::table('user_views')
			->select('ga_accounts.ga_view_id')
			->leftJoin('ga_accounts','ga_accounts.id','=','user_views.ga_account_id')
			->where('user_views.user_id',$userId)
			->first();
		if($view){ 
			return $view->ga_view_id;
		}
		return false;
	}

}