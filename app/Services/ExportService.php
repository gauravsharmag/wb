<?php 
namespace App\Services;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class ExportService  
{
 	
 	public function exportData(&$data) {
 		
 	}
	public function exportGaReportCsv($reportType='csv',$data) {
		if($reportType=='csv'){
			$file = fopen('php://output','w'); 
			fputcsv($file,array('Visits',''));
			fputcsv($file,array('Date','Visits'));
			foreach ($data['dashboardExport']['visits']['date'] as $key => $line) {
				$add=[$line,$data['dashboardExport']['visits']['views'][$key] ];		
				fputcsv($file,$add);
			}
			$this->addBlankRow($file);
	        fputcsv($file,array('PageViews',''));
	        fputcsv($file,array('Date','Views'));
	        foreach ($data['dashboardExport']['pageViews']['date'] as $key => $line) {
	        	$add=[$line,$data['dashboardExport']['pageViews']['views'][$key] ];		
	        	fputcsv($file,$add);
	        } 
	       	$this->addBlankRow($file);
	        fputcsv($file,array('AverageTime','')); 
	        fputcsv($file,array('Date','Time'));
	        foreach ($data['dashboardExport']['avgTime']['date'] as $key => $line) {
	        	$add=[$line,$data['dashboardExport']['avgTime']['views'][$key] ];		
	        	fputcsv($file,$add); 
	        }
	        $this->addBlankRow($file);
	        fputcsv($file,array('BounceRate',''));
	        fputcsv($file,array('Date','Bouncerate'));
	        foreach ($data['dashboardExport']['bounceRate']['date'] as $key => $line) {
	        	$add=[$line,$data['dashboardExport']['bounceRate']['views'][$key] ];		
	        	fputcsv($file,$add);
	        }  

           	$this->addBlankRow($file);
            fputcsv($file,array('Social/Referral',''));
            fputcsv($file,array('Date','Visits'));
            foreach ($data['socialExport']['data'] as $key => $line) {
            	$add=[$key,$line ];		
            	fputcsv($file,$add);
            } 
           	$this->addBlankRow($file);
            fputcsv($file,array('ChannelData',''));
            fputcsv($file,array('Channel','Data'));
            foreach ($data['channelExport']['data'] as $key => $line) {
            	$add=[$key,$line ];		
            	fputcsv($file,$add);
            }  
            $this->addBlankRow($file);
            fputcsv($file,array('DeviceData',''));
            fputcsv($file,array('Device','Data'));
    		foreach ($data['deviceExport']['data'] as $key => $line) {
    			$add=[$key,$line ];		
    			fputcsv($file,$add);
    		}
    		if($data['topPageViews']){ 
    		   $this->addBlankRow($file);
    		   fputcsv($file,array('TopPageViews',''));
    		   fputcsv($file,array('PageTitle','PageUrl','Views')); 
    		   foreach ($data['topPageViews']['rows'] as $page ) {
    		   	$add=[$page[1],$page[0],$page[2]];
    		   	fputcsv($file,$add);	
    		   }
    		}

    		fclose($file); 
    		header('Content-type: application/csv');
    		header('Content-Disposition: attachment; filename=data.csv'); 
    		header('Pragma: no-cache');
		} 
	} 
	public function exportKeywordsCsv($data) { 
		if($data){  
			$file = fopen('php://output','w'); 
			fputcsv($file,array('Keywords',''));
			fputcsv($file,array('Keyword','Rank','Url'));
			foreach ($data as $key ) {
				$add=[$key->keyword,$key->keyword_rank?$key->keyword_rank:'--',$key->url ];		
				fputcsv($file,$add);	
			} 
			fclose($file); 
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename=keywords.csv');  
			header('Pragma: no-cache');
		}
	}

	public function exportFullReportCsv($data) {
		$file = fopen('php://output','w'); 
		fputcsv($file,array('Visits',''));
		fputcsv($file,array('Date','Visits'));
		foreach ($data['exportGaData']['dashboardExport']['visits']['date'] as $key => $line) {
			$add=[$line,$data['exportGaData']['dashboardExport']['visits']['views'][$key] ];		
			fputcsv($file,$add);
		}
		$this->addBlankRow($file);
        fputcsv($file,array('PageViews',''));
        fputcsv($file,array('Date','Views'));
        foreach ($data['exportGaData']['dashboardExport']['pageViews']['date'] as $key => $line) {
        	$add=[$line,$data['exportGaData']['dashboardExport']['pageViews']['views'][$key] ];		
        	fputcsv($file,$add);
        } 
       	$this->addBlankRow($file);
        fputcsv($file,array('AverageTime','')); 
        fputcsv($file,array('Date','Time'));
        foreach ($data['exportGaData']['dashboardExport']['avgTime']['date'] as $key => $line) {
        	$add=[$line,$data['exportGaData']['dashboardExport']['avgTime']['views'][$key] ];		
        	fputcsv($file,$add); 
        }
        $this->addBlankRow($file);
        fputcsv($file,array('BounceRate',''));
        fputcsv($file,array('Date','Bouncerate'));
        foreach ($data['exportGaData']['dashboardExport']['bounceRate']['date'] as $key => $line) {
        	$add=[$line,$data['exportGaData']['dashboardExport']['bounceRate']['views'][$key] ];		
        	fputcsv($file,$add);
        }  

       	$this->addBlankRow($file);
        fputcsv($file,array('Social/Referral',''));
        fputcsv($file,array('Date','Visits'));
        foreach ($data['exportGaData']['socialExport']['data'] as $key => $line) {
        	$add=[$key,$line ];		
        	fputcsv($file,$add); 
        } 
       	$this->addBlankRow($file);
        fputcsv($file,array('ChannelData',''));
        fputcsv($file,array('Channel','Data'));
        foreach ($data['exportGaData']['channelExport']['data'] as $key => $line) {
        	$add=[$key,$line ];		
        	fputcsv($file,$add);
        }  
        $this->addBlankRow($file);
        fputcsv($file,array('DeviceData',''));
        fputcsv($file,array('Device','Data'));
		foreach ($data['exportGaData']['deviceExport']['data'] as $key => $line) {
			$add=[$key,$line ];		
			fputcsv($file,$add);
		}
		if($data['exportGaData']['topPageViews']){ 
		   $this->addBlankRow($file);
		   fputcsv($file,array('TopPageViews',''));
		   fputcsv($file,array('PageTitle','PageUrl','Views')); 
		   foreach ($data['exportGaData']['topPageViews']['rows'] as $page ) {
		   	$add=[$page[1],$page[0],$page[2]];
		   	fputcsv($file,$add);	
		   }
		}

		$this->addBlankRow($file);
		fputcsv($file,array('Keywords',''));
		fputcsv($file,array('Keyword','Rank','Url'));
		foreach ($data['exportKeywordData'] as $key ) {
			$add=[$key->keyword,$key->keyword_rank?$key->keyword_rank:'--',$key->url ];		
			fputcsv($file,$add);	
		} 

		fclose($file); 
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename=data.csv'); 
		header('Pragma: no-cache');
	}

	public function exportKeywordsCsv($data) {
		
	}

	public function addBlankRow(&$file) {
		fputcsv($file,array('',''));
	    fputcsv($file,array('',''));
	}

}