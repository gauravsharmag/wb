<?php 
namespace App\Services;

use Config;
use Illuminate\Support\Str;

use Spatie\Analytics\Analytics;
use Spatie\Analytics\AnalyticsClientFactory;
use Illuminate\Support\Facades\Auth;
use DB;

class AnalyticService 
{

	public function getDashboardExportData($data) { 
		$response=[];	
		if($data){ 
		    $counter=0;
		    foreach ($data['rows'] as $row) {
		        $response['visits']['date'][] = $row[2].'-'.$row[3].'-'.$row[1];
		        $response['visits']['views'][] = $row[10];
		        $response['pageViews']['date'][] = $row[2].'-'.$row[3].'-'.$row[1];
		        $response['pageViews']['views'][] =  $row[9];
		        $response['avgTime']['date'][] = $row[2].'-'.$row[3].'-'.$row[1];
				$response['avgTime']['views'][] = round($row[11]);
				$response['bounceRate']['date'][] = $row[2].'-'.$row[3].'-'.$row[1];
				$response['bounceRate']['views'][] = round($row[8]); 
		        $counter++;
		    }
		}
		return $response;  
	}
	public function getSocialExportData($data) {
		$response=$countRow=[];	
		if($data){
			$counter=0;
			foreach ($data['rows'] as $row) {
			    if($row[4] !='(direct)'){
			        $countRow[$counter]['date'] = $row[2].'-'.$row[3].'-'.$row[1];
			        $countRow[$counter]['visits'] = $row[5];
			        $counter++;
			    }
			}
			if($countRow){
			    $response['success']=true;
			    foreach ($countRow as $value) {
			        $dataRow[$value['date']][] = $value['visits'];
			    }
			}
			if($dataRow){
			    foreach ($dataRow as $key => $value) {
			        $nDataRow[$key] = array_sum($value);
			    }
			    $response['data'] = $nDataRow;
			}
		}
		return $response;
	}
	public function getChannelExportData($data) {
		$response=[];
		if($data){ 
		    $counter=0;
		    foreach ($data['rows'] as $row) {
		        $countRow[$row[1]][] = $row[2];
		    }
		    if($countRow){
		        $response['success']=true;
		        foreach ($countRow as $key => $value) {
		            $dataRow[$key] = array_sum($value);
		        }
		        $response['data'] = $dataRow;
		    }
		}
		return $response; 
	}
	public function getDeviceExportData($data) {
		$response=$countRow=[];
		if($data){
		    $counter=0;
		    foreach ($data['rows'] as $row) {
		        $countRow[$row[1]][] = $row[2];
		    }
		    if($countRow){
		        $response['success']=true;
		        $dataRow['desktop'] = isset($countRow['desktop'])?array_sum($countRow['desktop']):0;
		        $dataRow['mobile'] = isset($countRow['mobile'])?array_sum($countRow['mobile']):0;
		        $dataRow['tablet'] = isset($countRow['tablet'])?array_sum($countRow['tablet']):0;
		        $response['data'] = $dataRow;
		    }
		}
		return $response;
	}

	public function exportPerformanceData($data) {
		$response=[];
		if($data){ 
            $counter=0;
            foreach ($data['rows'] as $row) {
                $response['visits']['date'][] = $row[2].'-'.$row[3].'-'.$row[1];
                $response['visits']['views'][] = $row[10];
                $response['pageViews']['date'][] = $row[2].'-'.$row[3].'-'.$row[1];
                $response['pageViews']['views'][] =  $row[9];
                $response['avgTime']['date'][] = $row[2].'-'.$row[3].'-'.$row[1];
        		$response['avgTime']['views'][] = round($row[11]);
        		$response['bounceRate']['date'][] = $row[2].'-'.$row[3].'-'.$row[1];
        		$response['bounceRate']['views'][] = round($row[8]); 
                $counter++;
            }
        }
        return $response;
	}

	public function exportPerformanceDevice($data) {
		$response=$countRow=[];
		$newArr=$desktopArr=$mobArr=$tabArr=[];
		if($data){
		    $counter=0;
		    foreach ($data['rows'] as $row) {
		        $countRow[ date('m-d-Y',strtotime($row[0])) ][$row[1]] = $row[2];
		    }
		    if($countRow){
		        foreach ($countRow as $key => $value) {
		            if(!isset($value['tablet'])) {
		                $countRow[$key]['tablet']=0; 
		            }
		            if(!isset($value['mobile'])) {
		                $countRow[$key]['mobile']=0; 
		            }
		            if(!isset($value['desktop'])) {
		                $countRow[$key]['desktop']=0;  
		            }
		        }
				foreach ($countRow as $key => $value) {
					$desktopArr[$key]=$value['desktop'];
					$mobArr[$key]=$value['mobile'];
					$tabArr[$key]=$value['tablet'];
				}		   
		        $response['desktop'] = $desktopArr;
		        $response['mobile'] = $mobArr;
		        $response['tab'] = $tabArr;
		    }
		}
		return $response;
	}

	public function exportPerformanceChannel($data) { 
		$response = $countRow =[];
		$directArr=$organicArr=$referalArr=$socialArr=$paidArr=[];
		if($data){ 
		    $counter=0;
		    foreach ($data['rows'] as $row) {
		        $countRow[date('m-d-Y',strtotime($row[0]))][$row[1]] = $row[2];
		    }
		    if($countRow){
		        foreach ($countRow as $key => $value) {
		            if(!isset($value['Direct'])) {
		                $countRow[$key]['Direct']=0; 
		            }
		            if(!isset($value['Organic Search'])) {
		                $countRow[$key]['Organic Search']=0; 
		            }
		            if(!isset($value['Referral'])) {
		                $countRow[$key]['Referral']=0; 
		            }
		            if(!isset($value['Social'])) {
		                $countRow[$key]['Social']=0; 
		            }
		            if(!isset($value['Paid Search'])) {
		                $countRow[$key]['Paid Search']=0; 
		            }
		        } 
        		foreach ($countRow as $key => $value) {
        			$directArr[$key]=$value['Direct'];
        			$organicArr[$key]=$value['Organic Search'];
        			$referalArr[$key]=$value['Referral'];
        			$socialArr[$key]=$value['Social'];
        			$paidArr[$key]=$value['Paid Search'];
        		}		   
                $response['direct'] = $directArr;
                $response['organic'] = $organicArr;
                $response['referral'] = $referalArr;
                $response['social'] = $socialArr;
                $response['paid'] = $paidArr;
		    }
		} 
		return $response;
	} 


 
}