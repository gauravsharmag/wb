<?php

// Load the Google API PHP Client Library.
require_once __DIR__.'/../../vendor/autoload.php';
require('MyBusiness.php'); 
$analytics = initializeAnalytics();


// $reviews = initReview();


// $profile = getAllProfiles($analytics);


function initReview() {
     

    $KEY_FILE_LOCATION = __DIR__ .'/../analytics/agoralytics-test-24f6744dea7f.json';  

    /*$client = new Google_Client();
    $client->setApplicationName("Agoralytics Reporting");
    $client->setAuthConfig($KEY_FILE_LOCATION);
    $client->setScopes(['https://www.googleapis.com/auth/plus.business.manage']);
    $client->setAccessType("offline");
    $client->setApprovalPrompt("force");
    */



    $client = new Google_Client();
    $client->useApplicationDefaultCredentials();
    $client->setAuthConfig($KEY_FILE_LOCATION);
    $client->addScope("https://www.googleapis.com/auth/plus.business.manage");

    //require_once('Google_Service_MyBusiness.php');
    $mybusinessService = new Google_Service_MyBusiness($client);

    $accounts = $mybusinessService->accounts;
    $accountsList = $accounts->listAccounts()->getAccounts();
    echo "<pre>";
    foreach ($accountsList as $accKey => $account) {
        var_dump('$account->name', $account->name);

        $locations = $mybusinessService->accounts_locations;
        $locationsList = $locations->listAccountsLocations($account->name)->getLocations();
        var_dump('$locationsList', $locationsList);


        // Final Goal of my Code
        if (empty($locationsList)===false) {
            foreach ($locationsList as $locKey => $location) {
                $reviews = $mybusinessService->accounts_locations_reviews;
                $listReviewsResponse = $reviews->listAccountsLocationsReviews($location->name);
                $reviewsList = $listReviewsResponse->getReviews();
                var_dump('$reviewsList', $reviewsList); 
            }
        }
    }

    die;

    $httpClient = $client->authorize();

    // make an HTTP request
    $response = $httpClient->get('https://www.googleapis.com/plus/v1/people/me');


    $mybusinessService = new Google_Service_Mybusiness($client);

    // For testing purposes, selects the very first account in the accounts array
     
    $accounts = $mybusinessService->accounts;
     
    $accountsList = $accounts->listAccounts()->getAccounts();
     dump($accountsList);
    $account = $accountsList[0];
    dump($account->name);
     
    // For testing purposes, selects the very first location in the locations array
     
    $locations = $mybusinessService->accounts_locations;
    dump($locations);
    $locationsList = $locations->listAccountsLocations($account->name)->getLocations();
     dump($locationsList);

    //$location = $locationsList[0];
     
    // Lists all reviews for the specified location
     
    $reviews = $mybusinessService->accounts_locations_reviews;
     
    /*$listReviewsResponse = $reviews->listAccountsLocationsReviews($location->name);
     
    $reviewsList = $listReviewsResponse->getReviews();*/
    
    dd($reviews);


}


function initializeAnalytics()
{
    // Creates and returns the Analytics Reporting service object.

    // Use the developers console and download your service account
    // credentials in JSON format. Place them in this directory or
    // change the key file location if necessary.
    $KEY_FILE_LOCATION = __DIR__ .'/../analytics/agoralytics-test-24f6744dea7f.json';  

    $client = new Google_Client();
    $client->setApplicationName("Agoralytics Reporting");
    $client->setAuthConfig($KEY_FILE_LOCATION);
    $client->setScopes(['https://www.googleapis.com/auth/plus.business.manage']);


    /*$gmbService = new Google_Service_MyBusiness($client);
    $results = $gmbService->accounts->listAccounts();
    $location = $gmbService->accounts_locations->listAccountsLocations($results->accounts[0]->name);*/
    
    $mybusinessService = new Google_Service_Mybusiness($client);

    $accounts = $mybusinessService->accounts->listAccounts();    
    
    dump($accounts);
    dump($accounts->accounts[0]->name);
    $locations = $mybusinessService->accounts_locations->listAccountsLocations($accounts->accounts[0]->name);
    dump($locations);
    


    $reviews = $mybusinessService->accounts_locations_reviews;
    dd($reviews);
    $listReviewsResponse = $reviews->listAccountsLocationsReviews($location->name);
    $reviewsList = $listReviewsResponse->getReviews();

    

    return $analytics;
}
 
function getFirstProfileId($analytics) {
    // Get the list of accounts for the authorized user.
    $accounts = $analytics->management_accounts->listManagementAccounts();
    // dump($accounts);
    if (count($accounts->getItems()) > 0) {
        $items = $accounts->getItems();
        $firstAccountId = $items[0]->getId();
        $firstAccountName = $items[0]->getName();

        // Get the list of properties for the authorized user.
        $properties = $analytics->management_webproperties
            ->listManagementWebproperties($firstAccountId);
        // dump($properties);

        if (count($properties->getItems()) > 0) {
            $items = $properties->getItems();
            $firstPropertyId = $items[0]->getId();
            $firstPropertyName = $items[0]->getName();

            // Get the list of views (profiles) for the authorized user.
            $profiles = $analytics->management_profiles
                ->listManagementProfiles($firstAccountId, $firstPropertyId);
            // dump($profiles);
            if (count($profiles->getItems()) > 0) {
                $items = $profiles->getItems();
                $profileInfo = [];
                $profileName = $items[0]->getName();
                $profileId = $items[0]->getId();

                $profileInfo = [$profileId, $profileName, $firstPropertyName, $firstAccountName];
                return $profileInfo;

            } else {
                throw new Exception('No views (profiles) found for this user.');
            }
        } else {
            throw new Exception('No properties found for this user.');
        }
    } else {
        throw new Exception('No accounts found for this user.');
    }
}

function getResults($analytics, $profileInfo) {

    // Calls the Core Reporting API and queries for the number of sessions, users, new users, average session duration and bounce rate for the last month (main data).

    // Get profile info
    $data1 = array_slice($profileInfo, 1, 2);

    $analyticsViewId    = 'ga:' . $profileInfo[0];
    $startDate = ['7daysAgo', '14daysAgo', '21daysAgo', '28daysAgo'];
    $endDate = ['today', '7daysAgo', '14daysAgo', '21daysAgo'];
    $metrics1 = 'ga:sessions, ga:users, ga:newUsers, ga:avgSessionDuration, ga:bounceRate , ga:pageviews,ga:visits';
    $metrics2 = 'ga:sessions';

    // Get main data
    for ($i = 0, $size = count($startDate); $i < $size; $i++) {
        $data2[$i] = $analytics->data_ga->get($analyticsViewId, $startDate[$i], $endDate[$i], $metrics1);
    }

    // Get spatial data
    for ($i = 0, $size = count($startDate); $i < $size; $i++) {
        $data3[$i] = $analytics->data_ga->get($analyticsViewId, $startDate[$i], $endDate[$i], $metrics2, array(
            'dimensions'    => 'ga:country',
            'sort'          => '-ga:sessions',
            // 'max-results'   => '5'
        ));
    }

    // Gather in an array
    $data = [$data1, $data2, $data3];

    // Return variable to controller
    return $data;
}

function getAllProfiles($analytics) {
    // Get the list of accounts for the authorized user.
    $accounts = $analytics->management_accounts->listManagementAccounts();
    // dd($accounts); 
    $profileInfo = [];
    if (count($accounts->getItems()) > 0) {
        $items = $accounts->getItems();
        if($items){
            foreach ($items as $itm) {
                // Get the list of properties for the authorized user.
                $properties = $analytics->management_webproperties->listManagementWebproperties($itm->getId());
                $accountID = $itm->getId();
                $propertyItems = $properties->getItems();
                if (count($propertyItems) > 0) {
                    $propertyId = $propertyItems[0]->getId();
                    $propertyName = $propertyItems[0]->getName();

                    // Get the list of views (profiles) for the authorized user.
                    
                    $profiles = $analytics->management_profiles->listManagementProfiles($itm->getId(),$propertyId);
                    $profileItems = $profiles->getItems();

                    if (count($profileItems) > 0) {
                        
                        foreach ($profileItems as $pItem) {
                            $profileInfo[]= [
                               $pItem->getId(),
                               $pItem->getName(),
                               $propertyName, 
                               $accountID
                            ];
                        }

                        /*$profileInfo[]= [
                           $profileItems[0]->getId(),
                           $profileItems[0]->getName(),
                           $propertyName, 
                           $accountID
                        ];*/
                    }

                }
            }
        }
        // die;
        return $profileInfo;
    } else {
        throw new Exception('No accounts found for this user.');
    }
}

