<?php
// You can download this file from here https://cdn.dataforseo.com/v3/examples/php/php_RestClient.zip
require('RestClient.php');
/*$api_url = 'https://api.dataforseo.com/';
$api_url_sandbox = 'https://sandbox.dataforseo.com/';*/

$api_url = env('DATA_FOR_SEO_API_URL'); 

$client = new RestClient($api_url,null,env('DATA_FOR_SEO_LOGIN'), env('DATA_FOR_SEO_PASSWORD'));    
$url = preg_replace("(^https?://)", "", $website );

try { 
	// POST /v3/keywords_data/google/keywords_for_site/live
	// the full list of possible parameters is available in documentation
	// $result = $client->post('/v3/keywords_data/google/keywords_for_site/Standard', $post_array);
	
	// $result = $client->post('/v3/keywords_data/google/keywords_for_site/live', $post_array); 

	// $result = $client->post('/v3/serp/google/organic/live/regular', $post_array);  
	// $result = $client->post('/v3/keywords_data/google/keywords_for_site/live', $post_array);



	$keyword_array[] = array( 
	   "target" => $url,
	   "language_name" => "English",
	   // "location_code" => 2840,
	   /*"filters" => [
	      ["keyword_data.keyword_info.search_volume", "<>", 0],
	      "and",
	      [ 
	         ["ranked_serp_element.serp_item.type", "<>", "paid"],
	         "or",
	         ["ranked_serp_element.serp_item.is_malicious", "=", false]
	      ]
	   ]*/
	);
	$result = $client->post('/v3/dataforseo_labs/ranked_keywords/live', $keyword_array);
	// dd($result); 


	/*$result = $client->get('/v3/keywords_data/google/keywords_for_site/tasks_ready');
	dump($result);   
	$endPoint =  $client->get($result['tasks'][0]['result'][0]['endpoint']);
	dd($endPoint); */
	
} catch (RestClientException $e) {
	/*dump($e->getHttpCode());
	dump($e->getCode());
	dump($e->getMessage());
	dump($e->getTraceAsString());
	die;*/


	echo "\n";
	print "HTTP code: {$e->getHttpCode()}\n";
	print "Error code: {$e->getCode()}\n";
	print "Message: {$e->getMessage()}\n";
	print  $e->getTraceAsString();
	echo "\n";
}
$client = null;
?>
