@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
            <img src="{{$site_title}}"/>
        @endcomponent
    @endslot 
    {{ __('Dear')}} <br>
	{{__($msg)}}. <br>
    @component('mail::button', ['url' => $link])
        {{__('Reset Password')}}
    @endcomponent
    @slot('footer')  
        @component('mail::footer') 
        	{{__('Thanks')}}<br>
            {{env('SITE_TITLE')}}   
        @endcomponent
    @endslot
@endcomponent

