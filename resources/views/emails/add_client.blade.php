@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
            
        @endcomponent
    @endslot 
    {{ __('Hi')}} {{$name}}, <br>
	{{__($msg)}}. <br>
    @component('mail::button', ['url' => $link])
        {{__('Set Password')}}
    @endcomponent 
    @slot('footer')  
        @component('mail::footer') 
        	{{__('Thanks')}}<br>  
            {{env('SITE_TITLE')}} 
        @endcomponent
    @endslot
@endcomponent

