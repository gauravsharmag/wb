@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => env('SITE_URL') ])
            
        @endcomponent
    @endslot 
    {{ __('Hi')}} <br>
    {{__('You have received a new support request.')}} <br>
    <p><b>Name: </b> {{$name}} </p>
    <p><b>Email: </b> {{$email}} </p>

	{{__($msg)}} <br>
    @slot('footer')  
        @component('mail::footer') 
        	{{__('Thanks')}}<br>  
            {{$site_title}} 
        @endcomponent
    @endslot
@endcomponent

