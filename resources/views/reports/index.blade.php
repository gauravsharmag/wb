@extends('layouts.contentLayoutMaster')
@section('title','Reports')
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-users.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" />
@endsection
@section('content')
<!-- users list start -->
<section class="users-list-wrapper">
  
  <div class="users-list-table">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <div class="users-list-filter px-1">
            <form> 
              <div class="row border rounded py-2 mb-2">
                <div class="col-md-6">
                 
                </div>
                <div class="col-md-2"> 
                  
                  
                 
                </div>
                <div class="col-md-2">
                  <input type="date" name="start" id="start" placeholder="Start Date" autocomplete="off">
                </div> 
                <div class="col-md-2">
                  <input type="date" name="end" id="end" placeholder="End Date" autocomplete="off">  
                </div>
              </div>
            </form>
           
          </div> 
          <div class="table-responsive">
            <table id="usersTable" class="table">
              <thead>
                <tr> 
                    <th>Client</th>
                    <th>KW Report</th>
                    <th>GA Report</th>
                    <th>Full Report</th>
                </tr>
              </thead>
              <tbody>
                

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="multiple-msgs" style="display: none;">
       <div class="text-bold-600">
         <span class="icon-spinner9 icon-spin text-left"></span>&nbsp; Loading ...
       </div>
   </div>
</section>
<!-- users list ends --> 
@endsection 
 
@section('vendor-scripts')
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>


<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script> -->


<script src="{{asset('vendors/js/ui/blockUI.min.js')}}"></script>
 
<script src="{{asset('js/custom/reports.js')}}"></script>
 
@endsection
