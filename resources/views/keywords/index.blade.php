@extends('layouts.contentLayoutMaster')
@section('title','Keywords')
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-users.css')}}">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css"> 
@endsection
@section('content')
<!-- users list start -->
<section class="users-list-wrapper">
  
  <div class="users-list-table">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <a href="/export/keywords?type=pdf" class="btn btn-primary mr-1 mb-1">PDF</a>&nbsp; <a href="/export/keywords?type=csv" class="btn btn-primary mr-1 mb-1">CSV</a>
          <div class="table-responsive">
            <table id="keywordsTable" class="table">
             <!-- <img src="images/logo/5f73708f4d268.png" style="height: 100px;"> -->
              <thead>
                <tr> 
                    <th>Keyword</th>
                    <th>Rank</th>
                    <th>Page Url</th>
                </tr>
              </thead>
              <tbody>
                

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- users list ends -->
@endsection 

@section('vendor-scripts')
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
 

<script src="//cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>

 


<script type="text/javascript">
    $(function() { 
      getKeywords();   
    }); 
 
    function getKeywords() { 
      $('#keywordsTable').DataTable({
        responsive: true,
        processing: true,  
        serverSide: true, 
        bDestroy:true,
        ajax: "/keywords/get",    
        lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
        /*dom: 'Bfrtip',
        buttons: [
            'csvHtml5',
            'pdfHtml5'
        ],*/
        columns: [
              { data: 'keyword', name: 'keyword' },
              { data: 'keyword_rank', name: 'keyword_rank' },
              { data: 'url', name: 'url' }
        ] 
      });
    }

    function base64Encode(str) {
        var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var out = "", i = 0, len = str.length, c1, c2, c3;
        while (i < len) {
            c1 = str.charCodeAt(i++) & 0xff;
            if (i == len) {
                out += CHARS.charAt(c1 >> 2);
                out += CHARS.charAt((c1 & 0x3) << 4);
                out += "==";
                break;
            }
            c2 = str.charCodeAt(i++);
            if (i == len) {
                out += CHARS.charAt(c1 >> 2);
                out += CHARS.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
                out += CHARS.charAt((c2 & 0xF) << 2);
                out += "=";
                break;
            }
            c3 = str.charCodeAt(i++);
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
            out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
            out += CHARS.charAt(c3 & 0x3F);
        }
        console.log(out);
        return out;
    }


    
</script>

@endsection
