@extends('layouts.contentLayoutMaster')
@section('title','Website Settings')
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
@endsection

@section('content')

	<section id="dashboard-analytics">
	    <div class="row match-height">
	      		
	      	<div class="col-md-8 col-12">
	      	      <div class="card" >
	      	        <div class="card-header">
	      	          <h4 class="card-title"> {{__('Website Settings')}} </h4>
	      	        </div>
	      	        <div class="card-content">
	      	          <div class="card-body">
	      	            <form class="form form-horizontal" method="POST" action="/settings" enctype="multipart/form-data">
	      	            	@csrf
	      	              <div class="form-body">
	      	                <div class="row">
	      	                
	      	                	<div class="col-md-4">
	      	                	  <label>Website Title</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="website_title" class="form-control @error('website_title') is-invalid @enderror" name="website_title" value="{{ isset($optArr['website_title'])?$optArr['website_title']:'' }}"> 
	      	                	  @error('website_title')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Website Description</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <textarea name="website_desc" class="form-control"> 
	      	                	  	{{ isset($optArr['website_desc'])?$optArr['website_desc']:'' }} 
	      	                	  </textarea>
	      	                	  @error('website_desc')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Phone#</label> 
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="number" id="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ isset($optArr['phone'])?$optArr['phone']:'' }}">
	      	                	  @error('phone')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Email</label> 
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ isset($optArr['email'])?$optArr['email']:'' }}">
	      	                	  @error('email')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Website Logo</label> 
	      	                	</div>
	      	                	<div class="col-md-8 form-group"> 
	      	                	  <input type="file" id="logo" class="form-control @error('logo') is-invalid @enderror" name="logo" >
	      	                	  @error('logo')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror

	      	                	  @if(isset($optArr['logo']))
	      	                	  	<img src="{{$optArr['logo']}}" class="img-responsive" style="height: 100px;" title="Current Logo">
	      	                	  @endif 
	      	                	</div> 


	      	                	<div class="col-md-4">
	      	                	  <label>Facebook Follow Link</label> 
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="url" id="fb_url" class="form-control @error('fb_url') is-invalid @enderror" name="fb_url" value="{{ isset($optArr['fb_url'])?$optArr['fb_url']:'' }}"> 
	      	                	</div> 
	      	                	<div class="col-md-4">
	      	                	  <label>Twitter Follow Link</label> 
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="url" id="tw_url" class="form-control @error('tw_url') is-invalid @enderror" name="tw_url" value="{{ isset($optArr['tw_url'])?$optArr['tw_url']:'' }}">
	      	                	</div>
	      	                	<div class="col-md-4">
	      	                	  <label>Instagram Follow Link</label> 
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="url" id="in_url" class="form-control @error('in_url') is-invalid @enderror" name="in_url" value="{{ isset($optArr['in_url'])?$optArr['in_url']:'' }}">
	      	                	</div> 
	      	                	<div class="col-md-4">
	      	                	  <label>Footer Text</label> 
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="footer_text" class="form-control @error('footer_text') is-invalid @enderror" name="footer_text" value="{{ isset($optArr['footer_text'])?$optArr['footer_text']:'' }}">
	      	                	</div> 
	      	                	 
	      	                  <div class="col-sm-12 d-flex justify-content-end">
	      	                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{__('Submit')}} </button>
	      	                  </div>
	      	                </div>
	      	              </div>
	      	            </form>
	      	          </div>
	      	        </div>
	      	      </div>
	      	</div>

	    </div>
	</section>
	
@endsection