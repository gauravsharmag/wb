<p style="text-align: center;"><img src="{{url($siteLogo)}}" class="img-responsive" style="height: 100px;display: block;
    margin-left: auto;
    margin-right: auto;"></p> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.css" integrity="sha512-H6KANMpZQKsK9c09UqdcQv02JTiZ/hMVwxkcbDLrp125CR884wwEdnWDm+Yuo6tihuC/cizLYWAjMZi0xAfGig==" crossorigin="anonymous" />

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" />


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.js" integrity="sha512-y/fY9i5oZuCdGyNN/S4MV8lT9t3I8ztPJ0+5OqeXWswT19mt9QruHXAzSBazb3JCJD4/49HvmCYkEHE+/iT1pQ==" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
<script src="{{asset('vendors/js/ui/blockUI.min.js')}}"></script>



<section id="dashboard-analytics">
	<div class="row">
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="visitsCard">
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Visits (<span id="dashboard-visits-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1">
	            <div id="dashboard-visits-chart"></div>
	            <div id="dashboard-visits-chart-error" style="text-align:center;"></div>
	          </div>
	        </div> 
	      </div>
	    </div> 
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="pageViewsCard">
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Page Views (<span id="dashboard-pageviews-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1">
	            <div id="dashboard-pageviews-chart"></div>
	            <div id="dashboard-pageviews-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	</div>

	<div class="row">
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="avgTimeCard">
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Average Time On Site</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1">
	            <div id="dashboard-avgtime-chart"></div>
	            <div id="dashboard-avgtime-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="bounceRateCard">
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Bounce Rate</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1">
	            <div id="dashboard-bouncerate-chart"></div>
	            <div id="dashboard-bouncerate-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	</div>

	<div class="row">
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="organicTrafficCard"> 
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Organic Traffic (<span id="dashboard-organic-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1">
	            <div id="dashboard-organic-traffic-chart" ></div>
	            <div id="dashboard-organic-traffic-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="referralTrafficCard">
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Referral Traffic (<span id="dashboard-referral-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1"> 
	            <div id="dashboard-referral-chart" ></div>
	            <div id="dashboard-referral-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	</div>

	<div class="row">
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="directTrafficCard"> 
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Direct Traffic (<span id="dashboard-direct-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1">
	            <div id="dashboard-direct-traffic-chart" ></div>
	            <div id="dashboard-direct-traffic-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="paidSearchTrafficCard">
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Paid Search Traffic (<span id="dashboard-paidsearch-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1"> 
	            <div id="dashboard-paidsearch-traffic-chart" ></div>
	            <div id="dashboard-paidsearch-traffic-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	</div>

	<div class="row">
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="socialTrafficCard"> 
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Social Media Traffic (<span id="dashboard-social-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1">
	            <div id="dashboard-social-traffic-chart" ></div>
	            <div id="dashboard-social-traffic-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="visitsComputerCard">
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Visits From Computers (<span id="dashboard-computer-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1"> 
	            <div id="dashboard-visits-computer-chart" ></div>
	            <div id="dashboard-visits-computer-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	</div> 

	<div class="row">
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="visitsPhoneCard"> 
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Visits From Phones (<span id="dashboard-phone-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1">
	            <div id="dashboard-visits-phone-chart" ></div>
	            <div id="dashboard-visits-phone-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div> 
	    </div>
	    <div class="col-md-6 col-sm-12">
	      <div class="card" id="visitsTabletsCard">
	        <div class="card-header d-flex justify-content-between align-items-center">
	          <h4 class="card-title">Visits From Tablets (<span id="dashboard-tablet-count"></span>)</h4>
	          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
	        </div>
	        <div class="card-content">
	          <div class="card-body pb-1"> 
	            <div id="dashboard-visits-tablet-chart" ></div>
	            <div id="dashboard-visits-tablet-chart-error" style="text-align:center;"></div>
	          </div>
	        </div>
	      </div>
	    </div>
	</div>

</section>

<script type="text/javascript">
	



	var start = "{{$request->start}}";
	var end = "{{$request->end}}";
	var viewID = "{{$request->view_id}}";

	getDeviceData();
	getChannelData();
	getPerformanceData(); 

	function getPerformanceData() {
		blockUI();
	    $.ajax({ 
	      url:'/performance/get-data?start='+start+'&end='+end+'&view_id='+viewID,  
	      dataType:'json',
	      success:function(res){
	        var datesArr=[];  
	        var visitsArr=[];
	        var pageViewsArr=[];
	        var bounceRateArr=[];
	        var avgTimeArr=[];
	        if(res.response.success){
		        $.each(res.response.data,function(i,v){
		            datesArr.push(v.date);  
		            visitsArr.push(v.visits);
		            pageViewsArr.push(v.pageViews);
		            bounceRateArr.push(v.bounceRate);
		            avgTimeArr.push(v.avgTime); 
		        }); 
	          buildVisitChart(datesArr,visitsArr);
	          buildPageViewsChart(datesArr,pageViewsArr); 
	          
	          $('#dashboard-avgtime-chart').html(res.response.totals.avgTime); 
	          $('#dashboard-bouncerate-chart').html(res.response.totals.avgBounceRate);  
	          $('#dashboard-visits-count').html(res.response.totals.totalVisits);
	          $('#dashboard-pageviews-count').html(res.response.totals.totalPageViews);
	          printDoc();
	          unBlockUI();
	        }
	      }
	    });
	}

	function getDeviceData() {
	  $.ajax({ 
	    url:'/performance/get-device-data?start='+start+'&end='+end+'&view_id='+viewID, 
	    dataType:'json',
	    success:function(res){
	      var datesArr=[];  
	      var desktopVisitsArr=[];
	      var phoneVisitsArr=[];
	      var tabVisitsArr=[];
	      if(res.response.success){
	        $.each(res.response.data,function(i,v){
	          datesArr.push(i);  
	          desktopVisitsArr.push(v.desktop);
	          phoneVisitsArr.push(v.mobile);
	          tabVisitsArr.push(v.tablet);
	        });
	      } 
	      buildDeviceChart(datesArr,desktopVisitsArr,'dashboard-visits-computer-chart'); 
	      buildDeviceChart(datesArr,phoneVisitsArr,'dashboard-visits-phone-chart'); 
	      buildDeviceChart(datesArr,tabVisitsArr,'dashboard-visits-tablet-chart'); 
	      $('#dashboard-computer-count').html(res.response.totals.desktop);
	      $('#dashboard-phone-count').html(res.response.totals.mobile);
	      $('#dashboard-tablet-count').html(res.response.totals.tablet); 
	    } 
	  });
	} 
	function getChannelData() {
	  $.ajax({
	    url:'/performance/get-channel-data?start='+start+'&end='+end+'&view_id='+viewID, 
	    dataType:'json', 
	    success:function(res){ 
	      var datesArr=[];  
	      var organicVisitsArr=[];
	      var referralVisitsArr=[];
	      var directVisitsArr=[];
	      var paidVisitsArr=[];
	      var socialVisitsArr=[];

	      if(res.response.success){
	        $.each(res.response.data,function(i,v){
	          datesArr.push(i);  
	          organicVisitsArr.push(v['Organic Search']);
	          referralVisitsArr.push(v.Referral);
	          directVisitsArr.push(v.Direct);
	          
	          socialVisitsArr.push(v.Social);
	          if( typeof( v['Paid Search'] ) != "undefined" && v['Paid Search'] !== null) {
	            paidVisitsArr.push(v['Paid Search']);
	          }
	        });
	      }  

	      buildChannelChart(datesArr,organicVisitsArr,'dashboard-organic-traffic-chart'); 
	      buildChannelChart(datesArr,referralVisitsArr,'dashboard-referral-chart');  
	      buildChannelChart(datesArr,directVisitsArr,'dashboard-direct-traffic-chart');
	      buildChannelChart(datesArr,paidVisitsArr,'dashboard-paidsearch-traffic-chart');
	      buildChannelChart(datesArr,socialVisitsArr,'dashboard-social-traffic-chart'); 

	      $('#dashboard-organic-count').html(res.response.totals.organic);
	      $('#dashboard-referral-count').html(res.response.totals.referral);
	      $('#dashboard-direct-count').html(res.response.totals.direct);
	      $('#dashboard-paidsearch-count').html(res.response.totals.paid);
	      $('#dashboard-social-count').html(res.response.totals.social);
	    } 
	  });
	}

	function buildVisitChart(date,data){ 
	  var options = {
	        series: [{
	          name: "Site Visits",
	          data: data
	        }],
	        chart: {
	          height: 250, 
	          type: 'line',
	          /*zoom: {
	            type: 'x',
	            enabled: true,
	            autoScaleYaxis: true
	          },
	          toolbar: {
	            autoSelected: 'zoom'
	          }*/
	        },
	        dataLabels: {
	          enabled: false
	        },
	        stroke: {
	          curve: 'straight'
	        },
	        title: {
	          text: '',
	          align: 'left'
	        },
	        grid: {
	          row: {
	            colors: ['#f3f3f3', 'transparent'], 
	            opacity: 0.5
	          },
	        }, 
	        xaxis: {
	          categories: date,
	        },
	        noData: { 
	          text: 'Loading...'
	        }
	  }; 
	  var chart = new ApexCharts(document.querySelector("#dashboard-visits-chart"), options);
	  chart.render(); 
	}
	function buildPageViewsChart(date,data){ 
	  var options = {
	        series: [{
	          name: "Page Views",
	          data: data
	        }],
	        chart: {
	          height: 250,
	          type: 'line',
	        },
	        dataLabels: {
	          enabled: false
	        },
	        stroke: {
	          curve: 'straight'
	        },
	        title: {
	          text: '',
	          align: 'left'
	        },
	        grid: {
	          row: {
	            colors: ['#f3f3f3', 'transparent'], 
	            opacity: 0.5
	          },
	        },
	        xaxis: {
	          categories: date,
	        }
	  };
	  // chart.destroy(); 
	  var chart = new ApexCharts(document.querySelector("#dashboard-pageviews-chart"), options);
	  chart.render();  
	} 
	function buildDeviceChart(date,data,element) {
	  var options = {
	        series: [{
	          name: "Site Visits",
	          data: data
	        }],
	        chart: {
	          height: 250, 
	          type: 'line',
	        },
	        dataLabels: {
	          enabled: false
	        },
	        stroke: {
	          curve: 'straight'
	        },
	        title: {
	          text: '',
	          align: 'left'
	        },
	        grid: {
	          row: {
	            colors: ['#f3f3f3', 'transparent'], 
	            opacity: 0.5
	          },
	        },
	        xaxis: {
	          categories: date,
	        },

	        noData: { 
	          text: 'Loading...'
	        }
	  };
	 
	  var chart = new ApexCharts(document.querySelector("#"+element), options);
	  chart.render();
	}
	function buildChannelChart(date,data,element) {
	  var options = {
	        series: [{
	          name: "Channel Traffic",
	          data: data
	        }],
	        chart: {
	          height: 250, 
	          type: 'line',
	        },
	        dataLabels: {
	          enabled: false
	        },
	        stroke: {
	          curve: 'straight'
	        },
	        title: {
	          text: '',
	          align: 'left'
	        },
	        grid: {
	          row: {
	            colors: ['#f3f3f3', 'transparent'], 
	            opacity: 0.5
	          },
	        },
	        xaxis: {
	          categories: date,
	        },
	        noData: { 
	          text: 'Loading...'
	        }
	  };
	  var chart = new ApexCharts(document.querySelector("#"+element), options);
	  chart.render(); 
	}     

	 


	function printDoc() {
		setTimeout(function(){ window.print(); }, 2000); 
	}

	function blockUI() {
	  var $white = '#fff';
	  $.blockUI({
	    message: '<div class="semibold"><span class="bx bx-revision icon-spin text-left"></span>&nbsp; Please Wait...</div>',
	    overlayCSS: {
	      backgroundColor: $white,
	      opacity: 0.8,
	      cursor: 'wait'
	    },
	    css: {
	      border: 0,
	      padding: 0,
	      backgroundColor: 'transparent'
	    }
	  });
	}
	function unBlockUI() {
	  $.unblockUI();
	}

</script>

 








