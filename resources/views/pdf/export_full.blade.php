<p style="text-align: center;"><img src="{{url($siteLogo)}}" class="img-responsive" style="height: 100px;"></p>  
<h3 style="text-align: center;"> Visits</h3>
<table style="width: 100%"> 
  <tr>
    <th style="text-align: left;">Date</th>
    <th style="text-align: left;">Visits</th>
  </tr> 
  @foreach( $fullData['exportGaData']['dashboardExport']['visits']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $fullData['exportGaData']['dashboardExport']['visits']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>
 
<h3 style="text-align: center;">Page Views</h3>
<table style="width: 100%">
  <tr>
    <th style="text-align: left;">Date</th>
    <th style="text-align: left;">Visits</th> 
  </tr>
  @foreach( $fullData['exportGaData']['dashboardExport']['pageViews']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $fullData['exportGaData']['dashboardExport']['pageViews']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Average Time</h3>
<table style="width: 100%">
  <tr>
    <th style="text-align: left;">Date</th>
    <th style="text-align: left;">Time</th>
  </tr>
  @foreach( $fullData['exportGaData']['dashboardExport']['avgTime']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $fullData['exportGaData']['dashboardExport']['avgTime']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Bounce Rate</h3>
<table style="width: 100%">
  <tr>
    <th style="text-align: left;">Date</th>
    <th style="text-align: left;">Rate</th>
  </tr>
  @foreach( $fullData['exportGaData']['dashboardExport']['bounceRate']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $fullData['exportGaData']['dashboardExport']['bounceRate']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Social Visits</h3>
<table style="width: 100%">
  <tr>
    <th style="text-align: left;">Date</th>
    <th style="text-align: left;">Visit</th>
  </tr>
  @foreach( $fullData['exportGaData']['socialExport']['data'] as $key => $date )
    <tr>
      <td>{{ $key }} </td>
      <td>{{ $date }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Channel Data</h3>
<table style="width: 100%">
  <tr>
    <th style="text-align: left;">Type</th>
    <th style="text-align: left;">Visit</th>
  </tr>
  @foreach( $fullData['exportGaData']['channelExport']['data'] as $key => $date )
    <tr>
      <td>{{ $key }} </td>
      <td>{{ $date }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Device Data</h3>
<table style="width: 100%">
  <tr>
    <th style="text-align: left;">Device</th>
    <th style="text-align: left;">Visit</th>
  </tr>
  @foreach( $fullData['exportGaData']['deviceExport']['data'] as $key => $date )
    <tr>
      <td>{{ $key }} </td>
      <td>{{ $date }} </td>
    </tr>
  @endforeach
</table> 

<h3 style="text-align: center;">Top Page Views</h3>
<table style="width: 100%">
  <tr>
    <th style="text-align: left;">Page Title</th>
    <th style="text-align: left;">Page Url</th>
    <th style="text-align: left;">Views</th>
  </tr>
  @foreach( $fullData['exportGaData']['topPageViews']['rows'] as $pageV)
    <tr> 
      <td>{{ $pageV[1] }} </td>
      <td>{{ $pageV[0] }} </td>
      <td>{{ $pageV[2] }} </td> 
    </tr>
  @endforeach
</table>

@if(isset($fullData['exportKeywordData']))
    <h3 style="text-align: center;">Keywords</h3>
    <table style="width: 100%">
      <tr>
        <th style="text-align: left;">Keyword</th>
        <th style="text-align: left;">Rank</th>
        <th style="text-align: left;">Url</th>
      </tr>
      @foreach( $fullData['exportKeywordData'] as $keyword)
        <tr>
          <td>{{ $keyword->keyword }} </td>
          <td>{{ $keyword->keyword_rank }} </td>
          <td>{{ $keyword->url }} </td>
        </tr>
      @endforeach
    </table> 
@endif


 