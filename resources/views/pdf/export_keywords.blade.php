<p style="text-align: center;"><img src="{{url($siteLogo)}}" class="img-responsive" style="height: 100px;"></p>  
<h3 style="text-align:center;">Keywords</h3>
@if($keywordsData)
  <table style="width:100%">
    <tr>
      <th style="text-align:left;">Keyword</th>
      <th style="text-align:left;">Rank</th>
      <th style="text-align:left;">Url</th>  
    </tr> 
    


    @foreach( $keywordsData as $data )
      <tr>
        <td>{{ $data->keyword }} </td> 
        <td>
          <?php 
            if($data->keyword_rank){
              if($data->keyword_rank <=5){
                echo '<span style="background-color: #39DA8A">'.$data->keyword_rank.'</span>';
              }else if($data->keyword_rank >5 &&  $data->keyword_rank <=10 ){ 
                echo '<span style="background-color: yellow">'.$data->keyword_rank.'</span>';
              } else if($k->keyword_rank >10 &&  $data->keyword_rank <=20 ){
                echo '<span style="background-color: red">'.$data->keyword_rank.'</span>';
              } else if($data->keyword_rank >20){ 
                echo '<span style="background-color: grey">'.$data->keyword_rank.'</span>';
              }
            }else {
              echo '<span style="background-color:#475F7B">NOT RANKED </span>'; 
            }
          ?>
        </td>
        <td>{{ $data->url }} </td> 
      </tr>
    @endforeach
  </table>
@else
  <p style="text-align: center;">No keywords found</p>
@endif
