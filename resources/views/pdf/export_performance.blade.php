<p style="text-align: center;"><img src="{{url($siteLogo)}}" class="img-responsive" style="height: 100px;"></p>  
<h3>Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visits</th>
  </tr> 
  @foreach( $pdfData['dashboardData']['visits']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $pdfData['dashboardData']['visits']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3>Page Views</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visits</th> 
  </tr>
  @foreach( $pdfData['dashboardData']['pageViews']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $pdfData['dashboardData']['pageViews']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3>Average Time</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Time</th>
  </tr>
  @foreach( $pdfData['dashboardData']['avgTime']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $pdfData['dashboardData']['avgTime']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3>Bounce Rate</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Rate</th>
  </tr>
  @foreach( $pdfData['dashboardData']['bounceRate']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $pdfData['dashboardData']['bounceRate']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>



<h3>Direct Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visit</th>
  </tr>
  @foreach( $pdfData['channelData']['direct'] as $key => $date )
        <tr>
          <td>{{ $key }} </td>
          <td>{{ $date }} </td>
        </tr>
  @endforeach
</table>

<h3>Organic Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visit</th>
  </tr>
  @foreach( $pdfData['channelData']['organic'] as $key => $date )
        <tr>
          <td>{{ $key }} </td>
          <td>{{ $date }} </td>
        </tr>
  @endforeach
</table>

<h3>Referral Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visit</th>
  </tr>
  @foreach( $pdfData['channelData']['referral'] as $key => $date )
        <tr>
          <td>{{ $key }} </td>
          <td>{{ $date }} </td>
        </tr>
  @endforeach
</table>

<h3>Social Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visit</th>
  </tr>
  @foreach( $pdfData['channelData']['social'] as $key => $date )
        <tr>
          <td>{{ $key }} </td>
          <td>{{ $date }} </td>
        </tr>
  @endforeach
</table>

<h3>Paid Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visit</th>
  </tr>
  @foreach( $pdfData['channelData']['paid'] as $key => $date )
        <tr>
          <td>{{ $key }} </td>
          <td>{{ $date }} </td>
        </tr>
  @endforeach
</table>


<h3>Desktop Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visit</th>
  </tr>
  @foreach( $pdfData['deviceData']['desktop'] as $key => $date )
        <tr>
          <td>{{ $key }} </td>
          <td>{{ $date }} </td>
        </tr>
  @endforeach
</table>

<h3>Mobile Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visit</th>
  </tr>
  @foreach( $pdfData['deviceData']['mobile'] as $key => $date )
        <tr>
          <td>{{ $key }} </td>
          <td>{{ $date }} </td>
        </tr>
  @endforeach
</table> 

<h3>Tablet Visits</h3>
<table style="width: 50%">
  <tr>
    <th>Date</th>
    <th>Visit</th>
  </tr>
  @foreach( $pdfData['deviceData']['tab'] as $key => $date )
        <tr>
          <td>{{ $key }} </td>
          <td>{{ $date }} </td>
        </tr>
  @endforeach
</table>








