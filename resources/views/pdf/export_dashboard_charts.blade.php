<p style="text-align: center;"><img src="{{url($siteLogo)}}" class="img-responsive" style="height: 100px;display: block;
    margin-left: auto;
    margin-right: auto;"></p> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.css" integrity="sha512-H6KANMpZQKsK9c09UqdcQv02JTiZ/hMVwxkcbDLrp125CR884wwEdnWDm+Yuo6tihuC/cizLYWAjMZi0xAfGig==" crossorigin="anonymous" />

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" />
 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.js" integrity="sha512-y/fY9i5oZuCdGyNN/S4MV8lT9t3I8ztPJ0+5OqeXWswT19mt9QruHXAzSBazb3JCJD4/49HvmCYkEHE+/iT1pQ==" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
<script src="{{asset('vendors/js/ui/blockUI.min.js')}}"></script>


 
<div class="row">
    <div class="col-md-6 col-sm-12">
      <div class="card" id="visitsCard">

        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Visits (<span id="dashboard-visits-count"></span>)</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">
          <div class="card-body pb-1">
            <!-- <div id="analytics-bar-chart"></div> -->
            <div id="dashboard-visits-chart"></div>
            <div id="dashboard-visits-chart-error" style="text-align:center;"></div>
          </div>
        </div> 
      </div>
    </div> 
    <div class="col-md-6 col-sm-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Social/Referral (<span id="dashboard-social-count"></span>)</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">
          <div class="card-body pb-1">
            <div id="dashboard-social-chart"></div>
            <div id="dashboard-social-chart-error" style="text-align:center;"></div>
          </div>
        </div>
      </div>
    </div> 
</div> 
 
<div class="row">
    <div class="col-md-6 col-sm-12">
      <div class="card">  
        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Page Views (<span id="dashboard-pageviews-count"></span>)</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">
          <div class="card-body pb-1">
            <div id="dashboard-pageviews-chart"></div>
            <div id="dashboard-pageviews-chart-error" style="text-align:center;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-sm-12">
      <div class="card" id="pageViewTable"> 
        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Top Page Views</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">
          <div class="card-body pb-1">
            <table class="table">
                <thead> 
                  <tr>
                    <th>Page Title</th>
                    <th>Page Url</th>
                    <th>Visits</th>
                  </tr>
                </thead>
                <tbody id="topPageViewBody">
                  	@foreach($topPageViews as $top)
                  		<tr>
                  			<td> {{$top[1]}} </td>
                  			<td> {{$top[0]}} </td>
                  			<td> {{$top[2]}} </td> 
                  		</tr>
                  	@endforeach
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-12">
      <div class="card" id="channelsCard"> 
        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Channels (<span id="dashboard-channels-count"></span>)</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">
          <div class="card-body pb-1">
            <div id="dashboard-channels-chart" ></div>
            <div id="dashboard-channels-chart-error" style="text-align:center;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-sm-12">
      <div class="card" id="devicesCard">
        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Devices (<span id="dashboard-devices-count"></span>)</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">
          <div class="card-body pb-1"> 
            <div id="dashboard-devices-chart" ></div>
            <div id="dashboard-devices-chart-error" style="text-align:center;"></div>
            <!-- <div id="donut-chart" class="d-flex justify-content-center"></div> -->
          </div>
        </div>
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Average Time</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">
          <div class="card-body pb-1">
            <div id="dashboard-avgtime-chart"></div>
            <div id="dashboard-avgtime-chart-error" style="text-align:center;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-sm-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Bounce Rate</h4>
          <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
        </div>
        <div class="card-content">
          <div class="card-body pb-1">
            <div id="dashboard-bouncerate-chart"></div>
            <div id="dashboard-bouncerate-chart-error" style="text-align:center;"></div>
          </div>
        </div>
      </div>
    </div>
</div>

@if($hasKeywords)
<div class="row">
  <div class="col-md-12 col-sm-6">
    <div class="card">
      <div class="card-header d-flex justify-content-between align-items-center">
        <h4 class="card-title">Keywords</h4>
        <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
      </div>
      <div class="card-content">
        <div class="card-body pb-1">
          <div class="table-responsive">
            <table id="keywordsTable" class="table">
              <thead>
                <tr> 
                    <th>Keyword</th>
                    <th>Rank</th>
                    <th>Page Url</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                	foreach($keywords as $k) {
                		echo "<tr><td>".$k->keyword."</td>";
                		echo "<td>";
                		if($k->keyword_rank){
            				if($k->keyword_rank <=5){
            					echo '<span style="background-color:#39DA8A">'.$k->keyword_rank.'</span>';
            				}else if($k->keyword_rank >5 &&  $k->keyword_rank <=10 ){
            					echo '<span style="background-color:#f0ad4e;;">'.$k->keyword_rank.'</span>';
            				} else if($k->keyword_rank >10 &&  $k->keyword_rank <=20 ){
            					echo '<span style="background-color:#d9534f">'.$k->keyword_rank.'</span>';
            				} else if($k->keyword_rank >20){
            					echo '<span style="background-color:#777">'.$k->keyword_rank.'</span>';
            				}
                		}else {
                			echo '<span style="background-color:#475F7B">Not Ranked</span>';
                		}
                		echo "</td>";
                		echo "<td>".$k->url."</td>"; 
                		echo "</tr>";
                	}
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif




<script type="text/javascript">
   
	var start = "{{$request->start}}";
	var end = "{{$request->end}}";
	var viewID = "{{$request->view_id}}";


	var $primary = '#5A8DEE';
	var $success = '#39DA8A';
	var $danger = '#FF5B5C';
	var $warning = '#FDAC41';
	var $info = '#00CFDD';
	var $label_color = '#475f7b';
	var $primary_light = '#E2ECFF';
	var $danger_light = '#ffeed9';
	var $gray_light = '#828D99';
	var $sub_label_color = "#596778";
	var $radial_bg = "#e7edf3";


	
	getDeviceData(); 
	getChannelData();
	getSocialData();  
	getDashboardData(); 
   

	function getDashboardData() {
		blockUI();
	    $.ajax({
	      url:'/get-client-dashboard-data?start='+start+'&end='+end+'&view_id='+viewID, 
	      dataType:'json',
	      success:function(res){
	        var datesArr=[];  
	        var visitsArr=[];
	        var pageViewsArr=[];
	        var bounceRateArr=[];
	        var avgTimeArr=[]; 
	        if(res.response.success){
		        $.each(res.response.data,function(i,v){
		            datesArr.push(v.date);  
		            visitsArr.push(v.visits);
		            pageViewsArr.push(v.pageViews);
		            bounceRateArr.push(v.bounceRate);
		            avgTimeArr.push(v.avgTime); 
		        });
	          buildVisitChart(datesArr,visitsArr);
	          buildPageViewsChart(datesArr,pageViewsArr);
	          $('#dashboard-avgtime-chart').html(res.response.totals.avgTime); 
	          $('#dashboard-bouncerate-chart').html(res.response.totals.avgBounceRate);  
	          $('#dashboard-visits-count').html(res.response.totals.totalVisits);   
	          $('#dashboard-pageviews-count').html(res.response.totals.totalPageViews); 
	        }
	        printDoc(); 
	      }
	    }); 
	}

	function getDeviceData() {
	  $.ajax({ 
	    url:'/get-client-visit-by-device?start='+start+'&end='+end+'&view_id='+viewID, 
	    dataType:'json', 
	    success:function(res){
	      var datesArr=[];  
	      var desktopVisitsArr=[];
	      var phoneVisitsArr=[];
	      var tabVisitsArr=[];
	      if(res.response.success){
	        $.each(res.response.data,function(i,v){
	          datesArr.push(i);  
	          desktopVisitsArr.push(v.desktop);
	          phoneVisitsArr.push(v.mobile);
	          tabVisitsArr.push(v.tablet);
	        }); 
	      } 
	      buildDeviceChart(res.response.data); 
	      $('#dashboard-devices-count').html(res.response.totals);   
	    } 
	  });
	} 
	function getChannelData() {
	  $.ajax({
	    url:'/get-client-traffic-by-channel?start='+start+'&end='+end+'&view_id='+viewID, 
	    dataType:'json', 
	    success:function(res){ 
	      var datesArr=[];  
	      var organicVisitsArr=[];
	      var referralVisitsArr=[];
	      var directVisitsArr=[];
	      var paidVisitsArr=[];
	      var socialVisitsArr=[];

	      if(res.response.success){
	      	buildChannelChart(res.response.data,'dashboard-channels-chart'); 
	      	 $('#dashboard-channels-count').html(res.response.totals);    
	      }  
	    } 
	  });
	}
	function getSocialData() {
	  $.ajax({
	    url:'/get-client-traffic-by-social?start='+start+'&end='+end+'&view_id='+viewID, 
	    dataType:'json', 
	    success:function(res){ 
	      var datesArr=[];  
	      var visitsArr=[];
	      
	      if(res.response.success){
	      	var datesArr=[];
	      	var visitsArr =[];
	      	$.each(res.response.data,function(i,v){
	      	  datesArr.push(i);  
	      	  visitsArr.push(v);
	      	});
	      	buildSocialChart(datesArr,visitsArr);
	      	$('#dashboard-social-count').html(res.response.totals); 
	      }  
	    } 
	  });
	}



	function buildVisitChart(date,data){ 
	  var options = {
	        series: [{
	          name: "Site Visits",
	          data: data
	        }],
	        chart: {
	          height: 250,
	          type: 'line',
	        },
	        dataLabels: {
	          enabled: false
	        },
	        stroke: {
	          curve: 'straight'
	        },
	        title: {
	          text: '',
	          align: 'left'
	        },
	        grid: { 
	          row: {
	            colors: ['#f3f3f3', 'transparent'],
	            opacity: 0.5
	          },
	        },
	        xaxis: {
	          categories: date,
	        },
	        fill: {
	          type: 'gradient',
	          gradient: {
	            shade: 'light',
	            type: "horizontal",
	            shadeIntensity: 0.25,
	            gradientToColors: undefined,
	            inverseColors: true,
	            opacityFrom: 0.85,
	            opacityTo: 0.85,
	            stops: [50, 0, 100]
	          },
	        }
	  }; 
	  var chart = new ApexCharts(document.querySelector("#dashboard-visits-chart"), options);
	  chart.render(); 
	}  
	function buildPageViewsChart(date,data){ 
	  var options = {
	        series: [{
	          name: "Page Views",
	          data: data
	        }],
	        chart: {
	          height: 250,  
	          type: 'line',
	        },
	        dataLabels: {
	          enabled: false
	        },
	        stroke: {
	          curve: 'straight'
	        },
	        title: {
	          text: '',
	          align: 'left'
	        },
	        grid: {
	          row: {
	            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
	            opacity: 0.5
	          },
	        },
	        xaxis: {
	          categories: date,
	        }
	  };
	  
	  var chart = new ApexCharts(document.querySelector("#dashboard-pageviews-chart"), options);
	  chart.render();
	}
	function buildDeviceChart(data) {
	  var labelsArr=[];
	  var labelsDataArr=[];
	  $.each(data,function(i,v){
	    labelsArr.push(i.toUpperCase());
	    labelsDataArr.push(v);
	  }); 
	 
	  var donutChartOption = {
	    chart: {
	      height: 250,
	      type: 'donut',
	    },
	    dataLabels: {
	      enabled: true,
	      formatter: function (val, opts) {
	        return opts.w.config.series[opts.seriesIndex]
	      } 
	    },
	    series: labelsDataArr,
	    labels: labelsArr,
	    stroke: {
	      width: 0,
	      lineCap: 'round',
	    },
	    align:"center",
	    colors: [$primary, $info, $warning],
	    plotOptions: {
	      pie: {
	        donut: {
	          size: '50%',
	          labels: {
	            show: true,
	            name: {
	              show: true,
	              fontSize: '15px',
	              colors: $sub_label_color,
	              offsetY: 20,
	              fontFamily: 'IBM Plex Sans',
	            },
	            value: {
	              show: true,
	              fontSize: '26px',
	              fontFamily: 'Rubik',
	              color: $label_color,
	              offsetY: -20,
	              formatter: function (val) {
	                return val; 
	              }
	            }, 
	            total: { 
	              show: true,
	              label: 'Visits',
	              color: $gray_light,
	              formatter: function (w) {
	                return w.globals.seriesTotals.reduce(function (a, b) {
	                  return a + b;
	                }, 0)
	              }
	            }
	          }
	        }
	      }
	    },
	    legend: {
	      show: true
	    }
	  }
	  var chart = new ApexCharts(document.querySelector("#dashboard-devices-chart"),donutChartOption);
	  chart.render(); 
	}
	function buildChannelChart(data) {
	  var labelsChannelArr=[];
	  var labelsChannelDataArr=[];
	  $.each(data,function(i,v){
	    labelsChannelArr.push(i.toUpperCase());
	    labelsChannelDataArr.push(v);
	  });
	  
	  var channelChartOption = {
	    chart: {
	      height: 250,
	      type: 'donut',
	    },
	    dataLabels: {
	      enabled: true,
	      formatter: function (val, opts) {
	        return opts.w.config.series[opts.seriesIndex]
	      } 
	    },
	    series: labelsChannelDataArr,
	    labels: labelsChannelArr,
	    stroke: {
	      width: 0,
	      lineCap: 'round',
	    },
	    colors: [$primary, $info, $warning], 
	    plotOptions: {
	      pie: {
	        donut: {
	          size: '50%',
	          labels: {
	            show: true,
	            name: {
	              show: true,
	              fontSize: '15px',
	              colors: $sub_label_color,
	              offsetY: 20,
	              fontFamily: 'IBM Plex Sans',
	            },
	            value: {
	              show: true,
	              fontSize: '26px',
	              fontFamily: 'Rubik',
	              color: $label_color,
	              offsetY: -20,
	              formatter: function (val) {
	                return val
	              }
	            },
	            total: {
	              show: true,
	              label: 'Visits',
	              color: $gray_light,
	              formatter: function (w) {
	                return w.globals.seriesTotals.reduce(function (a, b) {
	                  return a + b
	                }, 0)
	              } 
	            }
	          }
	        }
	      }
	    },
	    legend: {
	      show: true
	    }
	  }
	  
	  var chart = new ApexCharts(document.querySelector("#dashboard-channels-chart"),channelChartOption);
	  chart.render();
	}
	function buildSocialChart(date,data){
	  var socialOptions = {
	        series: [{
	          name: "Site Visits",
	          data: data
	        }],
	        chart: {
	          height: 250,
	          type: 'line',
	        },
	        dataLabels: {
	          enabled: false
	        },
	        stroke: {
	          curve: 'straight'
	        },
	        title: {
	          text: '',
	          align: 'left'
	        },
	        grid: {
	          row: {
	            colors: ['#f3f3f3', 'transparent'], 
	            opacity: 0.5
	          },
	        },
	        xaxis: {
	          categories: date,
	        }
	  };
	 
	  var chart = new ApexCharts(document.querySelector("#dashboard-social-chart"), socialOptions);
	  chart.render();  
	}

	function blockUI() {
	  var $white = '#fff';
	  $.blockUI({
	    message: '<div class="semibold"><span class="bx bx-revision icon-spin text-left"></span>&nbsp; Please Wait...</div>',
	    overlayCSS: {
	      backgroundColor: $white,
	      opacity: 0.8,
	      cursor: 'wait'
	    },
	    css: {
	      border: 0,
	      padding: 0,
	      backgroundColor: 'transparent'
	    }
	  });
	}
	function unBlockUI() {
	  $.unblockUI();
	}
	function printDoc() {
		setTimeout(function(){ 
			unBlockUI();
			window.print(); 
		}, 3000);  
	}


</script>