<p style="text-align: center;"><img src="{{url($siteLogo)}}" class="img-responsive" style="height: 100px;"></p>  
<h3 style="text-align: center;">Visits</h3>
<table style="width: 100%">
  <tr>
     <th style="text-align: left;">Date</th>
     <th style="text-align: left;">Visits</th>
  </tr> 
  @foreach( $pdfData['dashboardData']['visits']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $pdfData['dashboardData']['visits']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Page Views</h3>
<table style="width: 100%">
  <tr>
     <th style="text-align: left;">Date</th>
     <th style="text-align: left;">Visits</th> 
  </tr>
  @foreach( $pdfData['dashboardData']['pageViews']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $pdfData['dashboardData']['pageViews']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Average Time</h3>
<table style="width: 100%">
  <tr>
     <th style="text-align: left;">Date</th>
     <th style="text-align: left;">Time</th>
  </tr>
  @foreach( $pdfData['dashboardData']['avgTime']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $pdfData['dashboardData']['avgTime']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Bounce Rate</h3>
<table style="width: 100%">
  <tr>
     <th style="text-align: left;">Date</th>
     <th style="text-align: left;">Rate</th>
  </tr>
  @foreach( $pdfData['dashboardData']['bounceRate']['date'] as $key => $date )
    <tr>
      <td>{{ $date }} </td>
      <td>{{ $pdfData['dashboardData']['bounceRate']['views'][$key] }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Social Visits</h3>
<table style="width: 100%">
  <tr>
     <th style="text-align: left;">Date</th>
     <th style="text-align: left;">Visit</th>
  </tr>
  @foreach( $pdfData['socialData']['data'] as $key => $date )
    <tr>
      <td>{{ $key }} </td>
      <td>{{ $date }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Channel Data</h3>
<table style="width: 100%">
  <tr>
     <th style="text-align: left;">Type</th>
     <th style="text-align: left;">Visit</th>
  </tr>
  @foreach( $pdfData['channelData']['data'] as $key => $date )
    <tr>
      <td>{{ $key }} </td>
      <td>{{ $date }} </td>
    </tr>
  @endforeach
</table>

<h3 style="text-align: center;">Device Data</h3>
<table style="width: 100%">
  <tr>
     <th style="text-align: left;">Device</th>
     <th style="text-align: left;">Visit</th>
  </tr>
  @foreach( $pdfData['deviceData']['data'] as $key => $date )
    <tr>
      <td>{{ $key }} </td>
      <td>{{ $date }} </td>
    </tr>
  @endforeach
</table> 

@if(isset($pdfData['keywords']))
    <h3 style="text-align: center;">Keywords</h3>
    <table style="width: 100%">
      <tr>
         <th style="text-align: left;">Keyword</th>
         <th style="text-align: left;">Rank</th>
         <th style="text-align: left;">Url</th>
      </tr>
      @foreach( $pdfData['keywords'] as $keyword)
        <tr>
          <td>{{ $keyword->keyword }} </td>
          <td>{{ $keyword->keyword_rank }} </td>
          <td>{{ $keyword->url }} </td>
        </tr>
      @endforeach
    </table> 
@endif


<h3 style="text-align: center;">Top Page Views</h3>
<table style="width: 100%">
  <tr>
     <th style="text-align: left;">Page Title</th>
     <th style="text-align: left;">Page Url</th>
     <th style="text-align: left;">Views</th>
  </tr>
  @foreach( $pdfData['topPageViews']['rows'] as $pageV)
    <tr>
      <td>{{ $pageV[1] }} </td>
      <td>{{ $pageV[0] }} </td>
      <td>{{ $pageV[2] }} </td> 
    </tr>
  @endforeach
</table> 