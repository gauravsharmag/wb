@extends('layouts.contentLayoutMaster')
@section('title','Reviews')
@section('vendor-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.css" integrity="sha512-H6KANMpZQKsK9c09UqdcQv02JTiZ/hMVwxkcbDLrp125CR884wwEdnWDm+Yuo6tihuC/cizLYWAjMZi0xAfGig==" crossorigin="anonymous" />
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-users.css')}}">
@endsection
@section('content')
<!-- users list start -->
<section class="users-list-wrapper">
  <div class="users-list-filter px-1">
    <form>  
      <div class="row border rounded py-2 mb-2">
        <div class="col-md-4">
        </div>
        <div class="col-md-4"> 
            <span>Total Reviews {{$reviewArr['totalReviews']}}</span> 
            <div id="avg-review"> </div>
        </div>
        <div class="col-md-4"> 
            
        </div>
      </div>
    </form>
    
  </div>
  <div class="users-list-table">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <div class="table-responsive">
            <table id="users-list-datatable" class="table">
              <thead>
                <tr> 
                    <th>Rating</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Review</th>
                </tr>
              </thead>
              <tbody>
              @foreach($reviewArr['review'] as $rev)
                <tr>
                  <td>{{$rev['rating']}} </td>
                  <td>{{$rev['date']}} </td>
                  <td>{{$rev['reviewer']}} </td>
                  <td>{{$rev['review']}} </td>
                </tr>
              @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- users list ends -->
@endsection

@section('vendor-scripts')
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.js" integrity="sha512-y/fY9i5oZuCdGyNN/S4MV8lT9t3I8ztPJ0+5OqeXWswT19mt9QruHXAzSBazb3JCJD4/49HvmCYkEHE+/iT1pQ==" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(function() {  $('#users-list-datatable').DataTable();    }); 
 
    
    buildReviewChart();

    function buildReviewChart(total,avg) {
      var $primary = '#5A8DEE';
      var $success = '#39DA8A';
      var $danger = '#FF5B5C';
      var $warning = '#FDAC41';
      var $info = '#00CFDD';
      var $label_color = '#475f7b';
      var $primary_light = '#E2ECFF';
      var $danger_light = '#ffeed9';
      var $gray_light = '#828D99';
      var $sub_label_color = "#596778";
      var $radial_bg = "#e7edf3";
      
      
      var totalReviews = "{{$reviewArr['totalReviews']}}";
      var totalStars = "{{$reviewArr['totalStars']}}";
      var avgStars = "{{$reviewArr['avgStars']}}";
      avgStars = parseFloat(avgStars);
      var labelsChannelArr=['Total Rating','Avg Rating'];
      var labelsChannelDataArr=[5,avgStars]; 
      

      var channelChartOption = {
        chart: {
          height: 170,
          type: 'donut',
        },
        dataLabels: {
          enabled: true,
          formatter: function (val, opts) {
            return opts.w.config.series[opts.seriesIndex]
          }  
        },
        series: labelsChannelDataArr,
        labels: labelsChannelArr,
        stroke: {
          width: 0,
          lineCap: 'round',
        },
        colors: [$primary, $info, $warning], 
        plotOptions: {
          pie: {
            donut: {
              size: '50%',
              labels: {
                show: true,
                name: {
                  show: true,
                  fontSize: '15px',
                  colors: $sub_label_color,
                  offsetY: 20,
                  fontFamily: 'IBM Plex Sans',
                },
                value: {
                  show: true,
                  fontSize: '26px',
                  fontFamily: 'Rubik',
                  color: $label_color,
                  offsetY: -20,
                  formatter: function (val) {
                    return val
                  }
                },
                total: {
                  show: true,
                  label: 'Ratings',
                  color: $gray_light,
                  formatter: function (w) {
                    return w.globals.seriesTotals.reduce(function (a, b) {
                      return  b
                    }, 0)
                  }
                }
              }
            }
          }
        },
        legend: {
          show: true
        }
      }
      chart = new ApexCharts(document.querySelector("#avg-review"),channelChartOption);
      chart.render();
    }


    
</script>

@endsection
