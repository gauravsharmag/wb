@extends('layouts.contentLayoutMaster')
@section('title','Support Ticket')
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
@endsection
@section('content')

  <section id="dashboard-analytics">
      <div class="row match-height">
             
          <div class="col-md-8 col-12">
                <div class="card" >
                  <div class="card-header">
                    <h4 class="card-title"> {{__('Support Ticket')}} </h4>
                  </div>
                  <div class="card-content">
                    <div class="card-body">  
                        <div class="form-body">
                          <div class="row">

                            <div class="col-md-4">
                              <label>Name</label>
                            </div>
                            <div class="col-md-8 form-group">
                              {{$support->name}}
                            </div>

                            <div class="col-md-4">
                              <label>Email</label>
                            </div>
                            <div class="col-md-8 form-group">
                              {{$support->email}}
                            </div>

                            <div class="col-md-4">
                              <label>Question</label>
                            </div>
                            <div class="col-md-8 form-group">
                              {{$support->question}}
                            </div>

                            <div class="col-md-4">
                              <label>Submitted On</label>
                            </div>
                            <div class="col-md-8 form-group">
                              {{date('d M,Y H:i:s',strtotime($support->created_at))}}
                            </div>

                          </div>
                        </div>
                    </div>
                  </div>
                </div>
          </div>
      </div>
  </section>
  
@endsection