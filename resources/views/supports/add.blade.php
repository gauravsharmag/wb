@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title','Submit Support Request')
{{-- venodr style --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
@endsection

{{-- page style --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
@endsection

@section('content')

	<section id="dashboard-analytics">
	    <div class="row match-height">
	      		
	      	<div class="col-md-8 col-12">
	      	      <div class="card" > 
	      	        <div class="card-header">
	      	          <h4 class="card-title"> {{__('Submit Support Request')}} </h4>
	      	        </div>
	      	        <div class="card-content">
	      	          <div class="card-body">
	      	            <form class="form form-horizontal" method="POST" action="/support/add" >
	      	            	@csrf
	      	              <div class="form-body">
	      	                <div class="row">
	      	                
	      	                	

	      	                	<div class="col-md-4">
	      	                	  <label>Name</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ Auth::user()->name }}" readonly="true">
	      	                	  @error('name')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Email</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" readonly="true"> 
	      	                	  @error('email')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Question</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <textarea id="question" class="form-control @error('question') is-invalid @enderror" name="question" cols="60" rows="6"> 
	      	                	  	{{old('question')}}
	      	                	  </textarea>
	      	                	  @error('question') 
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	

	      	                  
	      	                  <div class="col-sm-12 d-flex justify-content-end">
	      	                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{__('Submit')}} </button>
	      	                  </div>
	      	                </div>
	      	              </div>
	      	            </form>
	      	          </div>
	      	        </div>
	      	      </div>
	      	</div>

	    </div>
	</section>



@endsection