@extends('layouts.contentLayoutMaster')
@section('title','Supports')
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-users.css')}}">
@endsection
@section('content')
<section class="users-list-wrapper">
 
  <div class="users-list-table">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <div class="table-responsive">
            <table id="support" class="table">
              <thead>
                <tr> 
                    <th>User</th>
                    <th>Email</th>
                    <th>Date & Time</th>
                    <th>Action</th> 
                </tr>
              </thead>
              <tbody>
                 

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('vendor-scripts')
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
    $(function() {  
      $('#support').DataTable({
          responsive: true,
          processing: true,  
          serverSide: true,
          ordering: false,
          ajax: "{{ route('getSupportsAjax') }}",      
          lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
          columns: [
                { data: 'name', name: 'name' }, 
                { data: 'email', name: 'email' },
                { data: 'created_at', name: 'created_at'}, 
                { data: 'action', name: 'action'}    
          ], 
      });
    }); 
</script>

@endsection
