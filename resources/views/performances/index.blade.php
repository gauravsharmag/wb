@extends('layouts.contentLayoutMaster')
@section('title','Performance')
@section('vendor-styles')
<!-- <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}"> -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.css" integrity="sha512-H6KANMpZQKsK9c09UqdcQv02JTiZ/hMVwxkcbDLrp125CR884wwEdnWDm+Yuo6tihuC/cizLYWAjMZi0xAfGig==" crossorigin="anonymous" />
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
@endsection

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" />
@endsection
@section('content')
 
  <section id="dashboard-analytics">
    <div class="users-list-filter px-1">
      <form> 
        <div class="row border rounded py-2 mb-2">
          <div class="col-md-6">
            <?php  $first  = array_values($viewArr); $firstKey = array_keys($viewArr); ?>
            <p>
                Showing data for {{$first[0]}} <span id="durationMessage"></span>
            </p>
          </div>
          <div class="col-md-2"> 
            
            
            <input type="hidden" name="view_id" id="view_id" value="{{$firstKey[0]}}">
          </div>
          <div class="col-md-2">
            <input type="text" name="start" id="start" placeholder="Start Date" autocomplete="off">
          </div> 
          <div class="col-md-2">
            <input type="text" name="end" id="end" placeholder="End Date" autocomplete="off">  
          </div>
        </div>
      </form>
      <div class="row border rounded py-2 mb-2">
        <div class="col-md-6">
          
        </div>
        <div class="col-md-4">
          
        </div>
        <div class="col-md-2"> 
          <a href="javascript:;" onclick="exportCharts('pdf')">Export PDF</a> &nbsp;
          <a href="javascript:;" onclick="exportCharts('csv')">Export CSV</a>
        </div>
      </div>
    </div> 

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card" id="visitsCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Visits (<span id="dashboard-visits-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-visits-chart"></div>
                <div id="dashboard-visits-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div> 
        <div class="col-md-6 col-sm-12">
          <div class="card" id="pageViewsCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Page Views (<span id="dashboard-pageviews-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-pageviews-chart"></div>
                <div id="dashboard-pageviews-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card" id="avgTimeCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Average Time On Site</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-avgtime-chart"></div>
                <div id="dashboard-avgtime-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card" id="bounceRateCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Bounce Rate</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-bouncerate-chart"></div>
                <div id="dashboard-bouncerate-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card" id="organicTrafficCard"> 
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Organic Traffic (<span id="dashboard-organic-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-organic-traffic-chart" ></div>
                <div id="dashboard-organic-traffic-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card" id="referralTrafficCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Referral Traffic (<span id="dashboard-referral-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1"> 
                <div id="dashboard-referral-chart" ></div>
                <div id="dashboard-referral-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card" id="directTrafficCard"> 
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Direct Traffic (<span id="dashboard-direct-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-direct-traffic-chart" ></div>
                <div id="dashboard-direct-traffic-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card" id="paidSearchTrafficCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Paid Search Traffic (<span id="dashboard-paidsearch-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1"> 
                <div id="dashboard-paidsearch-traffic-chart" ></div>
                <div id="dashboard-paidsearch-traffic-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card" id="socialTrafficCard"> 
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Social Media Traffic (<span id="dashboard-social-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-social-traffic-chart" ></div>
                <div id="dashboard-social-traffic-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card" id="visitsComputerCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Visits From Computers (<span id="dashboard-computer-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1"> 
                <div id="dashboard-visits-computer-chart" ></div>
                <div id="dashboard-visits-computer-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card" id="visitsPhoneCard"> 
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Visits From Phones (<span id="dashboard-phone-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-visits-phone-chart" ></div>
                <div id="dashboard-visits-phone-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div> 
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card" id="visitsTabletsCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Visits From Tablets (<span id="dashboard-tablet-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1"> 
                <div id="dashboard-visits-tablet-chart" ></div>
                <div id="dashboard-visits-tablet-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <!-- <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="card" id="goalsCard"> 
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Goals Data</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-visits-phone-chart" ></div>
                <div id="dashboard-visits-phone-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div> --> 


  </section>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
  <!-- <script src="{{asset('vendors/js/charts/apexcharts.min.js')}}"></script> -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.js" integrity="sha512-y/fY9i5oZuCdGyNN/S4MV8lT9t3I8ztPJ0+5OqeXWswT19mt9QruHXAzSBazb3JCJD4/49HvmCYkEHE+/iT1pQ==" crossorigin="anonymous"></script>
  <script src="{{asset('vendors/js/ui/blockUI.min.js')}}"></script>
  <script src="{{asset('js/custom/performance.js')}}"></script>   
    
@endsection

@section('vendor-scripts')
<script src="{{asset('vendors/js/extensions/dragula.min.js')}}"></script>
@endsection



