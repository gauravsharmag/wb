@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title','User Profile')
{{-- vendor style --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/swiper.min.css')}}">
@endsection
{{-- page-styles --}} 
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-user-profile.css')}}">
@endsection
@section('content')
<!-- page user profile start -->
<section class="page-user-profile">
  <div class="row">
    <div class="col-12">
      <!-- user profile heading section start -->
      <div class="card">
        <div class="card-content">
          <div class="user-profile-images">
            <!-- user timeline image -->
           <!--  <img src="{{asset('images/profile/post-media/profile-banner.jpg')}}"
              class="img-fluid rounded-top user-timeline-image" alt="user timeline image"> -->
            <!-- user profile image -->
           <!--  <img src="{{asset('images/portrait/small/avatar-s-16.jpg')}}" class="user-profile-image rounded"
              alt="user profile image" height="140" width="140"> -->
          </div>
          <!-- <div class="user-profile-text">
            <h4 class="mb-0 text-bold-500 profile-text-color">Martina Ash</h4>
            <small>Devloper</small>
          </div> -->
          <!-- user profile nav tabs start -->
          <div class="card-body px-0">

          <div class="row"> 
              <div class="col-md-1"> 
                 
              </div>
              <div class="col-md-11">
                 <form class="form form-horizontal" method="POST" action="/users/profile" enctype="multipart/form-data">
                  @csrf
                   <div class="form-body"> 
                     <div class="row">
                       <input type="hidden" name="id" value="{{$profile->id}}">
                      

                      <div class="col-md-4">
                        <label> Contact Name</label>
                      </div>
                      <div class="col-md-8 form-group">
                        <input type="text" id="client_name" class="form-control @error('client_name') is-invalid @enderror" name="client_name" value="{{$profile->name}}">
                        @error('client_name')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                      </div> 

                      <div class="col-md-4">
                        <label>Contact Email</label>
                      </div>
                      <div class="col-md-8 form-group">
                        <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$profile->email}}">
                        @error('email')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                      </div>
  
                      <div class="col-md-4">
                        <label>Phone#</label>
                      </div>
                      <div class="col-md-8 form-group">
                        <input type="number" id="phone" class="form-control @error('email') is-invalid @enderror" name="phone" value="{{$profile->phone}}">
                        @error('phone')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                      </div>

                      <div class="col-md-4">
                        <label>Website Url</label>
                      </div>
                      <div class="col-md-8 form-group">
                          <input type="text" id="website_url" class="form-control" name="website_url" value="{{$profile->website_url}}">
                          @error('website_url') 
                            <span class="invalid-feedback" role="alert" style="display: block;">
                              <strong>{{ $message }}</strong> 
                            </span>
                          @enderror  
                      </div>

                      

                      <div class="col-md-4">
                        <label>Upload Logo</label>
                      </div>
                      <div class="col-md-8 form-group">
                        <input type="file" id="website_logo" class="form-control" name="website_logo" > 
                        @error('website_logo')
                          <span class="invalid-feedback" role="alert" style="display: block !important;">
                            <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                      </div>

                      <div class="col-md-4">
                        <label>Address</label>
                      </div>
                      <div class="col-md-8 form-group">
                        <input type="text" id="address" class="form-control" name="address" value="{{$profile->address}}">
                      </div>
                      <div class="col-md-4">
                        <label>State</label>
                      </div>
                      <div class="col-md-8 form-group">
                        {!! Form::select('state',$states,$profile->state_id,['class'=>'form-control','id'=>'state','placeholder'=>'Select State'] ) !!} 
                        @error('state')  
                          <span class="invalid-feedback" role="alert" style="display: block;">
                            <strong>{{ $message }}</strong> 
                          </span>
                        @enderror
                      </div>
                      <div class="col-md-4">
                        <label>City</label>
                      </div>
                      <div class="col-md-8 form-group">
                        <input type="text" id="city" class="form-control" name="city" value="{{$profile->city}}">
                        @error('city')  
                          <span class="invalid-feedback" role="alert" style="display: block;">
                            <strong>{{ $message }}</strong> 
                          </span>
                        @enderror
                      </div>
                      <div class="col-md-4">
                        <label>Zip</label>
                      </div>
                      <div class="col-md-8 form-group">
                        <input type="text" id="zip" class="form-control" name="zip" value="{{$profile->zip}}">
                        @error('zip')  
                          <span class="invalid-feedback" role="alert" style="display: block;">
                            <strong>{{ $message }}</strong>  
                          </span>
                        @enderror
                      </div>
                     
                       <div class="col-sm-12 d-flex justify-content-end">
                         <button type="submit" class="btn btn-primary mr-1 mb-1">{{__('Submit')}} </button>
                       </div>
                     </div>
                   </div>
                 </form>
              </div>
          </div>

            <!-- <ul
              class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0"
              role="tablist">
              <li class="nav-item pb-0">
                <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed"
                  aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span
                    class="d-none d-md-block">Feed</span></a>
              </li>
              <li class="nav-item pb-0">
                <a class="nav-link d-flex px-1" id="activity-tab" data-toggle="tab" href="#activity"
                  aria-controls="activity" role="tab" aria-selected="false"><i class="bx bx-user"></i><span
                    class="d-none d-md-block">Activity</span></a>
              </li>
              <li class="nav-item pb-0">
                <a class="nav-link d-flex px-1" id="friends-tab" data-toggle="tab" href="#friends"
                  aria-controls="friends" role="tab" aria-selected="false"><i class="bx bx-message-alt"></i><span
                    class="d-none d-md-block">Friends</span></a>
              </li>
              <li class="nav-item pb-0 mr-0">
                <a class="nav-link d-flex px-1" id="profile-tab" data-toggle="tab" href="#profile"
                  aria-controls="profile" role="tab" aria-selected="false"><i class="bx bx-copy-alt"></i><span
                    class="d-none d-md-block">Profile</span></a>
              </li>
            </ul> -->
          </div>
          <!-- user profile nav tabs ends -->
        </div>
      </div>
      <!-- user profile heading section ends -->

    </div>
  </div>
</section>
<!-- page user profile ends -->
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('vendors/js/extensions/swiper.min.js')}}"></script>
@endsection
{{-- page scripts --}}
@section('page-scripts')
<script src="{{asset('js/scripts/pages/page-user-profile.js')}}"></script>
@endsection