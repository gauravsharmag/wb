@extends('layouts.contentLayoutMaster')
@section('title','Admin Dashboard')
@section('vendor-styles')
<!-- <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.css" integrity="sha512-H6KANMpZQKsK9c09UqdcQv02JTiZ/hMVwxkcbDLrp125CR884wwEdnWDm+Yuo6tihuC/cizLYWAjMZi0xAfGig==" crossorigin="anonymous" />
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
@endsection

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" />
@endsection
@section('content')
 
  <!-- Dashboard Analytics Start --> 
  <section id="dashboard-analytics">
    <div class="users-list-filter px-1">
      <form>
        <div class="row border rounded py-2 mb-2">
          <div class="col-md-4">
            <p id="durationMessage"> </p>
          </div>
          <div class="col-md-4">
            <p> Select the google analytic view id to view the results </p>
            {!! Form::select('view_id',$views,null,['class'=>'form-control','id'=>'view_id'] ) !!}  
          </div>
          <div class="col-md-2">
             <input type="text" name="start" id="start" placeholder="Start Date" autocomplete="off">
          </div> 
          <div class="col-md-2">
             <input type="text" name="end" id="end" placeholder="End Date" autocomplete="off">  
          </div>
        </div>
      </form>
      <div class="row border rounded py-2 mb-2">
        <div class="col-md-6">
          
        </div>
        <div class="col-md-4">
          
        </div>
        <div class="col-md-2">
          <a href="javascript:;" onclick="exportCharts('pdf')">Export PDF</a> &nbsp;
          <a href="javascript:;" onclick="exportCharts('csv')">Export CSV</a>
        </div>
      </div>
    </div> 

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card" id="visitsCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Visits (<span id="dashboard-visits-count"></span>)</h4> 
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <!-- <div id="analytics-bar-chart"></div> -->
                <div id="dashboard-visits-chart"></div>
                <div id="dashboard-visits-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>  
        <div class="col-md-6 col-sm-12">
          <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Social/Referral (<span id="dashboard-social-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-social-chart"></div>
                <div id="dashboard-social-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card"> 
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Page Views (<span id="dashboard-pageviews-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-pageviews-chart"></div>
                <div id="dashboard-pageviews-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card" id="pageViewTable"> 
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Top Page Views</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <table class="table">
                    <thead> 
                      <tr>
                        <th>Page Title</th>
                        <th>Page Url</th>
                        <th>Visits</th>
                      </tr>
                    </thead>
                    <tbody id="topPageViewBody">
                      
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card" id="channelsCard"> 
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Channels (<span id="dashboard-channels-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-channels-chart" ></div>
                <div id="dashboard-channels-chart-error" style="text-align:center;"></div>
              </div> 
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card" id="devicesCard">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Devices (<span id="dashboard-devices-count"></span>)</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1"> 
                <div id="dashboard-devices-chart" ></div>
                <div id="dashboard-devices-chart-error" style="text-align:center;"></div>
                <!-- <div id="donut-chart" class="d-flex justify-content-center"></div> -->
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Average Time</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-avgtime-chart"></div>
                <div id="dashboard-avgtime-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
              <h4 class="card-title">Bounce Rate</h4>
              <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
            </div>
            <div class="card-content">
              <div class="card-body pb-1">
                <div id="dashboard-bouncerate-chart"></div>
                <div id="dashboard-bouncerate-chart-error" style="text-align:center;"></div>
              </div>
            </div>
          </div>
        </div>
    </div>

   <!--  <div class="row">
      <div class="col-md-12 col-sm-6">
        <div class="card">
          <div class="card-header d-flex justify-content-between align-items-center">
            <h4 class="card-title">Keywords</h4>
            <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
          </div>
          <div class="card-content">
            <div class="card-body pb-1">
              <div class="table-responsive">
                <table id="keywordsTable" class="table">
                  <thead>
                    <tr> 
                        <th>Keyword</th>
                        <th>Rank</th>
                        <th>Page Url</th>
                    </tr>
                  </thead>
                  <tbody> 
                  
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->


  </section>
  <!-- Dashboard Analytics end -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
  <!-- <script src="{{asset('vendors/js/charts/apexcharts.min.js')}}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/apexcharts/3.21.0/apexcharts.min.js" integrity="sha512-y/fY9i5oZuCdGyNN/S4MV8lT9t3I8ztPJ0+5OqeXWswT19mt9QruHXAzSBazb3JCJD4/49HvmCYkEHE+/iT1pQ==" crossorigin="anonymous"></script>
  <script src="{{asset('vendors/js/ui/blockUI.min.js')}}"></script>
  <script src="{{asset('js/custom/admin_dashboard.js')}}"></script>  
  
@endsection

@section('vendor-scripts')
<script src="{{asset('vendors/js/extensions/dragula.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({  
          url:'/get-analytics-data',
          success:function(res){ }
        });
    }); 
    function getKeywords() {  
      $('#keywordsTable').DataTable({
        responsive: true,
        processing: true,  
        serverSide: true, 
        bDestroy:true, 
        ajax: "/keywords/get",    
        lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
        columns: [
              { data: 'keyword', name: 'keyword' },
              { data: 'keyword_rank', name: 'keyword_rank' },
              { data: 'url', name: 'url' }
        ] 
      });
    } 
</script>


@endsection

@section('page-scripts')
<!-- <script src="{{asset('js/scripts/pages/dashboard-analytics.js')}}"></script> -->
@endsection


