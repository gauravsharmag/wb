@extends('layouts.contentLayoutMaster')
@section('title','Profile')
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
@endsection
@section('content')

  <section id="dashboard-analytics">
      <div class="row match-height">
             
          <div class="col-md-8 col-12">
                <div class="card" >
                  <div class="card-header">
                    <h4 class="card-title"> {{__('Profile')}} </h4>
                  </div>
                  <div class="card-content">
                    <div class="card-body"> 

                      <form class="form form-horizontal" method="POST" action="/users/admin-profile" enctype="multipart/form-data"> 
                      @csrf
                        <input type="hidden" name="id" value="{{$profile->id}}">
                        <div class="form-body">
                          <div class="row">

                            <div class="col-md-4">
                              <label>Name</label>
                            </div>
                            <div class="col-md-8 form-group">
                              <input type="text" id="name" class="form-control" name="name" value="{{$profile->address}}">
                            </div> 

                            <div class="col-md-4">
                              <label>Email</label>
                            </div> 
                            <div class="col-md-8 form-group">
                              <input type="email" id="email" class="form-control" name="email" value="{{$profile->email}}">
                            </div>
                            
                            <div class="col-md-4">
                              <label>Phone#</label>
                            </div>
                            <div class="col-md-8 form-group">
                              <input type="number" id="phone" class="form-control" name="phone" value="{{$profile->phone}}">
                            </div> 

                            <div class="col-md-4">
                              <label>Address</label>
                            </div>
                            <div class="col-md-8 form-group">
                              <input type="text" id="address" class="form-control" name="address" value="{{$profile->address}}">
                            </div>
                            <div class="col-md-4">
                              <label>State</label>
                            </div>
                            <div class="col-md-8 form-group">
                              {!! Form::select('state',$states,$profile->state_id,['class'=>'form-control','id'=>'state','placeholder'=>'Select State'] ) !!} 
                              @error('state')  
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                  <strong>{{ $message }}</strong> 
                                </span>
                              @enderror
                            </div>
                            <div class="col-md-4">
                              <label>City</label>
                            </div>
                            <div class="col-md-8 form-group">
                              <input type="text" id="city" class="form-control" name="city" value="{{$profile->city}}">
                              @error('city')  
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                  <strong>{{ $message }}</strong> 
                                </span>
                              @enderror 
                            </div>
                            <div class="col-md-4">
                              <label>Zip</label>
                            </div>
                            <div class="col-md-8 form-group">
                              <input type="text" id="zip" class="form-control" name="zip" value="{{$profile->zip}}">
                              @error('zip')  
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                  <strong>{{ $message }}</strong>  
                                </span>
                              @enderror
                            </div>
                            <div class="col-md-4">
                              <label>Upload Pic</label>
                            </div>
                            <div class="col-md-8 form-group">
                              <input type="file" id="pic" class="form-control" name="pic" > 
                              @error('pic') 
                                <span class="invalid-feedback" role="alert" style="display: block !important;">
                                  <strong>{{ $message }}</strong>
                                </span>
                              @enderror
                              <img src="/images/users/{{$profile->website_logo}}" class="img-responsive" style="height: 100px;">
                            </div>
                            <div class="col-sm-12 d-flex justify-content-end">
                              <button type="submit" class="btn btn-primary mr-1 mb-1">{{__('Submit')}} </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
          </div>
      </div>
  </section>
  
@endsection