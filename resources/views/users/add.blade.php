@extends('layouts.contentLayoutMaster')
@section('title','Add Client')

@section('content')
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js" integrity="sha512-rMGGF4wg1R73ehtnxXBt5mbUfN9JUJwbk21KMlnLZDJh7BkPmeovBuddZCENJddHYYMkCh9hPFnPmS9sspki8g==" 
	crossorigin="anonymous"></script>
 

	<link rel="stylesheet" type="text/css" href="{{asset('css/plugins/forms/wizard.css')}}">
	 
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>




  	<section id="vertical-wizard"> 
  	   <div class="card">
  	      <div class="card-header">
  	         <h4 class="card-title">{{__('Add New Client')}}</h4>
  	      </div>
  	      <div class="card-content">
  	         <div class="card-body">
  	            <form  class="wizard-vertical" method="POST" action="/users/add" enctype="multipart/form-data">
  	               <!-- step 1 --> 
  	               @csrf
  	               <h3>
  	                  <span class="fonticon-wrap mr-1">
  	                  <i class="livicon-evo" data-options="name:gear.svg; size: 50px; style:lines; strokeColor:#adb5bd;"></i>
  	                  </span>
  	                  <span class="icon-title">
  	                  <span class="d-block">Users Details</span>
  	                  <small class="text-muted">Setup user account details here.</small>
  	                  </span>
  	               </h3>
  	               <!-- step 1 end-->
  	               <!-- step 1 content -->
  	               <fieldset class="pt-0">
  	                  <h6 class="pb-50">Enter User Account Details</h6>
  	                  <div class="row">
  	                    <div class="col-sm-6">
  	                        <div class="form-group">
  	                           <label for="firstName12">GA Account </label>
  	                          	{!! Form::select('ga_account[]',$gaAccounts,null,['class'=>'form-control','id'=>'ga_account','placeholder'=>'Select Ga Account'] ) !!}	
  	                           @error('ga_account')
  	                             <span class="invalid-feedback" role="alert" style="display:block;">
  	                               <strong>{{ $message }}</strong>
  	                             </span>
  	                           @enderror
  	                        </div>
  	                    </div>
  	                    <div class="col-sm-6">
                          <div class="form-group">
                             <label for="firstName12">Business Address </label>
                              <input type="text" id="address" class="form-control" name="address" value="{{old('address')}}">
                              @error('address') 
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                  <strong>{{ $message }}</strong>
                                </span>
                              @enderror
                          </div>
  	                    </div>
  	                  </div>

  	                  <div class="row">
  	                    <div class="col-sm-6">
                          <div class="form-group">
                             <label for="firstName12">CID (Customer identification number from google my business) </label>
                              <input type="text" id="cid" class="form-control @error('cid') is-invalid @enderror" name="cid" value="{{old('cid')}}"> 
                              @error('cid')
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                  <strong>{{ $message }}</strong>
                                </span>
                              @enderror
                          </div>
                          
  	                    </div>
  	                    <div class="col-sm-6">
                            <div class="form-group">
                               <label for="firstName12">State </label>
                                {!! Form::select('state',$states,null,['class'=>'form-control','id'=>'state','placeholder'=>'Select State'] ) !!} 
                                @error('state')  
                                  <span class="invalid-feedback" role="alert" style="display: block;">
                                    <strong>{{ $message }}</strong> 
                                  </span>
                                @enderror
                            </div> 
  	                    </div>
  	                  </div>

  	                  <div class="row">
  	                    <div class="col-sm-6">
                            <div class="form-group">
                               <label for="firstName12">Client Name </label>
                                <input type="text" id="client_name" class="form-control @error('client_name') is-invalid @enderror" name="client_name" value="{{old('client_name')}}">
                                @error('client_name')
                                  <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                @enderror
                            </div>
  	                    </div>
  	                    <div class="col-sm-6">
  	                        <div class="form-group">
                               <label for="firstName12">City</label>
                                <input type="text" id="city" class="form-control" name="city" > 
                                @error('city')
                                  <span class="invalid-feedback" role="alert" style="display: block !important;">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                @enderror
                            </div>
  	                    </div>
  	                  </div>

  	                  <div class="row">
  	                    <div class="col-sm-6">
                            <div class="form-group">
                               <label for="firstName12">Contact Name </label>
                                <input type="text" id="contact_name" class="form-control @error('email') is-invalid @enderror" name="contact_name" value="{{old('contact_name')}}">
                                @error('contact_name')
                                  <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                @enderror
                            </div>
  	                    </div>
  	                    <div class="col-sm-6">
                          <div class="form-group">
                             <label for="firstName12">Zip </label>
                              <input type="number" id="zip" class="form-control" name="zip" value="{{old('zip')}}">
                              @error('zip') 
                                <span class="invalid-feedback" role="alert" style="display: block;">
                                  <strong>{{ $message }}</strong> 
                                </span>
                              @enderror
                          </div>
  	                    </div>
  	                  </div>

  	                  <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                               <label for="firstName12">Client Email </label>
                                <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{old('email')}}">
                                @error('email')
                                  <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                @enderror
                            </div>
                        </div>
  	                    <div class="col-sm-6">
                            <div class="form-group">
                               <label for="firstName12">Logo</label>
                                <input type="file" id="website_logo" class="form-control" name="website_logo" accept=".png,.jpg,.jpeg"> 
                                @error('website_logo')
                                  <span class="invalid-feedback" role="alert" style="display: block !important;">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                @enderror
                            </div>
  	                    </div> 
  	                  </div>
  	                  <div class="row">
                        <div class="col-sm-6"> 
                            <div class="form-group">
                               <label for="firstName12">Phone# </label>
                                <input type="number" id="phone" class="form-control @error('email') is-invalid @enderror" name="phone" value="{{old('phone')}}">
                                @error('phone')
                                  <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                @enderror
                            </div>
                        </div>
  	                    <div class="col-sm-6">
  	                        <div class="form-group">
                               <label for="firstName12">Website Url </label>
                                <input type="text" id="website_url" class="form-control" name="website_url" value="{{old('website_url')}}">
                                @error('website_url') 
                                  <span class="invalid-feedback" role="alert" style="display: block;">
                                    <strong>{{ $message }}</strong> 
                                  </span>
                                @enderror
                            </div>
  	                    </div>
  	                  </div>
  	                  
  	               </fieldset>
  	               <!-- step 1 content end-->
  	               <!-- step 2 -->
  	               <h3>
  	                  <span class="fonticon-wrap mr-1">
  	                  <i class="livicon-evo"
  	                     data-options="name:location.svg; size: 50px; style:lines; strokeColor:#adb5bd;"></i>
  	                  </span>
  	                  <span class="icon-title">
  	                  <span class="d-block">KeyWords</span>
  	                  <small class="text-muted">Add Keywords.</small>
  	                  </span>
  	               </h3>
  	               <!-- step 2 end-->
  	               <!-- step 2 content -->
  	               <fieldset class="pt-0">
  	                  <h6 class="py-50">Setup Keywords</h6>
  	                  <div class="row">
  	                     <div class="col-sm-12">
  	                        <div class="form-group">
  	                           <label for="proposalTitle1">Add Keywords <small>(Enter new line seprated values)</small> </label> 
  	                          <textarea class="form-control" name="keywords" id="keywords" style="height:200px;">
  	                          	 
  	                          </textarea> 

  	                        </div>
  	                     </div> 
  	                  </div>
  	                  <div class="row">
                        OR
  	                     <div class="col-sm-12">
  	                        <div class="form-group">
  	                          <label for="proposalTitle1">Upload Keywords </label>
  	                          <input type="file" id="keywords_file" name="keywords_file" class="form-control"> 
  	                        </div>
  	                     </div>
  	                     
  	                  </div>
  	               </fieldset>
  	               <!-- step 2 content end-->
  	            </form> 
  	         </div>
  	      </div>
  	   </div>
  	</section>


 
	
	<script type="text/javascript">
		/*$('#ga_account').chosen({
			placeholder_text_multiple: "Select GA Accounts",
		});*/

		$(document).ready(function(){
			$('#keywords_file').change(function(){
		        var fileUpload = $("#keywords_file")[0];
		        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
		        if (regex.test(fileUpload.value.toLowerCase())) {
		            if (typeof (FileReader) != "undefined") {
		                var reader = new FileReader();
		                //For Browsers other than IE. 
		                if (reader.readAsBinaryString) {
		                    reader.onload = function (e) {
		                        ProcessExcel(e.target.result);
		                    };
		                    reader.readAsBinaryString(fileUpload.files[0]);
		                } else {
		                    //For IE Browser.
		                    reader.onload = function (e) {
		                        var data = "";
		                        var bytes = new Uint8Array(e.target.result);
		                        for (var i = 0; i < bytes.byteLength; i++) {
		                            data += String.fromCharCode(bytes[i]);
		                        }
		                        ProcessExcel(data);
		                    };
		                    reader.readAsArrayBuffer(fileUpload.files[0]);
		                }
		            } else {
		                alert("This browser does not support HTML5.");
		            }
		        } else {
		            alert("Please upload a valid Excel file.");
		        }
			});
		});
		
		
		function ProcessExcel(data) {
	        var workbook = XLSX.read(data,{ type: 'binary' });
	        var firstSheet = workbook.SheetNames[0];
	        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
          // $('#keywords').val(excelRows);
	        for (var i = 0; i < excelRows.length; i++) { 
	            $('#keywords').append("\n"+excelRows[i].Keywords);
	        }
		}


	</script> 
@endsection
{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('vendors/js/extensions/jquery.steps.min.js')}}"></script>
<script src="{{asset('vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
<script src="//jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script type="text/javascript">

</script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
<script src="{{asset('js/scripts/forms/wizard-steps.js')}}"></script>
@endsection