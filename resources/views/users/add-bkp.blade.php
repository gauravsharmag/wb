@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title','Add Client')
{{-- venodr style --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
@endsection

{{-- page style --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
@endsection

@section('content')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" />
	<section id="dashboard-analytics">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js" integrity="sha512-rMGGF4wg1R73ehtnxXBt5mbUfN9JUJwbk21KMlnLZDJh7BkPmeovBuddZCENJddHYYMkCh9hPFnPmS9sspki8g==" crossorigin="anonymous">
	</script>

	    <div class="row match-height">
	      		
	      	<div class="col-md-8 col-12">
	      	      <div class="card" >
	      	        <div class="card-header">
	      	          <h4 class="card-title"> {{__('Add New Client')}} </h4>
	      	        </div>
	      	        <div class="card-content">
	      	          <div class="card-body">
	      	            <form class="form form-horizontal" method="POST" action="/users/add" enctype="multipart/form-data">
	      	            @csrf
	      	              <div class="form-body">
	      	                <div class="row">
	      	                
	      	                	<div class="col-md-4">
	      	                	  <label>GA Account</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  {!! Form::select('ga_account[]',$gaAccounts,null,['class'=>'form-control','id'=>'ga_account','multiple'=>true] ) !!}	
	      	                	  @error('ga_account')
	      	                	    <span class="invalid-feedback" role="alert" style="display:block;">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror	
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Client Name</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="client_name" class="form-control @error('client_name') is-invalid @enderror" name="client_name" value="{{old('client_name')}}">
	      	                	  @error('client_name')
	      	                	    <span class="invalid-feedback" role="alert" style="display:block;">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Client Email</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{old('email')}}">
	      	                	  @error('email')
	      	                	    <span class="invalid-feedback" role="alert" style="display:block;">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Contact Name</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="contact_name" class="form-control @error('email') is-invalid @enderror" name="contact_name" value="{{old('contact_name')}}">
	      	                	  @error('contact_name')
	      	                	    <span class="invalid-feedback" role="alert" style="display:block;">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	
	      	                	<div class="col-md-4">
	      	                	  <label>Phone#</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="number" id="phone" class="form-control @error('email') is-invalid @enderror" name="phone" value="{{old('phone')}}">
	      	                	  @error('phone')
	      	                	    <span class="invalid-feedback" role="alert" style="display:block;">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Address</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="address" class="form-control" name="address" value="{{old('address')}}">
	      	                	  @error('address') 
	      	                	    <span class="invalid-feedback" role="alert" style="display: block;">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>
	      	                	<div class="col-md-4">
	      	                	  <label>Website Url</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="website_url" class="form-control" name="website_url" value="{{old('website_url')}}">
	      	                	  @error('website_url') 
	      	                	    <span class="invalid-feedback" role="alert" style="display: block;">
	      	                	      <strong>{{ $message }}</strong> 
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>
	      	                	<div class="col-md-4">
	      	                	  <label>Website Logo</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="file" id="website_logo" class="form-control" name="website_logo" > 
	      	                	  @error('website_logo')
	      	                	    <span class="invalid-feedback" role="alert" style="display: block !important;">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                  
	      	                  <div class="col-sm-12 d-flex justify-content-end">
	      	                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{__('Submit')}} </button>
	      	                  </div>
	      	                </div>
	      	              </div>
	      	            </form>
	      	          </div>
	      	        </div>
	      	      </div>
	      	</div>
	    </div>
	</section>
	<script type="text/javascript">
		$('#ga_account').chosen({
			placeholder_text_multiple: "Select GA Accounts",
		});
	</script>
@endsection