@extends('layouts.contentLayoutMaster')
{{-- page title --}}
@section('title','Users List')
{{-- vendor styles --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
@endsection
{{-- page styles --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-users.css')}}">
@endsection
@section('content')
<!-- users list start -->
<section class="users-list-wrapper">
  <div class="users-list-filter px-1">
    <form>
      <div class="row border rounded py-2 mb-2">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-2"></div>
        <div class="col-md-2">Hide Archived Clients <input type="checkbox" name="userType" id="userTypeCheckbox" checked="checked"></div>
      </div>
    </form>
  </div>
  <div class="users-list-table">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <div class="table-responsive">
            <table id="users-list-datatable" class="table">
              <thead>
                <tr> 
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Archived</th>
                    <th>Added On</th>
                    <th>Action</th> 
                </tr>
              </thead>
              <tbody>
                

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- users list ends -->
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
    $(function() { 
      getClients(0);   
      $('#userTypeCheckbox').change(function() {
        if($(this).is(":checked")) {
          getClients(0);    
        }else {
          getClients(1);
        }
      });
    }); 
 
    function getClients(type) { 
      $('#users-list-datatable').DataTable({
          responsive: true,
          processing: true,  
          serverSide: true, 
          bDestroy:true,
          ajax: "/users-ajax?type="+type,    
          lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
          columns: [
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'role_id', name: 'role_id' },
                { data: 'archived', name: 'archived' },
                { data: 'created_at', name: 'created_at'}, 
                { data: 'action', name: 'action'}    
          ], 
      });
    }


    function archiveUser(user) {
      if(user){
        if(confirm('Are you sure you want to archive this client?')){
          window.location.href = '/users/archive/'+user+'/1'; 
        }
      }
    }
    function unArchiveUser(user) {
      if(user){
        if(confirm('Are you sure you want to activate this client?')){
          window.location.href = '/users/archive/'+user+'/0';  
        }
      }
    }
    function deleteUser(user) {
      if(user){
        if(confirm('Are you sure you want to delete this client?')){
          window.location.href = '/users/delete/'+user; 
        }
      }
    }
    function masqueradeClient(client) {
      if(client){
        if(confirm('Are you sure you want to masquerade this client?')){ 
          window.location.href = '/users/masquerade/'+client;  
        }
      }
    }
</script>

@endsection





{{-- page scripts --}}
@section('page-scripts')
<!-- <script src="{{asset('js/scripts/pages/page-users.js')}}"></script> -->
@endsection