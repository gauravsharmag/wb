@extends('layouts.contentLayoutMaster')
@section('title','Add Admin')
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
@endsection

@section('content')

	<section id="dashboard-analytics">
	    <div class="row match-height">
	      		
	      	<div class="col-md-8 col-12">
	      	      <div class="card" >
	      	        <div class="card-header">
	      	          <h4 class="card-title"> {{__('Add New Admin')}} </h4>
	      	        </div>
	      	        <div class="card-content">
	      	          <div class="card-body">
	      	            <form class="form form-horizontal" method="POST" action="/users/add-admin" enctype="multipart/form-data">
	      	            	@csrf
	      	              <div class="form-body">
	      	                <div class="row">
	      	                
	      	                	<div class="col-md-4">
	      	                	  <label>Admin Name</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="client_name" class="form-control @error('client_name') is-invalid @enderror" name="client_name" value="{{old('client_name')}}">
	      	                	  @error('client_name')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Admin Email</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{old('email')}}">
	      	                	  @error('email')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Phone#</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="number" id="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{old('phone')}}">
	      	                	  @error('phone')
	      	                	    <span class="invalid-feedback" role="alert">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div> 

	      	                	<div class="col-md-4">
	      	                	  <label>Address</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="address" class="form-control" name="address" value="{{old('address')}}">
	      	                	</div>
	      	                	<div class="col-md-4">
	      	                	  <label>State</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  {!! Form::select('state',$states,null,['class'=>'form-control','id'=>'state','placeholder'=>'Select State'] ) !!} 
	      	                	  @error('state')  
	      	                	    <span class="invalid-feedback" role="alert" style="display: block;">
	      	                	      <strong>{{ $message }}</strong> 
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>
	      	                	<div class="col-md-4">
	      	                	  <label>City</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="city" class="form-control" name="city" value="{{old('city')}}">
	      	                	  @error('city')  
	      	                	    <span class="invalid-feedback" role="alert" style="display: block;">
	      	                	      <strong>{{ $message }}</strong> 
	      	                	    </span>
	      	                	  @enderror 
	      	                	</div>
	      	                	<div class="col-md-4">
	      	                	  <label>Zip</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="text" id="zip" class="form-control" name="zip" value="{{old('zip')}}">
	      	                	  @error('zip')  
	      	                	    <span class="invalid-feedback" role="alert" style="display: block;">
	      	                	      <strong>{{ $message }}</strong>  
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>
	      	                	<div class="col-md-4">
	      	                	  <label>Upload Pic</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	      	                	  <input type="file" id="pic" class="form-control" name="pic" > 
	      	                	  @error('pic') 
	      	                	    <span class="invalid-feedback" role="alert" style="display: block !important;">
	      	                	      <strong>{{ $message }}</strong>
	      	                	    </span>
	      	                	  @enderror
	      	                	</div>

	      	                	<div class="col-md-4">
	      	                	  <label>Admin Type</label>
	      	                	</div>
	      	                	<div class="col-md-8 form-group">
	  	      	                	<ul class="list-unstyled mb-0">
      	                	            <li class="d-inline-block mr-2 mb-1">
      	                	                <fieldset>
      	                	                  <div class="radio">
      	                	                    <input type="radio" name="admin_type" id="radio1" checked="" value="3">
      	                	                    <label for="radio1">Super Admin</label>
      	                	                  </div>
      	                	                </fieldset>
      	                	            </li> 
      	                	            <li class="d-inline-block mr-2 mb-1">
      	                	                <fieldset>
      	                	                  <div class="radio">
      	                	                    <input type="radio" name="admin_type" id="radio2" value="1">
      	                	                    <label for="radio2">Regular Admin</label>
      	                	                  </div>
      	                	                </fieldset>
      	                	            </li>
	                  	            </ul>
	      	                	</div>
	      	                	
 
	      	                  <div class="col-sm-12 d-flex justify-content-end">
	      	                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{__('Submit')}} </button>
	      	                  </div>
	      	                </div>
	      	              </div>
	      	            </form>
	      	          </div>
	      	        </div>
	      	      </div>
	      	</div>

	    </div>
	</section>
	
@endsection