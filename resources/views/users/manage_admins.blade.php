@extends('layouts.contentLayoutMaster')
{{-- page title --}}
@section('title','Manage Admins')
{{-- vendor styles --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
@endsection
{{-- page styles --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-users.css')}}">
@endsection
@section('content')
<!-- users list start -->
<section class="users-list-wrapper">
  <div class="users-list-filter px-1">
    <form>
      <div class="row border rounded py-2 mb-2">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-2">
          <a href="/users/add-admin" class="btn btn-primary glow w-100 position-relative"> 
            Add New <i id="icon-arrow" class="bx bx-plus-medical"></i>
          </a> 
        </div> 
        <div class="col-md-2">Hide Archived Admins <input type="checkbox" name="adminType" id="adminTypeCheckbox" checked="checked"></div>
      </div>
    </form>
  </div>
  <div class="users-list-table">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <div class="table-responsive">
            <table id="users-list-datatable" class="table">
              <thead>
                <tr> 
                    <th>Name</th>
                    <th>Email</th> 
                    <th>Phone</th>
                    <th>Role</th>
                    <th>Archive/Activate</th>
                    <th>Action</th> 
                </tr>
              </thead>
              <tbody>
                

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- users list ends -->
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
    $(function() { 
      getClients(0);   
      $('#adminTypeCheckbox').change(function() {
        if($(this).is(":checked")) {
          getClients(0);    
        }else {
          getClients(1);
        }
      });
    }); 
 
    function getClients(type) { 
      $('#users-list-datatable').DataTable({
          responsive: true,
          processing: true,  
          serverSide: true, 
          bDestroy:true,
          ajax: "/users/admins-ajax?type="+type,    
          lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
          columns: [
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'phone', name: 'phone' },
                { data: 'role_id', name: 'role_id' }, 
                { data: 'archived', name: 'archived' },
                { data: 'action', name: 'action'}    
          ], 
      });
    }


    function archiveUser(user) {
      if(user){
        if(confirm('Are you sure you want to archive this admin?')){
          window.location.href = '/users/archive/'+user+'/1'; 
        }
      }
    }
    function unArchiveUser(user) {
      if(user){
        if(confirm('Are you sure you want to activate this admin?')){ 
          window.location.href = '/users/archive/'+user+'/0';  
        }
      }
    }
    function deleteUser(user) {
      if(user){
        if(confirm('Are you sure you want to delete this admin?')){
          window.location.href = '/users/delete/'+user; 
        }
      }
    }
    
</script>

@endsection





{{-- page scripts --}}
@section('page-scripts')
<!-- <script src="{{asset('js/scripts/pages/page-users.js')}}"></script> -->
@endsection