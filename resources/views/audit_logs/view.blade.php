@extends('layouts.contentLayoutMaster')
@section('title','View Audit Log')
@section('vendor-styles')  
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/dragula.min.css')}}">
@endsection

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-analytics.css')}}">
@endsection

@section('content')
	<section id="dashboard-analytics">
	    <div class="row match-height">
	      		
	      	<div class="col-md-8 col-12">
	      	      <div class="card" >
	      	        <div class="card-header">
	      	          <h4 class="card-title"> {{__('View Audit Log')}} </h4>
	      	        </div>
	      	        <div class="card-content">
	      	          <div class="card-body">
	      	          	<div class="form-body">
	      	          	  	<div class="row">
      	          	  			<div class="col-md-4">
      	          	  			  <label>User</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->user}}</p>
      	          	  			</div>
      	          	  			<div class="col-md-4">
      	          	  			  <label>Action</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->type}}</p>
      	          	  			</div>

      	          	  			<div class="col-md-4">
      	          	  			  <label>Message</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->message}}</p>
      	          	  			</div>	
      	          	  			<div class="col-md-4">
      	          	  			  <label>Refrer</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->refrer}}</p>
      	          	  			</div>
      	          	  			<div class="col-md-4">
      	          	  			  <label>Host</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->host}}</p>
      	          	  			</div>
      	          	  			<div class="col-md-4">
      	          	  			  <label>IP</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->ip}}</p>
      	          	  			</div>
      	          	  			<div class="col-md-4">
      	          	  			  <label>Url</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->url}}</p>
      	          	  			</div>
      	          	  			<div class="col-md-4">
      	          	  			  <label>Method</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->method}}</p>
      	          	  			</div>
      	          	  			<div class="col-md-4">
      	          	  			  <label>User Agent</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{$log->user_agent}}</p>
      	          	  			</div>
      	          	  			<div class="col-md-4">
      	          	  			  <label>Created On</label>
      	          	  			</div>
      	          	  			<div class="col-md-8 form-group">
      	          	  				<p> {{date('d M,Y H:i:s',strtotime($log->created_at))}}</p>
      	          	  			</div>
	      	          	  	</div>
	      	          	</div>
	      	          </div>
	      	        </div>
	      	      </div>
	      	</div>
	    </div>
	</section>	
@endsection