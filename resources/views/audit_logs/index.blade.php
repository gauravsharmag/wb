@extends('layouts.contentLayoutMaster')
{{-- page title --}}
@section('title','Audit Log')
{{-- vendor styles --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
@endsection
{{-- page styles --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-users.css')}}">
@endsection
@section('content')
<section class="users-list-wrapper">
 
  <div class="users-list-table">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <div class="table-responsive">
            <table id="auditLogs" class="table">
              <thead>
                <tr> 
                    <th>User</th>
                    <th>Action</th>
                    <th>Date & Time</th>
                    <th>Action</th> 
                </tr>
              </thead>
              <tbody>
                

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
    $(function() {  
      $('#auditLogs').DataTable({
          responsive: true,
          processing: true,  
          serverSide: true,
          ordering: false,
          ajax: "{{ route('getAuditLogAjax') }}",     
          lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
          columns: [
                { data: 'user', name: 'user' }, 
                { data: 'type', name: 'type' },
                { data: 'created_at', name: 'created_at'}, 
                { data: 'action', name: 'action'}    
          ], 
      });
    }); 
    function archiveUser(user) {
      if(user){
        if(confirm('Are you sure you want to archive this user?')){
          window.location.href = '/users/archive/'+user; 
        }
      }
      
    }
</script>

@endsection





{{-- page scripts --}}
@section('page-scripts')
<!-- <script src="{{asset('js/scripts/pages/page-users.js')}}"></script> -->
@endsection